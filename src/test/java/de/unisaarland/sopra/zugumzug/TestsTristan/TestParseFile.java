package de.unisaarland.sopra.zugumzug.TestsTristan;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class TestParseFile {

	static GameLogic gl;
	static ByteArrayOutputStream output;
	static String standardError = "Error";
	static PrintStream old;
	static Server server;

	@BeforeClass
	public static void setUp() throws Exception {
		gl = new GameLogic(5002, null, 0, 2);
		output = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(output);
		old = System.out;
		System.setOut(ps);
		output.reset();
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	// Standard error should all start with Error

	@Test
	public void testMissingKeyWord() throws Exception {
		assertFalse (gl.parseFile("Maps/testMapMissingKeyWord.txt"));
	}

	// note not enough cards for 3 players
	@Test
	public void testNotEnoughMissionCards() throws Exception {
		gl.setmClosedStack(new ArrayList<MissionCard>());
		gl.setRequiredPlayers(3);
		assertFalse (gl.parseFile("Maps/testMapNotEnoughMCards.txt"));
	}

	@Test
	public void testWrongAuftraege() throws Exception {
		gl.setRequiredPlayers(4);
		assertFalse(gl.parseFile("Maps/testMapWrongAuftraege.txt"));
	}

	@Test
	public void testWrongComment() throws Exception {
		assertFalse (gl.parseFile("Maps/testMapWrongComment.txt"));
	}

	@Test
	public void testTrackKind() throws Exception {
		assertFalse(gl.parseFile("Maps/testMapWrongTrackKind.txt"));
	}

	@Test
	public void testWrongLimits() throws Exception {
		assertFalse (gl.parseFile("Maps/testMapWrongLimits.txt"));
	}

	@Test
	public void testWrongMaxTracks() throws Exception {
		assertFalse (gl.parseFile("Maps/testMapWrongMaxTracks.txt"));
	}

	@Test
	public void testWrongsStaedte() throws Exception {
		assertFalse (gl.parseFile("Maps/testMapWrongStaedte.txt"));
	}

	@Test
	public void testWrongStrecken() throws Exception {
		assertFalse (gl.parseFile("Maps/testMapWrongStrecken.txt"));
	}

	@Test
	public void testTwoLinesInOne() throws Exception {
		assertFalse (gl.parseFile("Maps/testMapTwoLinesInOne.txt"));
	}

	@Test
	public void testSameKeyWordTwice() throws Exception {
		assertFalse (gl.parseFile("Maps/testMapSameKeyWordTwice.txt"));
	}
}
