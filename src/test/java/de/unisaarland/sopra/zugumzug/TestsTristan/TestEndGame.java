package de.unisaarland.sopra.zugumzug.TestsTristan;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.server.EndGame;

public class TestEndGame {

	// Note this test case test only the private methods calculateMaxTrack and
	// checkMissionpoints
	// Since an other developer has test cases for the checkWinner method which
	// executes both methods.

	// TEST NOT FINSIHED

	private EndGame eg;
	private Board board;
	private Method calcTrack, checkMiss;
	private Contestant p1, p2, p3, p4;
	private City a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;

	@Before
	public void setUpBeforeClass() throws Exception {
		p1 = new Contestant(0, "A", 30);
		p2 = new Contestant(1, "B", 30);
		p3 = new Contestant(2, "C", 30);
		p4 = new Contestant(3, "D", 30);
		List<Contestant> playerList = new ArrayList<Contestant>();
		playerList.add(p1);
		playerList.add(p2);
		playerList.add(p3);
		playerList.add(p4);
		board = new Board();
		a = new City("A", 0, 2, 3);
		b = new City("B", 1, 1, 4);
		c = new City("C", 2, 1, 4);
		d = new City("D", 3, 3, 4);
		e = new City("E", 4, 3, 4);
		f = new City("F", 5, 3, 2);
		g = new City("G", 6, 2, 1);
		h = new City("H", 7, 4, 4);
		i = new City("I", 8, 4, 3);
		j = new City("J", 9, 4, 2);
		k = new City("K", 10, 4, 1);
		l = new City("L", 11, 1, 1);
		m = new City("M", 12, 2, 2);
		n = new City("N", 13, 1, 3);
		o = new City("O", 14, 2, 4);
		p = new City("P", 15, 3, 1);
		board.getListCities().add(a);
		board.getListCities().add(b);
		board.getListCities().add(c);
		board.getListCities().add(d);
		board.getListCities().add(e);
		board.getListCities().add(f);
		board.getListCities().add(g);
		board.getListCities().add(h);
		board.getListCities().add(i);
		board.getListCities().add(j);
		board.getListCities().add(k);
		board.getListCities().add(l);
		board.getListCities().add(m);
		board.getListCities().add(n);
		board.getListCities().add(o);
		board.getListCities().add(p);
		board.update(new Track(0, b, o, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(1, o, a, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(2, a, d, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(3, d, e, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(4, e, h, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(5, e, f, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(6, h, i, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(7, i, j, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(8, j, f, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(9, f, k, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(10, f, p, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(11, f, m, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(12, f, g, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(13, k, p, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(14, g, l, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(15, l, b, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(16, m, c, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(17, m, a, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(18, c, a, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(19, c, n, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(20, o, b, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(21, a, o, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(22, d, a, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(23, e, d, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(24, h, e, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(25, f, e, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(26, i, h, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(27, j, i, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(28, f, j, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(29, k, f, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(30, p, f, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(31, m, f, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(32, g, f, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(33, p, k, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(34, l, g, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(35, b, l, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(36, c, m, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(37, a, m, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(38, a, c, 1, 1, false, TrackKind.BLUE));
		board.update(new Track(39, n, c, 1, 1, false, TrackKind.BLUE));
		eg = new EndGame(board, playerList, null);
		eg.getListContestant().get(0).addMission(new MissionCard(m, d, 5, 0));

		calcTrack = EndGame.class.getDeclaredMethod("calculateMaxTrack", (Class[]) null);
		calcTrack.setAccessible(true);
		checkMiss = EndGame.class.getDeclaredMethod("checkMissionPoints", (Class[]) null);
		checkMiss.setAccessible(true);
	}

	@Test
	public void testCheckMissionPoints()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		int points = p1.getPoints();
		p1.getListTracks().add(new Track(17, m, a, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(36, c, m, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(22, d, a, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(38, a, c, 1, 1, false, TrackKind.BLUE));
		board.getListTracks().get(17).setOwnerId(0);
		board.getListTracks().get(36).setOwnerId(0);
		board.getListTracks().get(22).setOwnerId(0);
		board.getListTracks().get(38).setOwnerId(0);
		calcTrack.invoke(eg, (Object[]) null);
		checkMiss.invoke(eg, (Object[]) null);
		assertEquals(points + 15, p1.getPoints()); // I had to do +15 because p1
													// also got longest Track
													// Points

	}

	@Test
	public void testCalcMaxTrackCycle()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		p1.getListTracks().add(new Track(17, m, a, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(36, c, m, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(22, d, a, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(38, a, c, 1, 1, false, TrackKind.BLUE));
		board.getListTracks().get(17).setOwnerId(0);
		board.getListTracks().get(36).setOwnerId(0);
		board.getListTracks().get(22).setOwnerId(0);
		board.getListTracks().get(38).setOwnerId(0);

		calcTrack.invoke(eg, (Object[]) null);

		assertTrue(eg.getMaxTrack(0) == 4);
	}

	@Test
	public void tesClacMaxTrackStandard()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		// normal track
		p1.getListTracks().add(new Track(17, m, a, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(36, c, m, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(22, d, a, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(38, a, c, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(31, m, f, 1, 1, false, TrackKind.BLUE));
		p1.getListTracks().add(new Track(28, f, j, 1, 1, false, TrackKind.BLUE));
		board.getListTracks().get(17).setOwnerId(0);
		board.getListTracks().get(36).setOwnerId(0);
		board.getListTracks().get(22).setOwnerId(0);
		board.getListTracks().get(38).setOwnerId(0);
		board.getListTracks().get(31).setOwnerId(0);
		board.getListTracks().get(28).setOwnerId(0);

		// normal track
		p2.getListTracks().add(new Track(14, g, l, 1, 1, false, TrackKind.BLUE));
		p2.getListTracks().add(new Track(15, l, b, 1, 1, false, TrackKind.BLUE));
		p2.getListTracks().add(new Track(0, b, o, 1, 1, false, TrackKind.BLUE));
		p2.getListTracks().add(new Track(1, o, a, 1, 1, false, TrackKind.BLUE));
		p2.getListTracks().add(new Track(2, a, d, 1, 1, false, TrackKind.BLUE));
		p2.getListTracks().add(new Track(3, d, e, 1, 1, false, TrackKind.BLUE));
		board.getListTracks().get(14).setOwnerId(1);
		board.getListTracks().get(15).setOwnerId(1);
		board.getListTracks().get(0).setOwnerId(1);
		board.getListTracks().get(1).setOwnerId(1);
		board.getListTracks().get(2).setOwnerId(1);
		board.getListTracks().get(3).setOwnerId(1);

		// no track for p4

		// 2 tracks
		p3.getListTracks().add(new Track(25, f, e, 1, 1, false, TrackKind.BLUE));
		p3.getListTracks().add(new Track(23, e, d, 1, 1, false, TrackKind.BLUE));
		p3.getListTracks().add(new Track(27, j, i, 1, 1, false, TrackKind.BLUE));
		p3.getListTracks().add(new Track(26, i, h, 1, 1, false, TrackKind.BLUE));
		p3.getListTracks().add(new Track(24, h, e, 1, 1, false, TrackKind.BLUE));
		board.getListTracks().get(25).setOwnerId(2);
		board.getListTracks().get(23).setOwnerId(2);
		board.getListTracks().get(27).setOwnerId(2);
		board.getListTracks().get(26).setOwnerId(2);
		board.getListTracks().get(24).setOwnerId(2);

		calcTrack.invoke(eg, (Object[]) null);

		assertTrue(eg.getMaxTrack(1) == 6);
		assertTrue(eg.getMaxTrack(0) == 5);
		assertTrue(eg.getMaxTrack(3) == 0);
		assertTrue(eg.getMaxTrack(2) == 4);

	}
}
