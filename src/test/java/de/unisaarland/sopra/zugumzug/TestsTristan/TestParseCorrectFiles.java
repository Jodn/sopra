package de.unisaarland.sopra.zugumzug.TestsTristan;

import static org.junit.Assert.*;
import java.lang.reflect.Field;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class TestParseCorrectFiles {

	static GameLogic gl;
	static Server server;

	@BeforeClass
	public static void setUp() throws Exception {
		gl = new GameLogic(5222, null, 0, 2);
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}

	@AfterClass
	public static void closeUp() throws Exception {
		server.close();
	}

	@Test
	public void testTaskMap() throws Exception {
		assertTrue(gl.parseFile("Maps/TaskMap2.txt"));
	}

}
