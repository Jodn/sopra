package de.unisaarland.sopra.zugumzug.TestsTristan;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.zugumzug.Main;

public class TestsParseCommandLine {

	String[] args;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	Main m;
	String standardError = "Error in args:";

	// jarfile is not part of the args

	@Before
	public void setUp() throws Exception {
		m = new Main();
		System.setOut(new PrintStream(outContent));
		//args = new String[8];
		outContent.reset();

	}

	@Test
	public void testMainHelp() throws IOException {
		args = new String[8];
		args[0] = "--help";
		Main.parseCommandLine(args);
		// empty string for no output
		assertTrue(outContent.toString() != "");

	}

	@Test
	public void testCorrectSeed() {
		args = new String[8];
		outContent.reset();
		// after the javadoc https://sopra.cs.uni-saarland.de/site/javadoc/ seed
		// is int so it can be negativ
		args[0] = "--server";
		args[1] = "4000";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "-777";
		args[6] = "-p";
		args[7] = "10";

		Main.parseCommandLine(args);
		assertEquals(outContent.toString(), "");
	}

	@Test
	public void testCorrectGui() {
		args = new String[4];
		args[0] = "--gui";
		args[1] = "4000";
		args[2] = "-n";
		args[3] = "Alice";
		Main.parseCommandLine(args);
		assertEquals(outContent.toString(), "");
		outContent.reset();
	}

	@Test
	public void testCorrectKiNoName() {
		args = new String[2];
		// name is optional since the program offers a default name
		args = new String[2];
		args[0] = "--ki";
		args[1] = "134.96.12.54:4000";

		Main.parseCommandLine(args);
		assertEquals(outContent.toString(), "");
		outContent.reset();
	}

	@Test
	public void testCorrectKiName() {
		args = new String[4];
		args[0] = "--ki";
		args[1] = "localhost:4000";
		args[2] = "-n";
		args[3] = "Charlie";

		Main.parseCommandLine(args);
		assertEquals(outContent.toString(), "");

	}

	@Test
	public void testInCorrectServer() {
		args = new String[8];
		outContent.reset();
		args[0] = "-server";
		args[1] = "4000";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "777";
		args[6] = "-p";
		args[7] = "10";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));
	}

	@Test
	public void testInCorrectPort() {
		args = new String[8];
		args[0] = "--server";
		args[1] = "-777";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "777";
		args[6] = "-p";
		args[7] = "10";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));
	}

	@Test
	public void testInCorrectMap() {
		args = new String[8];
		args[0] = "--server";
		args[1] = "4000";
		args[2] = "--map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "777";
		args[6] = "-p";
		args[7] = "10";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));
	}


	@Test
	public void testInCorrectSeedArg() {
		args = new String[8];
		args[0] = "--server";
		args[1] = "4000";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-ss";
		args[5] = "777";
		args[6] = "-p";
		args[7] = "10";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));
	}

	@Test
	public void testInCorrectNumSeed() {
		args = new String[8];
		args[0] = "--server";
		args[1] = "4000";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "abc";
		args[6] = "-p";
		args[7] = "10";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));

	}

	@Test
	public void testInCorrectNumPlayerArgs() {
		args = new String[8];
		args[0] = "--server";
		args[1] = "4000";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "6";
		args[6] = "-P";
		args[7] = "10";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));
	}

	@Test
	public void testInCorrectNumPlayers() {
		args = new String[8];
		args[0] = "--server";
		args[1] = "4000";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "6";
		args[6] = "-p";
		args[7] = "-121212";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));
	}

	@Test
	public void testInCorrectArgs() {
		args = new String[8];
		args[0] = "--server";
		args[1] = "!§$%&/()=?'_'°^|<>µ";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "abc";
		args[6] = "-P";
		args[7] = "-121212";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));
	}

	@Test
	public void testMissingNumPlayers() {
		args = new String[8];
		args[0] = "--server";
		args[1] = "4000";
		args[2] = "-map";
		args[3] = "testmap.txt";
		args[4] = "-s";
		args[5] = "10";
		args[6] = "-p";

		Main.parseCommandLine(args);
		assertTrue(outContent.toString().contains(standardError));
	}

}