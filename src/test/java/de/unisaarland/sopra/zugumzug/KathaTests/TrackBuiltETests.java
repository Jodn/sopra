package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.events.TrackBuiltEvent;

public class TrackBuiltETests {

	private TrackBuiltEvent TBE;
	private Board board;
	private int TrackID, PlayerID;
	private TrackKind kind;
	private HashMap<Integer, Contestant> players;

	@Before
	public void setUp() {
		this.kind = TrackKind.BLUE;
		this.TrackID = 0;
		this.PlayerID = 4;
		this.board = new Board();
		this.players = new HashMap<Integer, Contestant>();
		Contestant cont = new Contestant(PlayerID, "Legolas", 20);
		cont.getHandCards().add(TrackKind.BLUE);
		cont.getHandCards().add(TrackKind.BLUE);
		cont.getHandCards().add(TrackKind.BLUE);
		cont.getHandCards().add(TrackKind.BLUE);
		cont.getHandCards().add(TrackKind.RED);
		cont.getHandCards().add(TrackKind.ALL);
		cont.getHandCards().add(TrackKind.ALL);
		cont.getHandCards().add(TrackKind.ALL);
		players.put(PlayerID, cont);
		City city1 = new City("Bruchtal", 3, 3, 3);
		City city2 = new City("Gondolin", 4, 4, 4);
		Track track = new Track(TrackID, city1, city2, 3, 4, false, kind);
		board.getListTracks().add(track);
		this.TBE = new TrackBuiltEvent(4, PlayerID, TrackID, 3, board, players, kind);
	}

	@Test
	public void testOwnerId() {
		assertEquals("Track already taken", -1, board.getListTracks().get(TrackID).getOwnerId());
		TBE.execute();
		assertEquals("Wrong ownerID", PlayerID, board.getListTracks().get(TrackID).getOwnerId());
	}

	@Test
	public void testAddedTrackToContestant() {
		assertEquals("Track is a tunnel", false, board.getListTracks().get(TrackID).isTunnel());
		int x = players.get(PlayerID).getListTracks().size();
		TBE.execute();
		assertEquals("Track wasn't added to Contestant", x + 1, players.get(PlayerID).getListTracks().size());
	}

	@Test
	public void testRemoveResourcesAndTrains() {
		int x = players.get(PlayerID).getHandCards().size();
		int cost = board.getListTracks().get(TrackID).getLength();
		int tr = players.get(PlayerID).getTrainsLeft();
		TBE.execute();
		assertEquals("Resources weren't removed", x - cost, players.get(PlayerID).getHandCards().size());
		assertEquals("Trains weren't removed", tr - 3, players.get(PlayerID).getTrainsLeft());
	}

	@Test
	public void testAddedPoints() {
		int x = players.get(PlayerID).getPoints();
		int points = board.getListTracks().get(TrackID).getPoints();
		TBE.execute();
		assertEquals("Wrong score", x + points, players.get(PlayerID).getPoints());
	}
}