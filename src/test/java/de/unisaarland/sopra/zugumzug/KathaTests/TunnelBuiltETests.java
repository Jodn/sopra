package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.BuildTrack;
import de.unisaarland.sopra.PayTunnel;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TakeSecretCard;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.client.PlayerLogic;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.events.TunnelBuiltEvent;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class TunnelBuiltETests {

	private TunnelBuiltEvent TBE;
	private int PlayerID, TrackID;
	private Board board;
	private int AddCost;
	private HashMap<Integer, Contestant> players;
	private TrackKind kind;
	private static PlayerLogic pl;
	private static Server server;
	private static GameLogic gl;

	@BeforeClass
	public static void SetUp() throws Exception {
		gl= new GameLogic(5555, null, 82, 0);
		gl.parseFile("C:/Users/Kadda/Studium/group-07/LogFiles/NormalGameTest/default.map");
		gl.waitPlayers();
		pl = new PlayerLogic ("localhost", 5555, "name");
	}

	
/*
	@AfterClass
	public static void afterClass() throws Exception{
	
		server.close();
	}*/
	
	@Before
	public void setUp() throws IOException {
		this.TrackID = 0;
		this.PlayerID = pl.myId;
		this.AddCost = 2;
		Contestant cont = pl.getContestant();
				//new Contestant(PlayerID, "Gandalf", 20);
		gl.addContestant(cont);
		cont.getHandCards().add(TrackKind.BLUE);
		cont.getHandCards().add(TrackKind.BLUE);
		cont.getHandCards().add(TrackKind.BLUE);
		cont.getHandCards().add(TrackKind.BLUE);
		cont.getHandCards().add(TrackKind.RED);
		cont.getHandCards().add(TrackKind.ALL);
		cont.getHandCards().add(TrackKind.ALL);
		cont.getHandCards().add(TrackKind.ALL);
		City city1 = new City("Helm's Deep", 3, 5, 4);
		City city2 = new City("Bree", 6, 5, 8);
		this.kind = TrackKind.BLUE;
		Track track = new Track(TrackID, city1, city2, 4, 5, true, this.kind);
		this.board = new Board();
		this.players = new HashMap<Integer, Contestant>();
		players.put(PlayerID, cont);
		board.getListTracks().add(track);
		//TODO will get null pointer
		this.TBE = new TunnelBuiltEvent(PlayerID, TrackID, 3, kind, AddCost, board, players,pl);
	}

	@Test
	public void testOwnerId() {
		assertEquals("Tunnel already taken", -1, board.getListTracks().get(TrackID).getOwnerId());
		TBE.execute();
		assertEquals("Wrong OwnerID", PlayerID, board.getListTracks().get(TrackID).getOwnerId());
	}

	@Test
	public void testAddedTunnelToContestant() {
		assertEquals("Track is not a tunnel", true, board.getListTracks().get(TrackID).isTunnel());
		TBE.execute();
		Track track = board.getListTracks().get(TrackID);
		assertTrue("Track wasn't added to contestant", players.get(PlayerID).getListTracks().contains(track));
	}

	@Test
	public void testRemoveHandcardsAndTrains() {
		int x = players.get(PlayerID).getHandCards().size();
		int costs = board.getListTracks().get(TrackID).getLength() + this.AddCost;
		int tr = players.get(PlayerID).getTrainsLeft();
		TBE.execute();
		assertEquals("Resources weren't removed", x - costs, players.get(PlayerID).getHandCards().size());
		assertEquals("Trains weren't removed", tr - 4, players.get(PlayerID).getTrainsLeft());
	}

	@Test
	public void testAddedPoints() {
		int x = players.get(PlayerID).getPoints();
		int points = board.getListTracks().get(TrackID).getPoints();
		TBE.execute();
		assertEquals("Wrong score", x + points, players.get(PlayerID).getPoints());
	}
}
