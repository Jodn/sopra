package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.server.EndGame;

public class EndGameTests {

	/*
	 * Bei Punkte-Gleichstand gewinnt der Spieler mit den meisten erfuellten
	 * Auftraegen bzw. der laengsten Strecke.
	 */

	private EndGame EG;
	private static Server server;
	private Board board;
	private List<Contestant> contlist;
	private MissionCard m1;
	private MissionCard m2;
	private MissionCard m3;
	private MissionCard m4;
	private MissionCard m5;
	private Track t1;
	private Track t2;
	private Track t3;
	private Track t4;
	private Track t5;
	private Track t6;
	private Track t7;
	private Track t8;
	private Track t9;
	private City c1;
	private City c2;
	private City c3;
	private City c4;
	private City c5;
	private City c6;
	private City c7;

	@BeforeClass
	public static void runBeforeClass() throws IOException {
		server = new Server(4000);
	}

	@Before
	public void setUp() {
		this.board = new Board();
		this.contlist = new ArrayList<Contestant>();
		this.EG = new EndGame(board, contlist, server);
		c1 = new City("Isengart", 0, 0, 6);
		c2 = new City("Minas Tirith", 1, 4, 1);
		c3 = new City("Osgiliath", 2, 5, 3);
		c4 = new City("Minas Morgul", 3, 6, 1);
		c5 = new City("Barad-dur", 4, 7, 6);
		c6 = new City("Edoras", 5, 2, 0);
		c7 = new City("Helm's Deep", 6, 1, 2);
		board.getListCities().add(c1);
		board.getListCities().add(c2);
		board.getListCities().add(c3);
		board.getListCities().add(c4);
		board.getListCities().add(c5);
		board.getListCities().add(c6);
		board.getListCities().add(c7);
		this.t1 = new Track(8, c1, c7, 4, 10, false, TrackKind.BLUE);
		this.t2 = new Track(1, c1, c2, 2, 4, false, TrackKind.ALL);
		this.t3 = new Track(2, c7, c2, 3, 7, false, TrackKind.BLACK);
		this.t4 = new Track(3, c6, c2, 2, 4, false, TrackKind.GREEN);
		this.t5 = new Track(4, c2, c3, 2, 4, true, TrackKind.ORANGE);
		this.t6 = new Track(5, c2, c4, 2, 4, false, TrackKind.ALL);
		this.t7 = new Track(6, c3, c4, 2, 4, false, TrackKind.BLUE);
		this.t8 = new Track(7, c4, c5, 3, 7, true, TrackKind.GREEN);
		this.t9 = new Track(0, c1, c3, 5, 15, true, TrackKind.BLUE);
		c1.getMap().put(0, c7);
		c1.getMap().put(1, c2);
		c1.getMap().put(8, c3);
		c2.getMap().put(1, c1);
		c2.getMap().put(2, c7);
		c2.getMap().put(3, c6);
		c2.getMap().put(4, c3);
		c2.getMap().put(5, c4);
		c3.getMap().put(4, c2);
		c3.getMap().put(6, c4);
		c3.getMap().put(8, c1);
		c4.getMap().put(5, c2);
		c4.getMap().put(6, c3);
		c4.getMap().put(7, c5);
		c5.getMap().put(7, c4);
		c6.getMap().put(3, c2);
		c7.getMap().put(0, c1);
		c7.getMap().put(2, c2);
		board.getListTracks().add(t1);
		board.getListTracks().add(t2);
		board.getListTracks().add(t3);
		board.getListTracks().add(t4);
		board.getListTracks().add(t5);
		board.getListTracks().add(t6);
		board.getListTracks().add(t7);
		board.getListTracks().add(t8);
		board.getListTracks().add(t9);
		this.m1 = new MissionCard(c1, c2, 0, 0);
		this.m2 = new MissionCard(c3, c6, 0, 1);
		this.m3 = new MissionCard(c4, c7, 0, 2);
		this.m4 = new MissionCard(c4, c5, 0, 3);
		this.m5 = new MissionCard(c1, c6, 0, 4);
	}

	@Test
	public void testCheckWinnerPoints() throws IOException {
		Contestant cont1 = new Contestant(0, "Frodo", 5);
		Contestant cont2 = new Contestant(1, "Peregrin", 5);
		Contestant cont3 = new Contestant(2, "Samweis", 5);
		cont1.setPoints(2);
		cont2.setPoints(15);
		cont3.setPoints(20);
		contlist.add(cont1);
		contlist.add(cont2);
		contlist.add(cont3);
		List<Integer> x = EG.checkWinner();
		assertEquals(1, x.size());
		assertEquals(cont3.getId(), x.get(0).intValue());
	}

	@Test
	public void testCheckWinnerMissions() throws IOException {
		Contestant cont1 = new Contestant(0, "Frodo", 5);
		Contestant cont2 = new Contestant(1, "Peregrin", 5);
		Contestant cont3 = new Contestant(2, "Samweis", 5);
		contlist.add(cont1);
		contlist.add(cont2);
		contlist.add(cont3);
		cont1.setPoints(2);
		cont2.setPoints(20);
		cont3.setPoints(10);
		cont1.getListMissions().add(this.m3);
		cont2.getListMissions().add(m2);
		cont2.getListMissions().add(m4);
		cont3.getListMissions().add(m1);
		cont3.getListMissions().add(m5);
		cont2.getListTracks().add(t8);
		t8.setOwnerId(1);
		cont3.getListTracks().add(t2);
		t2.setOwnerId(2);
		cont3.getListTracks().add(t4);
		t4.setOwnerId(2);
		// cont3 completes m1,m5
		// cont2 completes m4
		// cont3 +10 (MaxTrack)
		List<Integer> x = EG.checkWinner();
		assertEquals("Wrong points", 20, cont3.getPoints());
		assertEquals(1, x.size());
		assertEquals(cont3.getId(), x.get(0).intValue());
		assertEquals("Wrong nummissions cont2", 1, cont2.getNumFinishedMissions());
		assertEquals("Wrong nummissions cont3", 2, cont3.getNumFinishedMissions());
	}

	@Test
	public void testCheckWinnerTrackLength() throws IOException {
		Contestant cont1 = new Contestant(0, "Frodo", 5);
		Contestant cont2 = new Contestant(1, "Peregrin", 5);
		Contestant cont3 = new Contestant(2, "Samweis", 5);
		contlist.add(cont1);
		contlist.add(cont2);
		contlist.add(cont3);
		cont1.setPoints(6);
		cont2.setPoints(10);
		cont3.setPoints(20);
		cont1.getListMissions().add(m2);
		cont2.getListMissions().add(m3);
		cont2.getListMissions().add(m4);
		cont3.getListMissions().add(m1);
		cont3.getListMissions().add(m5);
		cont2.getListTracks().add(t3);
		t3.setOwnerId(1);
		cont2.getListTracks().add(t6);
		t6.setOwnerId(1);
		cont2.getListTracks().add(t8);
		t8.setOwnerId(1);
		cont3.getListTracks().add(t2);
		t2.setOwnerId(2);
		cont3.getListTracks().add(t4);
		t4.setOwnerId(2);
		// cont2: +10 points (MaxTrack)
		// cont2,cont3 complete two missions
		// cont2 owns longer track
		List<Integer> x = EG.checkWinner();
		assertEquals(1, x.size());
		assertEquals(cont2.getId(), x.get(0).intValue());
		assertEquals("Wrong points", 20, cont2.getPoints());
		assertEquals("Wrong nummissions cont2", 2, cont2.getNumFinishedMissions());
		assertEquals("Wrong nummissions cont3", 2, cont3.getNumFinishedMissions());
	}

	@Test
	public void testcheckWinnerTwoWinners() throws IOException {
		Contestant cont1 = new Contestant(0, "Frodo", 5);
		Contestant cont2 = new Contestant(1, "Peregrin", 5);
		Contestant cont3 = new Contestant(2, "Samweis", 5);
		contlist.add(cont1);
		contlist.add(cont2);
		contlist.add(cont3);
		cont1.setPoints(8);
		cont2.setPoints(20);
		cont3.setPoints(20);
		cont2.getListMissions().add(m2);
		cont3.getListMissions().add(m3);
		cont2.getListTracks().add(t5);
		t5.setOwnerId(1);
		cont2.getListTracks().add(t4);
		t4.setOwnerId(1);
		cont2.getListTracks().add(t9);
		t9.setOwnerId(1);
		cont3.getListTracks().add(t1);
		t1.setOwnerId(2);
		cont3.getListTracks().add(t3);
		t3.setOwnerId(2);
		cont3.getListTracks().add(t6);
		t6.setOwnerId(2);
		// cont1,cont2: +10 (MaxTrack)
		List<Integer> x = EG.checkWinner();
		assertEquals("Not two winners", 2, x.size());
		assertEquals("Wrong points cont2", 30, cont2.getPoints());
		assertEquals("Wrong points cont3", 30, cont3.getPoints());
		assertTrue(x.contains(cont2.getId()) && x.contains(cont3.getId()));
	}

	@Test
	public void testcheckWinnerCalculatePoints() throws IOException {
		Contestant cont1 = new Contestant(0, "Frodo", 5);
		Contestant cont2 = new Contestant(1, "Peregrin", 5);
		contlist.add(cont1);
		contlist.add(cont2);
		cont1.setPoints(6);
		cont2.setPoints(6);
		m1.setPoints(10);
		m2.setPoints(5);
		m3.setPoints(7);
		cont1.getListMissions().add(m1);
		cont2.getListMissions().add(m2);
		cont2.getListMissions().add(m3);
		cont1.getListTracks().add(t2);
		t2.setOwnerId(0);
		cont2.getListTracks().add(t4);
		t4.setOwnerId(1);
		cont2.getListTracks().add(t5);
		t5.setOwnerId(1);
		// cont1 completes m1 (+10)
		// cont2 completes m2(+5), not m3 (-7)
		// cont2: +10 (MaxTrack)
		List<Integer> x = EG.checkWinner();
		assertEquals(1, x.size());
		assertEquals(cont1.getId(), x.get(0).intValue());
		assertEquals("Wrong points cont1", 16, cont1.getPoints());
		assertEquals("Wrong points cont2", 14, cont2.getPoints());
		assertEquals("Wrong nummissions cont2", 1, cont2.getNumFinishedMissions());
		assertEquals("Wrong nummissions cont1", 1, cont1.getNumFinishedMissions());
	}

	@Test
	public void testCheckWinnerSeveralWinners() throws IOException {
		Contestant cont1 = new Contestant(0, "Frodo", 5);
		Contestant cont2 = new Contestant(1, "Peregrin", 5);
		Contestant cont3 = new Contestant(2, "Samweis", 5);
		Contestant cont4 = new Contestant(3, "Meriadoc", 5);
		Contestant cont5 = new Contestant(4, "Bilbo", 5);
		contlist.add(cont1);
		contlist.add(cont2);
		contlist.add(cont3);
		contlist.add(cont4);
		contlist.add(cont5);
		cont1.setPoints(20);
		cont2.setPoints(20);
		cont3.setPoints(20);
		cont4.setPoints(20);
		cont5.setPoints(20);
		List<Integer> x = EG.checkWinner();
		assertEquals(5, x.size());
	}
	
	
	@Test
	public void testLongestPath () throws NoSuchMethodException, SecurityException, ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		/*Class EndGame = EG.getClass();
		Class[] c = new Class[5];
		c[0] = City.class;
		c[1] = HashSet.class;
		c[2] = List.class;
		c[3] = int.class;
		c[4] = int.class;
		Method method = EndGame.getDeclaredMethod("findLongestTrack", c);
		method.setAccessible(true);
		*/
		Contestant cont1 = new Contestant(0, "Frodo", 5);
		Contestant cont2 = new Contestant(1, "Peregrin", 5);
		contlist.add(cont1);
		contlist.add(cont2);
		for (int i = 0; i<9; i++){
			board.getListTracks().get(i).setLength(1);
			board.getListTracks().get(i).setOwnerId(0);
			cont1.getListTracks().add(board.getListTracks().get(i));
		}
		for (int i = 0; i<7; i++){
			board.getListCities().get(i).getMap().clear();
		}
		
		t1.setOwnerId(0);
		//Tracks in circles
		t1.setCity1(c6);
		t1.setCity2(c2);
		t2.setCity1(c2);
		t2.setCity2(c7);
		t3.setCity1(c7);
		t3.setCity2(c1);
		t4.setCity1(c1);
		t4.setCity2(c2);
		t5.setCity1(c2);
		t5.setCity2(c3);
		t6.setCity1(c3);
		t6.setCity2(c4);
		t7.setCity1(c4);
		t7.setCity2(c5);
		t8.setCity1(c5);
		t8.setCity2(c3);
		t9.setCity1(c3);
		t9.setCity2(c6);
		c1.getMap().put(2, c7);
		c1.getMap().put(3, c2);
		c2.getMap().put(8, c6);
		c2.getMap().put(1, c7);
		c2.getMap().put(3, c1);
		c2.getMap().put(4, c3);
		c3.getMap().put(4, c2);
		c3.getMap().put(5, c4);
		c3.getMap().put(7, c5);
		c3.getMap().put(0, c6);
		c4.getMap().put(5, c3);
		c4.getMap().put(6, c5);
		c5.getMap().put(6, c4);
		c5.getMap().put(7, c3);
		c6.getMap().put(0, c3);
		c6.getMap().put(8, c2);
		c7.getMap().put(2, c1);
		c7.getMap().put(1, c2);
		
		//cont2 has parallel tracks
		Track s1 = new Track (9, c6, c3, 1, 2, false, TrackKind.BLACK);
		Track s2 = new Track (10, c2, c1, 1, 2, true, TrackKind.ALL);
		Track s3 = new Track (11, c1, c7, 1, 2, false, TrackKind.RED);
		c6.getMap().put(9, c3);
		c3.getMap().put(9, c6);
		c2.getMap().put(10, c1);
		c1.getMap().put(10, c2);
		c1.getMap().put(11, c7);
		c7.getMap().put(11, c1);
		board.getListTracks().add(s1);
		board.getListTracks().add(s2);
		board.getListTracks().add(s3);
		s1.setOwnerId(1);
		s2.setOwnerId(1);
		s3.setOwnerId(1);
		cont2.getListTracks().add(s1);
		cont2.getListTracks().add(s2);
		cont2.getListTracks().add(s3);
		
		/*
		Object [] args = new Object [5];
		args[0] = c1;
		args[1] = new HashSet<City>();
		args[2] = new ArrayList<Integer>();
		args[3] = 0;
		args[4] = 0;
		int length = (int)method.invoke(EndGame.newInstance(), args);
		*/

		int length = EG.findLongestTrack(c6, new HashSet<City>(), new ArrayList<Integer>(), 0,0);
		assertEquals ("WrongLength", 8, length);
		length = EG.findLongestTrack(c7, new HashSet<City>(), new ArrayList<Integer>(), 1,0);
		assertEquals (2,length);
		List <Contestant> list = EG.calculateMaxTrack();
		assertEquals ("Wrong size", 1, list.size());
		assertEquals ("Wrong contestant in list", 0, list.get(0).getId());
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

}
