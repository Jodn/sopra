package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.BuildTrack;
import de.unisaarland.sopra.Command;
import de.unisaarland.sopra.GetMissions;
import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.PayTunnel;
import de.unisaarland.sopra.Register;
import de.unisaarland.sopra.ReturnMissions;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TakeCard;
import de.unisaarland.sopra.TakeSecretCard;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class DecideCommandTests {

	private GameLogic gl;
	private Contestant cont;
	private City city1;
	private City city2;
	private Constructor<BuildTrack> btc;
	private Constructor<TakeCard> tcc;
	private Constructor<GetMissions> gm;
	private Constructor<ReturnMissions> rmc;
	private Constructor<TakeSecretCard> tscc;
	private Constructor<PayTunnel> ptc;
	private Server server;

	@Before
	public void setUp() throws IOException, NoSuchMethodException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		gl = new GameLogic(4330, null, 33, 1);
		cont = new Contestant(0, "Gil Galad", 20);
		gl.addContestant(cont);
		gl.setOpenResources(new ArrayList<TrackKind>());
		gl.setrClosedStack(new ArrayList<TrackKind>());
		city1 = new City("city1", 0, 2, 2);
		city2 = new City("city2", 1, 3, 3);
		gl.getBoard().getListCities().add(city1);
		gl.getBoard().getListCities().add(city2);
		Class<?>[] c = new Class[4];
		c[0] = int.class;
		c[1] = short.class;
		c[2] = TrackKind.class;
		c[3] = int.class;
		btc = BuildTrack.class.getDeclaredConstructor(c);
		btc.setAccessible(true);
		Class<?>[] o = new Class[2];
		o[0] = int.class;
		o[1] = TrackKind.class;
		tcc = TakeCard.class.getDeclaredConstructor(o);
		tcc.setAccessible(true);
		Class<?>[] a = new Class[1];
		a[0] = int.class;
		gm = GetMissions.class.getDeclaredConstructor(a);
		gm.setAccessible(true);
		Class<?>[] b = new Class[2];
		b[0] = int.class;
		b[1] = Mission[].class;
		rmc = ReturnMissions.class.getDeclaredConstructor(b);
		rmc.setAccessible(true);
		Class<?>[] f = new Class[2];
		f[0] = int.class;
		f[1] = TrackKind.class;
		tcc = TakeCard.class.getDeclaredConstructor(f);
		tcc.setAccessible(true);
		Class<?>[] d = new Class[1];
		d[0] = int.class;
		tscc = TakeSecretCard.class.getDeclaredConstructor(d);
		tscc.setAccessible(true);
		Class<?>[] e = new Class[2];
		e[0] = int.class;
		e[1] = int.class;
		ptc = PayTunnel.class.getDeclaredConstructor(e);
		ptc.setAccessible(true);

		gl.setTurnsLeft(2);
		gl.setMaxHandCards(100);

		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}

	@After
	public void after() throws Exception {
		server.close();
	}

	// If openStack has less than 5 cards and rClosed Stack isn't empty draw
	// open cards
	@Test
	public void testFillOpenStack() throws Exception {
		for (int i = 0; i < 10; i++) {
			cont.getHandCards().add(TrackKind.ALL);
		}
		cont.setNumFails(1);
		Track track = new Track(0, city1, city2, 4, 7, false, TrackKind.BLACK);
		gl.getBoard().getListTracks().add(track);
		Command bt = btc.newInstance(0, (short) track.getId(), TrackKind.BLACK, 4);
		gl.decideCommand(bt);
		assertTrue("Didn't fill open stack", gl.getOpenResources().size() > 0);
		assertEquals("Not right num of cards in open Stack", 4, gl.getOpenResources().size());
		assertEquals("NumFails wasn't set to 0", 0, cont.getNumFails());
	}

	@Test
	public void testDecideCommand() throws Exception {
		Contestant cont2 = new Contestant(1, "Elendil", 20);
		gl.addContestant(cont2);
		for (int i = 0; i < 5; i++) {
			gl.getOpenResources().add(TrackKind.BLACK);
		}
		Command tc = tcc.newInstance(0, TrackKind.BLACK);
		gl.decideCommand(tc);
		assertEquals("Wrong Turnsleft", 1, gl.getTurnsLeft());
		assertEquals("Wrong order of playerlist", 0, gl.getPlayerList().get(0).getId());
		gl.decideCommand(tc);
		assertEquals("Didn't change order of playerlist", 1, gl.getPlayerList().get(0).getId());
		assertEquals("Wrong playerturn", 2, gl.getTurnsLeft());
	}

	@Test
	public void testWrongPlayerTurn() throws Exception {
		Contestant cont2 = new Contestant(1, "Elendil", 20);
		gl.addContestant(cont2);
		for (int i = 0; i < 5; i++) {
			gl.getOpenResources().add(TrackKind.BLACK);
		}
		Command tc = tcc.newInstance(1, TrackKind.BLACK);
		gl.decideCommand(tc);
		assertFalse("execute although wrong player", cont2.getHandCards().contains(TrackKind.BLACK));
		assertEquals("wrong num of handcards", 0, cont2.getHandCards().size());
	}

	@Test
	public void testIllegalCommands() throws Exception {
		Contestant cont2 = new Contestant(1, "Elendil", 20);
		gl.addContestant(cont2);
		// cont.setNumFails(0);
		// cont2.setNumFails(0);
		for (int i = 0; i < 5; i++) {
			gl.getOpenResources().add(TrackKind.BLACK);
		}
		Track track = new Track(0, city1, city2, 4, 7, false, TrackKind.BLACK);
		gl.getBoard().getListTracks().add(track);
		Command bt = btc.newInstance(0, (short) 0, TrackKind.BLACK, 4);
		gl.decideCommand(bt);
		assertEquals("NumFails wasn't increased", 1, cont.getNumFails());
		assertEquals("Illegal BuildTrack command was executed", -1, gl.getBoard().getListTracks().get(0).getOwnerId());
		Command tc = tcc.newInstance(1, TrackKind.BLACK);
		gl.decideCommand(tc);
		assertEquals("NumFails wasn't increased", 1, cont2.getNumFails());
		assertFalse("Illegal TakeCard command was executed", cont2.getHandCards().contains(TrackKind.BLACK));
		gl.decideCommand(tc);
		assertEquals("NumFails wasn't increased", 2, cont2.getNumFails());
		assertFalse("Illegal TakeCard command was executed", cont2.getHandCards().contains(TrackKind.BLACK));
		Command tc2 = tcc.newInstance(0, TrackKind.BLACK);
		gl.decideCommand(tc2);
		assertTrue("command wasn't executed", cont.getHandCards().contains(TrackKind.BLACK));
		assertEquals("NumFails wasn't set to 0", 0, cont.getNumFails());
		assertEquals("wrong TurnsLeft", 1, gl.getTurnsLeft());
	}

	@Test
	public void testAllCommands() throws Exception {
		// Missionen und rClosedStack
		for (int i = 0; i < 10; i++) {
			gl.getrClosedStack().add(TrackKind.BLACK);
			gl.getrClosedStack().add(TrackKind.RED);
		}
		Collections.sort(gl.getrClosedStack());
		Track track = new Track(0, city1, city2, 4, 7, true, TrackKind.RED);
		gl.getBoard().getListTracks().add(track);
		for (int i = 2; i < 8; i++) {
			gl.getBoard().getListCities().add(new City("city " + i, i, i, i));
			gl.getBoard().getListTracks().add(new Track(i - 1, gl.getBoard().getListCities().get(i - 1),
					gl.getBoard().getListCities().get(i), i, i, false, TrackKind.BLACK));
			gl.getmClosedStack().add(new MissionCard(gl.getBoard().getListCities().get(i - 1),
					gl.getBoard().getListCities().get(i - 2), 6, i));
		}
		Contestant cont2 = new Contestant(1, "Elendil", 20);
		Contestant cont3 = new Contestant(2, "Isildur", 20);
		Contestant cont4 = new Contestant(3, "Elendur", 20);

		gl.addContestant(cont2);
		gl.addContestant(cont3);
		gl.addContestant(cont4);
		for (int i = 0; i < 5; i++) {
			gl.getOpenResources().add(TrackKind.WHITE);
		}

		// GetMissions Command
		Command getm = gm.newInstance(0);
		int size = gl.getmClosedStack().size();
		gl.decideCommand(getm);
		assertEquals("Player has no tmp missions", 3, gl.getPlayerList().get(0).getTempMissions().size());
		assertEquals("Missions weren't remove from stack", size - 3, gl.getmClosedStack().size());

		// ReturnMissionCommand
		Mission[] miss = { new Mission((short) cont.getTempMissions().get(0).getCity1().getId(),
				(short) cont.getTempMissions().get(0).getCity2().getId(), cont.getTempMissions().get(0).getPoints()) };
		Command rm = rmc.newInstance(0, miss);
		size = gl.getmClosedStack().size();
		gl.decideCommand(rm);
		assertEquals("Mission wasn't returned", size + 1, gl.getmClosedStack().size());
		assertEquals("Contestant has wrong num of missions", 2, cont.getListMissions().size());
		assertEquals("tmpMissions isn't empty", 0, cont.getTempMissions().size());
		assertEquals("Order of Playerlist wasn't changed", 1, gl.getPlayerList().get(0).getId());

		// TakeCardCommand
		Command tc = tcc.newInstance(1, TrackKind.WHITE);
		size = gl.getrClosedStack().size();
		gl.decideCommand(tc);
		assertEquals("card wasn't added to handcards", 1, cont2.getHandCards().size());
		assertEquals("OpenStack wasn't filled", 5, gl.getOpenResources().size());
		assertTrue("Card wasn't removed from open stack",
				gl.getOpenResources().contains(TrackKind.BLACK) || gl.getOpenResources().contains(TrackKind.RED));
		assertEquals("No card removed from rClosedStack", size - 1, gl.getrClosedStack().size());
		assertEquals("Order of Playerlist was changed", 1, gl.getPlayerList().get(0).getId());

		// TakeSecretCard Command
		Command tsc = tscc.newInstance(1);
		size = gl.getrClosedStack().size();
		gl.decideCommand(tsc);
		assertEquals("Card wasn't added to handcards", 2, cont2.getHandCards().size());
		assertEquals("Card wasn't removed from rClosedStack", size - 1, gl.getrClosedStack().size());
		assertEquals("Order of Playerlist wasn't changed", 2, gl.getPlayerList().get(0).getId());

		// BuildTrack Command
		cont3.getHandCards().add(TrackKind.BLACK);
		cont3.getHandCards().add(TrackKind.BLACK);
		Command bt = btc.newInstance(2, (short) 1, TrackKind.BLACK, 0);
		size = gl.getrClosedStack().size();
		gl.decideCommand(bt);
		assertEquals("Track wasn't build", 2, gl.getBoard().getListTracks().get(1).getOwnerId());
		assertTrue("Track wasn't added to Contestant",
				cont3.getListTracks().contains(gl.getBoard().getListTracks().get(1)));
		assertEquals("Resources weren't removed from handcards", 0, cont3.getHandCards().size());
		assertEquals("Trains weren't removed", 18, cont3.getTrainsLeft());
		assertEquals("Points weren't increased", 2, cont3.getPoints());
		assertEquals("Resources weren't added to rClosedStack", size + 2, gl.getrClosedStack().size());
		assertEquals("Order of Playerlist wasn't changed", 3, gl.getPlayerList().get(0).getId());

		// Build Tunnel
		for (int i = 0; i < 6; i++) {
			cont4.getHandCards().add(TrackKind.RED);
		}
		cont4.getHandCards().add(TrackKind.ALL);
		bt = btc.newInstance(3, (short) 0, TrackKind.RED, 1);
		gl.decideCommand(bt);
		assertTrue("AddCosts wasn't set", cont4.getAddCosts() >= 0);
		assertEquals("Wrong TunnelTrack", cont4.getTunnelID(), gl.getBoard().getListTracks().get(0).getId());
		assertTrue("Wrong TunnelKind", cont4.getTunnelKind().equals(gl.getBoard().getListTracks().get(0).getColor()));
		assertEquals("Resources were removed too early", 7, cont4.getHandCards().size());
		assertEquals("Order of Playerlist was changed", 3, gl.getPlayerList().get(0).getId());

		// PayTunnel Command
		cont4.setAddCosts(3);
		Command pc = ptc.newInstance(3, 0);
		size = gl.getrClosedStack().size();
		gl.decideCommand(pc);
		assertEquals("Track OwnerId wasn't changed", 3, gl.getBoard().getListTracks().get(0).getOwnerId());
		assertTrue("Track wasn't added to contestant",
				cont4.getListTracks().contains(gl.getBoard().getListTracks().get(0)));
		assertEquals("Resources weren't removed from handcards", 0, cont4.getHandCards().size());
		assertEquals("Trains weren't removed", 16, cont4.getTrainsLeft());
		assertEquals("Points weren't added to contestant", 7, cont4.getPoints());
		assertEquals("Resources weren't added to rClosedStack", size + 7, gl.getrClosedStack().size());
		assertEquals("Order of Playerlist wasn't changed", 0, gl.getPlayerList().get(0).getId());

		// GetMissions Command
		getm = gm.newInstance(0);
		size = gl.getmClosedStack().size();
		gl.decideCommand(getm);
		assertEquals("Player has no tmp missions", 3, gl.getPlayerList().get(0).getTempMissions().size());
		assertEquals("Missions weren't remove from stack", size - 3, gl.getmClosedStack().size());

		// Illegal TakeCardCommand
		tc = tcc.newInstance(0, TrackKind.WHITE);
		gl.decideCommand(tc);
		assertEquals("NumFails wasn't increased", 1, cont.getNumFails());
		assertFalse("Card was added to handcards", cont.getHandCards().contains(TrackKind.WHITE));

		// Try to return wrong missions
		Mission[] miss2 = { new Mission((short) cont.getListMissions().get(0).getCity1().getId(),
				(short) cont.getListMissions().get(0).getCity2().getId(), cont.getListMissions().get(0).getPoints()) };
		rm = rmc.newInstance(0, miss2);
		size = gl.getmClosedStack().size();
		gl.decideCommand(rm);
		assertEquals("Numfails wasn't increased", 2, cont.getNumFails());
		assertEquals("Return Mission wasn't executed", 3, cont.getTempMissions().size());
		assertEquals("Mission wasn't return to mClosedStack", size, gl.getmClosedStack().size());
		assertEquals("Order of Playerlist was changed", 0, gl.getPlayerList().get(0).getId());
	}

	@Test
	public void testIllegalCommands2() throws Exception {
		for (int i = 0; i < 10; i++) {
			gl.getrClosedStack().add(TrackKind.BLACK);
			gl.getrClosedStack().add(TrackKind.RED);
		}
		Collections.sort(gl.getrClosedStack());
		Track track = new Track(0, city1, city2, 4, 7, true, TrackKind.RED);
		gl.getBoard().getListTracks().add(track);
		for (int i = 2; i < 6; i++) {
			gl.getBoard().getListCities().add(new City("city " + i, i, i, i));
			gl.getBoard().getListTracks().add(new Track(i - 1, gl.getBoard().getListCities().get(i - 1),
					gl.getBoard().getListCities().get(i), i, i, false, TrackKind.BLACK));
			gl.getmClosedStack().add(new MissionCard(gl.getBoard().getListCities().get(i - 1),
					gl.getBoard().getListCities().get(i - 2), 6, i));
		}
		Contestant cont2 = new Contestant(1, "Elendil", 20);
		Contestant cont3 = new Contestant(2, "Isildur", 20);
		Contestant cont4 = new Contestant(3, "Elendur", 20);
		gl.addContestant(cont2);
		gl.addContestant(cont3);
		gl.addContestant(cont4);
		for (int i = 0; i < 4; i++) {
			gl.getOpenResources().add(TrackKind.WHITE);
		}
		gl.getOpenResources().add(TrackKind.ALL);

		// Return Mission Command
		Mission[] miss = { new Mission((short) gl.getmClosedStack().get(0).getCity1().getId(),
				(short) gl.getmClosedStack().get(0).getCity2().getId(), gl.getmClosedStack().get(0).getPoints()) };
		Command rm = rmc.newInstance(0, miss);
		int size = gl.getmClosedStack().size();
		gl.decideCommand(rm);
		assertEquals("NumFails wasn't increased", 1, cont.getNumFails());
		assertEquals("Missions were removed", size, gl.getmClosedStack().size());
		assertEquals("Order of Playerlist was changed", 0, gl.getPlayerList().get(0).getId());

		// BuildTrack Command without handcards
		Command bt = btc.newInstance(0, (short) 1, TrackKind.BLACK, 0);
		size = gl.getrClosedStack().size();
		gl.decideCommand(bt);
		assertEquals("NumFails wasn't increased", 2, cont.getNumFails());
		assertEquals("Track was built", -1, gl.getBoard().getListTracks().get(0).getOwnerId());
		assertFalse("Track was added to Contestant",
				cont.getListTracks().contains(gl.getBoard().getListTracks().get(0)));
		assertEquals(0, cont.getHandCards().size());
		assertEquals("Trains were removed", 20, cont.getTrainsLeft());
		assertEquals("Points were increased", 0, cont.getPoints());
		assertEquals("Resources were added to rClosedStack", size, gl.getrClosedStack().size());
		assertEquals("Order of Playerlist was changed", 0, gl.getPlayerList().get(0).getId());

		// Register Command
		Class<?>[] r = new Class[2];
		r[0] = int.class;
		r[1] = String.class;
		Constructor<Register> regC = Register.class.getDeclaredConstructor(r);
		regC.setAccessible(true);
		Command reg = regC.newInstance(0, "Blub");
		gl.decideCommand(reg);
		assertEquals("NumFails wasn't increased", 3, cont.getNumFails());
		assertTrue("Name was changed", cont.getName().equals("Gil Galad"));

		// TakeCardCommand with wrong kind
		Command tc = tcc.newInstance(0, TrackKind.GREEN);
		gl.decideCommand(tc);
		assertFalse("Player wasn't removed from list", gl.getPlayerList().contains(cont));
		assertFalse("Player wasn't removed from map", gl.getPlayerMap().containsKey(0));
		assertEquals("Order of Playerlist wasn't changed", 1, gl.getPlayerList().get(0).getId());

		// Try to take loc after TakeSecretCardCommand
		Command tsc = tscc.newInstance(1);
		size = gl.getrClosedStack().size();
		gl.decideCommand(tsc);
		assertEquals("Card wasn't added to contestant", 1, cont2.getHandCards().size());
		assertEquals("Card wasn't removed from stack", size - 1, gl.getrClosedStack().size());
		assertEquals("TurnsLeft wasn't decreased", 1, gl.getTurnsLeft());
		assertEquals("Order of Playerlist was changed", 1, gl.getPlayerList().get(0).getId());

		// Try to take loc
		Command tcl = tcc.newInstance(1, TrackKind.ALL);
		gl.decideCommand(tcl);
		assertEquals("NumFails wasn't increased", 1, cont2.getNumFails());
		assertTrue("Loc was removed from openStack", gl.getOpenResources().contains(TrackKind.ALL));
		assertEquals("Wrong num of handcards", 1, cont2.getHandCards().size());
		assertEquals("Order of Playerlist was changed", 1, gl.getPlayerList().get(0).getId());

		// Wrong Player tries to take card
		tc = tcc.newInstance(2, TrackKind.WHITE);
		gl.decideCommand(tc);
		assertEquals("NumFails wasn't increased", 1, cont3.getNumFails());
		assertEquals("NumFails of wrong player was increased", 1, cont2.getNumFails());
		assertEquals("Card was added to handcards", 0, cont3.getHandCards().size());
		assertEquals("Order of Playerlist was changed", 1, gl.getPlayerList().get(0).getId());

		// Right Player takes card
		tc = tcc.newInstance(1, TrackKind.WHITE);
		gl.decideCommand(tc);
		assertEquals("Card wasn't added to handcards", 2, cont2.getHandCards().size());
		assertEquals("NumFails wasn't set to 0", 0, cont2.getNumFails());
		assertEquals("Open stack wasn't filled", 5, gl.getOpenResources().size());
		assertEquals("Order of Playerlist wasn't changed", 2, gl.getPlayerList().get(0).getId());
		assertEquals("NumFails of Player with id 2", 1, cont3.getNumFails());

		// Only one loc left in openStack
		gl.getrClosedStack().clear();
		gl.getOpenResources().clear();
		gl.getOpenResources().add(TrackKind.ALL);

		// try to take loc
		tc = tcc.newInstance(2, TrackKind.ALL);
		gl.decideCommand(tc);
		assertEquals("NumFails weren't increased", 2, cont3.getNumFails());
		assertTrue("Loc was remove from openstack", gl.getOpenResources().contains(TrackKind.ALL));
		assertFalse("Loc was added to contestant", cont3.getHandCards().contains(TrackKind.ALL));

		// Two cards left on open stack (one loc)
		gl.getOpenResources().add(TrackKind.BLACK);

		// Take first card
		tc = tcc.newInstance(2, TrackKind.BLACK);
		gl.decideCommand(tc);
		assertEquals("Wrong num fails", 0, cont3.getNumFails());
		assertTrue("Card wasn't added to handcards", cont3.getHandCards().contains(TrackKind.BLACK));
		assertFalse("card wasn't removed from open stack", gl.getOpenResources().contains(TrackKind.BLACK));
		assertEquals("Wrong size of open stack", 1, gl.getOpenResources().size());
		assertEquals("Turnsleft wasn't decreased", 1, gl.getTurnsLeft());
		assertEquals("Order of Playerlist was changed", 2, gl.getPlayerList().get(0).getId());

		// take loc as second card
		tc = tcc.newInstance(2, TrackKind.ALL);
		gl.decideCommand(tc);
		assertEquals("Wrong num fails", 0, cont3.getNumFails());
		assertTrue("Card wasn't added to handcards", cont3.getHandCards().contains(TrackKind.ALL));
		assertEquals("Wrong size of open stack", 0, gl.getOpenResources().size());
		assertEquals("Order of Playerlist wasn't changed", 3, gl.getPlayerList().get(0).getId());

		// Illegal PayTunnel Command
		Command pt = ptc.newInstance(3, 0);
		gl.decideCommand(pt);
		assertEquals("Wrong numfails", 1, cont4.getNumFails());

		// Try to build tunnel
		bt = btc.newInstance(3, (short) 0, TrackKind.RED, 1);
		for (int i = 0; i < 3; i++) {
			cont4.getHandCards().add(TrackKind.RED);
		}
		cont4.getHandCards().add(TrackKind.ALL);
		cont4.getHandCards().add(TrackKind.ALL);
		cont4.getHandCards().add(TrackKind.ALL);
		gl.decideCommand(bt);
		assertEquals("Track was build", -1, gl.getBoard().getListTracks().get(0).getOwnerId());
		assertTrue("AddCosts are not set", cont4.getAddCosts() != -1);
		assertTrue("Tunnel infos not set", cont4.getTunnelKind().equals(TrackKind.RED) && cont4.getTmpLocs() == 1);
		assertEquals("Handcards were already removed", 6, cont4.getHandCards().size());
		assertEquals("NumFails wasn't set to 0", 0, cont4.getNumFails());

		// Wrong Command
		gl.getOpenResources().add(TrackKind.RED);
		tc = tcc.newInstance(3, TrackKind.RED);
		gl.decideCommand(tc);
		assertEquals("Numfails wasn't increased", 1, cont4.getNumFails());
		assertEquals("Wrong size of handcards", 6, cont4.getHandCards().size());
		assertFalse("Card was removed from open stack", gl.getOpenResources().isEmpty());

		// PayTunnel with wrong add Costs
		cont4.setAddCosts(2);
		pt = ptc.newInstance(3, 0);
		gl.decideCommand(pt);
		assertEquals("NumFails wasn't increased", 2, cont4.getNumFails());
		assertEquals("Track was build", -1, gl.getBoard().getListTracks().get(0).getOwnerId());
		assertEquals("AddCosts set to -1", 2, cont4.getAddCosts());
		assertEquals("handcards were removed", 6, cont4.getHandCards().size());

		// Legal PayTunnelCommand
		pt = ptc.newInstance(3, 2);
		gl.decideCommand(pt);
		assertEquals("NumFails wasn't set to 0", 0, cont4.getNumFails());
		assertEquals("Tunnel wasn't build", 3, gl.getBoard().getListTracks().get(0).getOwnerId());
		assertEquals("AddCosts wasn't set to -1", -1, cont4.getAddCosts());
		assertEquals("Handcards weren't removed", 0, cont4.getHandCards().size());
		assertEquals("Order of Playerlist wasn't changed", 1, gl.getPlayerList().get(0).getId());
		Field PTfield = GameLogic.class.getDeclaredField("nextPayTunnel");
		PTfield.setAccessible(true);
		boolean nextpt = (boolean) PTfield.get(gl);
		assertFalse("NextPayTunnel wasn't set false", nextpt);
	}

	// Tunnel can't build (can't pay addcosts)
	@Test
	public void testTunnelNotBuild() throws Exception {
		Contestant cont2 = new Contestant(1, "Player2", 50);
		gl.addContestant(cont2);
		for (int i = 0; i < 6; i++) {
			cont.getHandCards().add(TrackKind.BLACK);
			gl.getrClosedStack().add(TrackKind.BLACK);
		}
		Track track = new Track(0, city1, city2, 6, 10, true, TrackKind.BLACK);
		gl.getBoard().getListTracks().add(track);
		city1.getMap().put(0, city2);
		city2.getMap().put(0, city1);
		Command bt = btc.newInstance(0, (short) 0, TrackKind.BLACK, 0);
		gl.decideCommand(bt);
		Field PTfield = GameLogic.class.getDeclaredField("nextPayTunnel");
		PTfield.setAccessible(true);
		boolean nextpt = (boolean) PTfield.get(gl);
		assertFalse("PayTunnel was set", nextpt);
		assertEquals("Order of Playerlist wasn't changed", 1, gl.getPlayerList().get(0).getId());
		assertEquals("AddCosts was set", -1, cont.getAddCosts());
	}

	@Test
	public void testReachMaxHandcards() throws Exception {
		// Handcards == MaxHandcards -1
		gl.setMaxHandCards(5);
		for (int i = 0; i < 4; i++) {
			cont.getHandCards().add(TrackKind.RED);
		}
		for (int i = 0; i < 5; i++) {
			gl.getrClosedStack().add(TrackKind.BLACK);
			gl.getOpenResources().add(TrackKind.RED);
		}
		Command tc = tcc.newInstance(0, TrackKind.RED);
		gl.decideCommand(tc);
		assertEquals("NumFails wasn't increased", 1, cont.getNumFails());
		assertFalse("Card was removed", gl.getOpenResources().contains(TrackKind.BLACK));
		assertEquals("card was added to handcards", 4, cont.getHandCards().size());
		assertEquals("Order of Playerlist was changed", 0, gl.getPlayerList().get(0).getId());

		// Reached MaxHandcards
		cont.getHandCards().add(TrackKind.RED);
		tc = tcc.newInstance(0, TrackKind.RED);
		gl.decideCommand(tc);
		assertEquals("NumFails wasn't increased", 2, cont.getNumFails());
		assertFalse("Card was removed", gl.getOpenResources().contains(TrackKind.BLACK));
		assertEquals("card was added to handcards", 5, cont.getHandCards().size());
		assertEquals("Order of Playerlist was changed", 0, gl.getPlayerList().get(0).getId());
	}

	@Test
	public void testTakeLastCards() throws Exception {
		// Only one card left
		gl.getOpenResources().add(TrackKind.BLACK);
		Command tc = tcc.newInstance(0, TrackKind.BLACK);
		gl.decideCommand(tc);
		assertEquals("NumFails wasn't increased", 1, cont.getNumFails());
		assertTrue("Card was removed",
				gl.getOpenResources().contains(TrackKind.BLACK) && gl.getOpenResources().size() == 1);
		assertEquals("card was added to handcards", 0, cont.getHandCards().size());

		// Last card is loc
		gl.getOpenResources().clear();
		gl.getOpenResources().add(TrackKind.ALL);
		tc = tcc.newInstance(0, TrackKind.ALL);
		gl.decideCommand(tc);
		assertEquals("NumFails wasn't increased", 2, cont.getNumFails());
		assertTrue("Card was removed", gl.getOpenResources().size() == 1);
		assertEquals("card was added to handcards", 0, cont.getHandCards().size());

		// TakeSecretCards with empty rClosedStack
		Command tsc = tscc.newInstance(0);
		gl.decideCommand(tsc);
		assertEquals("NumFails wasn't increased", 3, cont.getNumFails());
		assertEquals("card was added to handcards", 0, cont.getHandCards().size());
	}

	@Test
	public void testTryToBuildTrackwrongColor() throws Exception {
		for (int i = 0; i < 6; i++) {
			cont.getHandCards().add(TrackKind.RED);
		}
		Track track = new Track(0, city1, city2, 4, 7, false, TrackKind.BLACK);
		gl.getBoard().getListTracks().add(track);
		assertEquals("Wrong fails", 0, cont.getNumFails());
		Command bt = btc.newInstance(0, (short) track.getId(), TrackKind.RED, 0);
		gl.decideCommand(bt);
		assertEquals("Num Fails wasn't increased", 1, cont.getNumFails());
		assertEquals("Track was build", -1, track.getOwnerId());
		// bt = btc.newInstance(0, (short) track.getId(), TrackKind.ALL, 0);
		gl.decideCommand(bt);
		assertEquals("Num Fails wasn't increased", 2, cont.getNumFails());
		assertEquals("Track was build", -1, track.getOwnerId());
		// bt = btc.newInstance(0, (short) track.getId(), TrackKind.BLACK, 4);
		gl.decideCommand(bt);
		assertEquals("Num Fails wasn't increased", 3, cont.getNumFails());
		assertEquals("Track was build", -1, track.getOwnerId());
	}

	@Test
	public void testOnlyTwoCardsLeft()
			throws Exception, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// One loc, take resource first
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);
		Command tc = tcc.newInstance(0, TrackKind.BLACK);
		gl.decideCommand(tc);
		assertEquals("Card wasn't added to handcards", 1, cont.getHandCards().size());
		assertEquals("Card wasn't removed from open stack", 1, gl.getOpenResources().size());
		assertEquals("Wrong card", TrackKind.BLACK, cont.getHandCards().get(0));
		assertEquals("Wrong turnsleft", 1, gl.getTurnsLeft());
		assertEquals("Order of Playerlist was changed", 0, gl.getPlayerList().get(0).getId());

		// Take loc
		tc = tcc.newInstance(0, TrackKind.ALL);
		gl.decideCommand(tc);
		assertEquals("Card wasn't added to handcards", 2, cont.getHandCards().size());
		assertEquals("Card wasn't removed from open stack", 0, gl.getOpenResources().size());
	}
	
	@Test
	public void testBuildTunnel () throws Exception{
		gl.getrClosedStack().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.RED);
		Track track = new Track(0, city1, city2, 4, 7, true, TrackKind.BLACK);
		gl.getBoard().getListTracks().add(track);
		for (int i = 0; i<5; i++){
		cont.getHandCards().add(TrackKind.BLACK);
		}
		cont.getHandCards().add(TrackKind.ALL);
		cont.getHandCards().add(TrackKind.ALL);
		cont.getHandCards().add(TrackKind.ALL);
		Command bt = btc.newInstance(cont.getId(), (short)track.getId(), TrackKind.BLACK, 2);
		gl.decideCommand(bt);
		assertEquals ("Owner ID was set", -1, track.getOwnerId());
		assertEquals ("Handcards were removed", 8, cont.getHandCards().size());
		assertFalse (cont.getListTracks().contains(track));
		assertEquals ("AddCosts wasn't set", 2, cont.getAddCosts());
		assertEquals ("tunnelId wasn't set", 0, cont.getTunnelID());
		Command pt = ptc.newInstance(cont.getId(), 0);
		gl.decideCommand(pt);
		assertEquals ("Track wasn't build", 0, track.getOwnerId());
		assertEquals ("Wrong handcards", 2, cont.getHandCards().size());
	}
	
	@Test
	public void testBuildGreyTrack() throws Exception{
		gl.getrClosedStack().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.RED);
		Track track = new Track(0, city1, city2, 4, 7, true, TrackKind.ALL);
		gl.getBoard().getListTracks().add(track);
		for (int i = 0; i<5; i++){
		cont.getHandCards().add(TrackKind.BLACK);
		}
		cont.getHandCards().add(TrackKind.ALL);
		cont.getHandCards().add(TrackKind.ALL);
		cont.getHandCards().add(TrackKind.ALL);
		Command bt = btc.newInstance(cont.getId(), (short)track.getId(), TrackKind.BLACK, 2);
		gl.decideCommand(bt);
		assertEquals ("Owner ID was set", -1, track.getOwnerId());
		assertEquals ("Handcards were removed", 8, cont.getHandCards().size());
		assertFalse (cont.getListTracks().contains(track));
		assertEquals ("AddCosts wasn't set", 2, cont.getAddCosts());
		assertEquals ("tunnelId wasn't set", 0, cont.getTunnelID());
		Command pt = ptc.newInstance(cont.getId(), 0);
		gl.decideCommand(pt);
		assertEquals ("Track wasn't build", 0, track.getOwnerId());
		assertEquals ("Wrong handcards", 2, cont.getHandCards().size());
	}

}
