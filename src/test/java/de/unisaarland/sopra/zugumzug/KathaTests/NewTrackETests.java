package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;

import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.events.NewTrackEvent;

public class NewTrackETests {

	private NewTrackEvent TE;
	private Board board;
	private City city1, city2;
	private int ID;

	@Before
	public void setUp() {
		this.ID = 0;
		this.city1 = new City("Thal", 2, 3, 4);
		this.city2 = new City("Isengart", 5, 6, 7);
		this.board = new Board();
		this.TE = new NewTrackEvent(ID, 3, city1, city2, board, TrackKind.GREEN, false);
	}

	@Test
	public void testTrackAddedBoard() {
		int x = board.getListTracks().size();
		TE.execute();
		assertEquals("Track wasn't added", x + 1, board.getListTracks().size());
		assertEquals("Wrong track added", ID, board.getListTracks().get(x).getId());
		assertEquals("Wrong color", TrackKind.GREEN, board.getListTracks().get(x).getColor());
	}

	@Test
	public void testTrackAddedCity1() {
		int x = city1.getMap().size();
		TE.execute();
		assertEquals("Track wasn't added to city1", x + 1, city1.getMap().size());
		assertEquals("Track wasn't added to city1 map", "Isengart", city1.getMap().get(ID).getName());
	}

	@Test
	public void testTrackAddedCity2() {
		int x = city2.getMap().size();
		TE.execute();
		assertEquals("Track wasn't added to City2", x + 1, city2.getMap().size());
		assertEquals("Track wasn't added to city2 map", "Thal", city2.getMap().get(ID).getName());
	}

	@Test
	public void testIdAlreadyExists() {
		City city3 = new City("Dol Amroth", 1, 1, 1);
		City city4 = new City("Moria", 2, 2, 2);
		Track track = new Track(this.ID, city3, city4, 6, 5, false, TrackKind.BLACK);
		board.getListTracks().add(track);
		city3.getMap().put(this.ID, city4);
		city4.getMap().put(this.ID, city3);
		int x = board.getListTracks().size();
		TE.execute();
		assertEquals("Track with same ID added", x, board.getListTracks().size());
		assertEquals("Wrong city1", "Dol Amroth", board.getListTracks().get(0).getCity1().getName());
		assertEquals("Wrong city2", "Moria", board.getListTracks().get(0).getCity2().getName());
	}

	@Test
	public void testIllegalId() {
		NewTrackEvent TE2 = new NewTrackEvent(-2, 3, city1, city2, board, TrackKind.GREEN, false);
		int x = board.getListTracks().size();
		int y = city1.getMap().size();
		int z = city2.getMap().size();
		TE2.execute();
		assertEquals("Track with illegal ID added", x, board.getListTracks().size());
		assertEquals("Track with illegal ID added in city1", y, city1.getMap().size());
		assertEquals("Track with illegal ID added in city2", z, city2.getMap().size());
	}
}
