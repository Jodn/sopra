package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.events.NewCardsEvent;

public class NewCardsETests {

	private NewCardsEvent CE;
	private Contestant cont;

	@Before
	public void setUp() {
		this.cont = new Contestant(2, "Aragorn", 10);
		this.CE = new NewCardsEvent(cont, TrackKind.BLUE, 1);
	}

	@Test
	public void testCardAddedToCont() {
		int x = cont.getHandCards().size();
		CE.execute();
		assertEquals("Card wasn't added to handcards", x + 1, cont.getHandCards().size());
		assertTrue("Wrong card added", cont.getHandCards().contains(TrackKind.BLUE));
	}
}
