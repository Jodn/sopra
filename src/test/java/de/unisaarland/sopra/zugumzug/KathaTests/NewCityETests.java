package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.events.NewCityEvent;

public class NewCityETests {

	private NewCityEvent CE;
	private Board board;
	private int ID, x, y;

	@Before
	public void setUp() {
		this.x = 2;
		this.y = 3;
		this.board = new Board();
		this.ID = 0;
		this.CE = new NewCityEvent(ID, "Gondor", x, y, board);
	}

	@Test
	public void testAddedCity() {
		int z = board.getListCities().size();
		CE.execute();
		assertEquals("City wasn't added", z + 1, board.getListCities().size());
		assertEquals("Wrong coordinate", x, board.getListCities().get(ID).getX());
		assertEquals("Wrong coordinate", y, board.getListCities().get(ID).getY());
		assertEquals("Wrong name", "Gondor", board.getListCities().get(ID).getName());
	}

	@Test
	public void testIDAlreadyExists() {
		City city2 = new City("Minas Tirith", 0, 2, 3);
		board.getListCities().add(city2);
		int z = board.getListCities().size();
		CE.execute();
		assertEquals("City with same ID added", z, board.getListCities().size());
		assertEquals("Wrong City", "Minas Tirith", board.getListCities().get(ID).getName());
	}

	@Test
	public void test1() {
		NewCityEvent CE2 = new NewCityEvent(2, "Gondor", x, y, board);
		City city1 = new City("Edoras", 0, 4, 5);
		City city2 = new City("Osgiliath", 1, 5, 7);
		board.getListCities().add(city1);
		board.getListCities().add(city2);
		CE2.execute();
		assertEquals("Wrong num of cities", 3, board.getListCities().size());
		assertEquals("Wrong City added", "Gondor", board.getListCities().get(2).getName());
	}

	@Test
	public void testIllegalID() {
		int x = board.getListCities().size();
		NewCityEvent CE2 = new NewCityEvent(-3, "Gondor", x, y, board);
		CE2.execute();
		assertEquals("City with illegal ID added", x, board.getListCities().size());
	}
}
