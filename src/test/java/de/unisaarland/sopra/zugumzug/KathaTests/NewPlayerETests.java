package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.events.NewPlayerEvent;

public class NewPlayerETests {

	private NewPlayerEvent PE;
	private List<Contestant> players;
	private HashMap<Integer, Contestant> contmap;
	private int PlayerID;

	@Before
	public void setUp() {
		this.PlayerID = 0;
		this.players = new ArrayList<Contestant>();
		this.contmap = new HashMap<Integer, Contestant>();
		this.PE = new NewPlayerEvent(PlayerID, "Galadriel", players, contmap);
	}

	@Test
	public void testAddedID() {
		assertEquals("Size of playerlist is not 0", 0, players.size());
		assertEquals("Size of contmap is not 0", 0, contmap.size());
		this.PE.execute();
		assertEquals("Wrong number of players", 1, players.size());
		assertEquals("Wrong number of players in map", 1, contmap.size());
		assertTrue("ID wasn't added", contmap.containsKey(PlayerID));
	}

	@Test
	public void testIDAlreadyExists() {
		Contestant con = new Contestant(PlayerID, "Morgoth", 6);
		this.players.add(con);
		this.contmap.put(PlayerID, con);
		assertEquals("Size of playerlist not 1", 1, players.size());
		assertEquals("Size of contmap not 1", 1, contmap.size());
		PE.execute();
		assertEquals("Contestant with same ID added", 1, players.size());
		assertEquals("Contestant with same ID added in map", 1, contmap.size());
		assertEquals("Name wasn't changed", "Galadriel", contmap.get(PlayerID).getName());
	}
}
