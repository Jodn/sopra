package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.zugumzug.events.NewOpenCardEvent;
import de.unisaarland.sopra.TrackKind;

public class NewOpenCardETests {

	private NewOpenCardEvent OCE;
	private ArrayList<TrackKind> opencards;

	@Before
	public void setUp() {
		opencards = new ArrayList<TrackKind>();
		OCE = new NewOpenCardEvent(TrackKind.BLUE, opencards);
	}

	@Test
	public void testCardAdded() {
		int x = opencards.size();
		assertTrue("More than 5 open cards", opencards.size() < 5);
		OCE.execute();
		assertEquals("Card wasn't added", x + 1, opencards.size());
		assertEquals("Wrong TrackKind", TrackKind.BLUE, opencards.get(x));
	}

	@Test
	public void testTooMuchOpenCards() {
		opencards.add(TrackKind.BLACK);
		opencards.add(TrackKind.BLACK);
		opencards.add(TrackKind.BLACK);
		opencards.add(TrackKind.BLACK);
		opencards.add(TrackKind.BLACK);
		OCE.execute();
		assertEquals("More than 5 open cards", 5, opencards.size());
	}
}
