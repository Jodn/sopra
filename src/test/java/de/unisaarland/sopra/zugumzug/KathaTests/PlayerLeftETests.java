package de.unisaarland.sopra.zugumzug.KathaTests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.events.PlayerLeftEvent;

public class PlayerLeftETests {

	private PlayerLeftEvent PLE;
	private List<Contestant> contlist;
	private HashMap<Integer, Contestant> contmap;
	private int ID;

	@Before
	public void setUp() {
		this.ID = 0;
		this.contlist = new ArrayList<Contestant>();
		this.contmap = new HashMap<Integer, Contestant>();
		this.PLE = new PlayerLeftEvent(this.ID, contlist);
	}

	@Test
	public void testRemoveContestant() {
		Contestant cont = new Contestant(this.ID, "Saruman", 7);
		contlist.add(cont);
		contmap.put(ID, cont);
		assertTrue("No Player left", contlist.size() > 0);
		PLE.execute();
		assertEquals("Didn't remove player", 0, contlist.size());
	}

	@Test
	public void testWrongId() {
		Contestant cont = new Contestant(2, "Sauron", 7);
		contlist.add(cont);
		contmap.put(2, cont);
		PLE.execute();
		assertEquals("Removed wrong player", 1, contlist.size());
	}

	@Test
	public void testRemoveRightContestant() {
		Contestant cont = new Contestant(this.ID, "Eomer", 7);
		Contestant cont2 = new Contestant(1, "Saruman", 7);
		contlist.add(cont);
		contlist.add(cont2);
		contmap.put(ID, cont);
		contmap.put(1, cont2);
		assertEquals(2, contlist.size());
		PLE.execute();
		assertEquals("Wrong num of players", 1, contlist.size());
		assertEquals("Removed wrong player", "Saruman", contlist.get(0).getName());
	}
}