package de.unisaarland.sopra.zugumzug.TestsMegan;

import org.junit.*;

import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.server.GameLogic;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.List;

public class GameLogicTests {

	static GameLogic gl;
	static Server server;

	@BeforeClass
	public static void setUp() throws Exception {
		gl = new GameLogic(0, "Maps/TaskMap.txt", 0, 1);
		gl.parseFile("Maps/TaskMap.txt");
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Test
	public void testBasics() {

		if (gl.getBoard() == null)
			assertTrue("Board missing", false);

		assertTrue("Did not parse Resource Cards correctly", gl.getrClosedStack().size() == 55);
		assertEquals("Max Tracks not init correctly", gl.getMaxTracks(), 8);
		assertTrue("Did not parse Cities correctly", gl.getBoard().getListCities().size() == 6);
		assertTrue("Did not parse Tracks correctly", gl.getBoard().getListTracks().size() == 8);
		assertEquals("Did not parse Missions correctly", gl.getmClosedStack().size(), 8);

	}

	// tests that setting up the Closed Stack worked correctly
	@Test
	public void testrClosedStack() {

		List<TrackKind> rStack = gl.getrClosedStack();
		for (int i = 0; i < 6; i++) {
			assertEquals(rStack.get(i), TrackKind.VIOLET);
		}

		for (int i = 6; i < 12; i++) {
			assertEquals(rStack.get(i), TrackKind.WHITE);
		}
		for (int i = 12; i < 18; i++) {
			assertEquals(rStack.get(i), TrackKind.BLUE);
		}
		for (int i = 18; i < 24; i++) {
			assertEquals(rStack.get(i), TrackKind.YELLOW);
		}
		for (int i = 24; i < 30; i++) {
			assertEquals(rStack.get(i), TrackKind.ORANGE);
		}
		for (int i = 30; i < 36; i++) {
			assertEquals(rStack.get(i), TrackKind.BLACK);
		}
		for (int i = 36; i < 42; i++) {
			assertEquals(rStack.get(i), TrackKind.RED);
		}
		for (int i = 42; i < 48; i++) {
			assertEquals(rStack.get(i), TrackKind.GREEN);
		}
		for (int i = 48; i < 55; i++) {
			assertEquals(rStack.get(i), TrackKind.ALL);
		}
	}

	// Tests for a random City (Bochum) if set up worked
	@Test
	public void testCityBochum() {
		City bochum = gl.getBoard().getListCities().get(2); // Bochum
		assertTrue("City coordinates don't match", bochum.getX() == 5);
		assertTrue("City coordinates don't match", bochum.getY() == 1);
		assertTrue("city name not parsed correctly", bochum.getName().equals("Bochum"));
		assertTrue("city num tracks wrong", bochum.getMap().size() == 3);

	}

	// tests if two random Tracks are set up correctly
	@Test
	public void testRandomTracks() {
		Track track1 = gl.getBoard().getListTracks().get(1);
		Track track5 = gl.getBoard().getListTracks().get(5);

		assertEquals(track1.getColor(), TrackKind.ALL);
		assertEquals(track5.getColor(), TrackKind.BLUE);

		assertEquals(track1.isTunnel(), false);
		assertEquals(track5.isTunnel(), false);

		assertEquals(track1.getLength(), 2);
		assertEquals(track5.getLength(), 4);

		assertEquals(track1.getPoints(), 2);
		assertEquals(track5.getPoints(), 7);

		if (track1.getCity1() == null || track1.getCity2() == null || track5.getCity1() == null
				|| track5.getCity2() == null)
			assertTrue(false);

		if (track1.getCity1().getId() != 1) {
			assertEquals(track1.getCity1().getId(), 2);
			assertEquals(track1.getCity2().getId(), 1);
		} else
			assertEquals(track1.getCity2().getId(), 2);

		if (track5.getCity1().getId() != 0) {
			assertEquals(track5.getCity1().getId(), 3);
			assertEquals(track5.getCity2().getId(), 0);
		} else
			assertEquals(track5.getCity2().getId(), 3);

	}

	@Test
	public void testRandomMission() {
		MissionCard mission2 = gl.getmClosedStack().get(2);
		MissionCard mission7 = gl.getmClosedStack().get(7);

		if (mission2.getCity1() == null || mission2.getCity2() == null || mission7.getCity1() == null
				|| mission7.getCity2() == null)
			assertTrue(false);

		if (mission2.getCity1().getId() != 0) {
			assertEquals(mission2.getCity1().getId(), 3);
			assertEquals(mission2.getCity2().getId(), 0);
		} else
			assertEquals(mission2.getCity2().getId(), 3);

		if (mission7.getCity1().getId() != 5) {
			assertEquals(mission7.getCity1().getId(), 1);
			assertEquals(mission7.getCity2().getId(), 5);
		} else
			assertEquals(mission7.getCity2().getId(), 1);

		assertEquals(mission2.getPoints(), 5);
		assertEquals(mission7.getPoints(), 6);

	}

}
