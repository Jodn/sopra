package de.unisaarland.sopra.zugumzug.TestsMegan;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Field;

import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.commands.BuildTrackCommand;
import de.unisaarland.sopra.zugumzug.commands.CommandP;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class BuildTrackCommandTest {

	private static GameLogic gl;
	private static Contestant Alice;
	private static Server server;
	private static MersenneTwister twister;
	
	@BeforeClass
	public static void setUp() throws Exception {
		gl= new GameLogic(4000, null,10,20);
		gl.setMaxTracks(100);
		Alice= new Contestant(0,"Alice",100);
		gl.parseFile("Maps/USAMap.txt");
		gl.getPlayerList().add(Alice);
		gl.getPlayerMap().put(Alice.getId(), Alice);
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
		Field twisterfield = GameLogic.class.getDeclaredField("twister");
		twisterfield.setAccessible(true);
		twister = (MersenneTwister) twisterfield.get(gl);
	}

	
	
	
	@Test
	public void buildAllOnlyLocs() throws IOException{
		for(int i= 0; i<10; i++) {
			Alice.addResource(TrackKind.ALL);
		}

		CommandP btc= new BuildTrackCommand(gl.getBoard(),Alice,0,0,server,TrackKind.ALL,twister,gl.getrClosedStack());

		assertTrue(btc.execute()); //TODO: is this allowed? building a Track with Kind ALL?
		//TODO: also you shouldn't be allowed to pay with more locs than the track length
		assertEquals(9,Alice.getHandCards().size());
		assertTrue(Alice.getListTracks().contains(gl.getBoard().getListTracks().get(0)));
		
		
		btc= new BuildTrackCommand(gl.getBoard(),Alice,9,4,server,TrackKind.ALL,twister,gl.getrClosedStack());
		assertTrue(!btc.execute());
		assertEquals(9,Alice.getHandCards().size());
		
		btc= new BuildTrackCommand(gl.getBoard(),Alice,0,4,server,TrackKind.ALL,twister,gl.getrClosedStack());
		assertTrue(btc.execute());
		assertEquals(3,Alice.getHandCards().size());
		assertTrue(Alice.getListTracks().contains(gl.getBoard().getListTracks().get(4)));
		
		gl.getBoard().getListTracks().get(0).setOwnerId(-1);
		gl.getBoard().getListTracks().get(4).setOwnerId(-1);
		Alice.getListTracks().clear();
		
		
	}
	
	@Test
	public void buildNotEnoughCards() {
		
	}
	
}
