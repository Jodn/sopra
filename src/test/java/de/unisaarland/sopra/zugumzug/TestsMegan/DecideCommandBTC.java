package de.unisaarland.sopra.zugumzug.TestsMegan;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.BuildTrack;
import de.unisaarland.sopra.Command;
import de.unisaarland.sopra.PayTunnel;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TakeSecretCard;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class DecideCommandBTC {

	private static GameLogic gl;
	private static Contestant Daniel, Alice;
	private static Server server;
	
	private static Constructor<BuildTrack> btc;
	private static Constructor<TakeSecretCard> tsc;
	private static Constructor<PayTunnel> ptc;
	
	
	@BeforeClass
	public static void SetUp() throws Exception {
		gl= new GameLogic(4222, null, 0, 0);
		gl.setRequiredPlayers(2);
		gl.parseFile("Maps/TaskMap2.txt");
		Daniel= (new Contestant(0, "Daniel", 100));
		Alice = new Contestant(1,"Alice",100);
		gl.getPlayerMap().put(Alice.getId(), Alice);
		gl.getPlayerMap().put(Daniel.getId(),Daniel);
		gl.setMaxHandCards(10000);
		gl.setrClosedStack(new ArrayList<TrackKind>());
		List<TrackKind> openCards= new ArrayList<TrackKind>();
		for(int i=0;i<5; i++)
			openCards.add(TrackKind.VIOLET);
		gl.setOpenResources(openCards);
		
		
		Class<?>[] o = new Class[4];
		o[0] =int.class;
		o[1]=short.class;
		o[2] = TrackKind.class;
		o[3]= int.class;
		btc= BuildTrack.class.getDeclaredConstructor(o);
		btc.setAccessible(true);
		
		Class<?>[] ca= new Class[1];
		ca[0]=int.class;
	
		tsc= TakeSecretCard.class.getDeclaredConstructor(ca);
		tsc.setAccessible(true);
		
		Class<?>[] ca2= new Class[2];
		ca2[0]=int.class;
		ca2[1]=int.class;
		
		ptc= PayTunnel.class.getDeclaredConstructor(ca2);
		ptc.setAccessible(true);
		
		
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}
	
	@AfterClass
	public static void afterClass() throws Exception{
	
		server.close();
	}
	
	@Before
	public void before() {
		Daniel.setHandCards(new ArrayList<TrackKind>());
		Alice.setHandCards(new ArrayList<TrackKind>());
		for(int i=0; i<10; i++) {
			Daniel.getHandCards().add(TrackKind.ALL);
			Daniel.getHandCards().add(TrackKind.BLUE);
			Alice.getHandCards().add(TrackKind.ALL);
			Alice.getHandCards().add(TrackKind.BLACK);
		}
		gl.getPlayerList().add(Daniel);
		gl.getPlayerList().add(Alice);
		Daniel.getListTracks().clear();
		Alice.getListTracks().clear();
		Alice.setNumFails(0);
		gl.getrClosedStack().clear();
		
	}
	
	@Test
	public void buildTrackLocsOnlyTest() throws Exception {
		Command command= btc.newInstance(0,(short)0,TrackKind.ALL,0);
		gl.decideCommand(command);
		assertEquals(18,Daniel.getHandCards().size());
		assertEquals(2,gl.getrClosedStack().size());
		assertTrue(gl.getrClosedStack().contains(TrackKind.ALL));
		assertTrue(!gl.getrClosedStack().contains(TrackKind.BLUE));
		assertTrue(Daniel.getListTracks().contains(gl.getBoard().getListTracks().get(0)));
		
		command= btc.newInstance(1,(short)2,TrackKind.ALL,0);
		gl.decideCommand(command);
		assertEquals(17,Alice.getHandCards().size());
		assertTrue(Alice.getListTracks().contains(gl.getBoard().getListTracks().get(2)));
		
		gl.getBoard().getListTracks().get(0).setOwnerId(-1);
		gl.getBoard().getListTracks().get(2).setOwnerId(-1);
		gl.setrClosedStack(new ArrayList<TrackKind>());
		
	}
	
	@Test
	public void buildTrackNotEnoughResources() throws Exception, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Daniel.getHandCards().add(TrackKind.GREEN);
		Alice.getHandCards().add(TrackKind.GREEN);
		Command c= btc.newInstance(0,(short) 0, TrackKind.GREEN,0);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getNumFails());
		c= btc.newInstance(0,(short) 0, TrackKind.YELLOW,1);
		gl.decideCommand(c);
		assertEquals(2,Daniel.getNumFails());
		c= btc.newInstance(0,(short) 1, TrackKind.YELLOW,1);
		gl.decideCommand(c);
		assertEquals(3,Daniel.getNumFails());
		c= btc.newInstance(0,(short) 0, TrackKind.YELLOW,2);
		gl.decideCommand(c);
		assertEquals(0,Daniel.getNumFails());
		
		
		c= btc.newInstance(1,(short) 1, TrackKind.GREEN,0);
		gl.decideCommand(c);
		assertEquals(1,Alice.getNumFails());
		
		c= btc.newInstance(1,(short) 1, TrackKind.GREEN,0);
		gl.decideCommand(c);
		assertEquals(2,Alice.getNumFails());
		
		c= btc.newInstance(1,(short) 1, TrackKind.ALL,-1);
		gl.decideCommand(c);
		assertEquals(3,Alice.getNumFails());
		
		c= btc.newInstance(1,(short) 1, TrackKind.ALL,2);
		gl.decideCommand(c);
		assertEquals(0,Alice.getNumFails());
		
		gl.getBoard().getListTracks().get(1).setOwnerId(-1);
		gl.getBoard().getListTracks().get(2).setOwnerId(-1);
		gl.getBoard().getListTracks().get(0).setOwnerId(-1);
	}
	
	
	@Test
	public void wrongBuildTrack() throws Exception {
		
		
		Command command= btc.newInstance(0,(short)0,TrackKind.WHITE,0);
		gl.decideCommand(command);
		assertEquals(20,Daniel.getHandCards().size());
		assertTrue(!Daniel.getListTracks().contains(gl.getBoard().getListTracks().get(0)));
		
		command= btc.newInstance(0,(short)0,TrackKind.BLUE,4);
		gl.decideCommand(command);
		assertEquals(20,Daniel.getHandCards().size());
		assertTrue(!Daniel.getListTracks().contains(gl.getBoard().getListTracks().get(0)));
		assertEquals(0,gl.getrClosedStack().size());
		
		command= btc.newInstance(0,(short)0,TrackKind.BLUE,0);
		gl.decideCommand(command);
		assertEquals(18,Daniel.getHandCards().size());
		assertTrue(Daniel.getListTracks().contains(gl.getBoard().getListTracks().get(0)));
		assertEquals(2,gl.getrClosedStack().size());
		assertTrue(gl.getrClosedStack().contains(TrackKind.BLUE));
		gl.getrClosedStack().clear();
		
		
		command= btc.newInstance(1,(short)2,TrackKind.BLACK,1);
		gl.decideCommand(command);
		assertEquals(17,Alice.getHandCards().size());
		assertTrue(Alice.getListTracks().contains(gl.getBoard().getListTracks().get(2)));
		assertEquals(3,gl.getrClosedStack().size());
		assertTrue(gl.getrClosedStack().contains(TrackKind.ALL));
		gl.getrClosedStack().remove(TrackKind.BLACK);
		assertTrue(gl.getrClosedStack().contains(TrackKind.BLACK));
		
		gl.getBoard().getListTracks().get(1).setOwnerId(-1);
		gl.getBoard().getListTracks().get(2).setOwnerId(-1);
		gl.getBoard().getListTracks().get(0).setOwnerId(-1);
		
	}
	
	@Test
	public void buildAllTest() throws Exception, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Daniel.getHandCards().clear();
		for(int i=0;i<2;i++) {
			Daniel.getHandCards().add(TrackKind.ALL);
		}
		Command c = btc.newInstance(0, (short) 5, TrackKind.ALL,2);
		gl.decideCommand(c);
		assertEquals(0,Daniel.getListTracks().size());
		assertEquals(2,Daniel.getHandCards().size());
		assertTrue(Daniel.getTunnelID()!=5);
		
		
	}
	
	
	@Test
	public void buildTunnelTest() throws Exception {
		gl.getrClosedStack().add(TrackKind.BLACK);
		Command c= btc.newInstance(0,(short) 1, TrackKind.WHITE,0);
		gl.decideCommand(c);
		assertEquals(0,Daniel.getListTracks().size());
		assertEquals(1,Daniel.getNumFails());
		
		c= btc.newInstance(0, (short) 1, TrackKind.BLACK,2);
		gl.decideCommand(c);
		assertEquals(0,Daniel.getListTracks().size());
		assertEquals(0,Daniel.getNumFails());
		
		assertEquals(20,Alice.getHandCards().size());
		c= btc.newInstance(1,(short)2,TrackKind.ALL,0);
		gl.decideCommand(c);
		assertEquals(20,Alice.getHandCards().size());
		assertEquals(1,Alice.getNumFails());
		assertTrue(!Alice.getListTracks().contains(gl.getBoard().getListTracks().get(2)));
		gl.getrClosedStack().add(TrackKind.VIOLET);
		c= tsc.newInstance(1);
		gl.decideCommand(c);
		assertEquals(2,Alice.getNumFails());
		assertEquals(20,Alice.getHandCards().size());
		
		c= btc.newInstance(0,(short) 2, TrackKind.ALL,0);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getNumFails());
		assertEquals(0,Daniel.getListTracks().size());
		
		c= ptc.newInstance(0,1);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getListTracks().size());
		assertEquals(0,Daniel.getNumFails());
		assertTrue(17==Daniel.getHandCards().size());
		
		c= btc.newInstance(1,(short)2,TrackKind.BLACK,1);
		gl.decideCommand(c);
		assertEquals(17,Alice.getHandCards().size());
		
		
		
		gl.getBoard().getListTracks().get(1).setOwnerId(-1);
		gl.getBoard().getListTracks().get(2).setOwnerId(-1);
		
		
	}


}
