package de.unisaarland.sopra.zugumzug.TestsMegan;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.*;

import de.unisaarland.sopra.Command;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TakeCard;
import de.unisaarland.sopra.TakeSecretCard;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.server.GameLogic;
import de.unisaarland.sopra.zugumzug.data.Contestant;;

public class TakeCardTests {
	
	private static GameLogic gl;
	private static Contestant Daniel, Alice;
	private static Server server;
	private static Constructor<TakeCard> tcc;
	private static Constructor<TakeSecretCard> tsc;
	
	
	@BeforeClass
	public static void SetUp() throws IOException, NoSuchMethodException, SecurityException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
		gl= new GameLogic(4222, null, 0, 0);
		Daniel= (new Contestant(0, "Daniel", 100));
		Alice = new Contestant(1,"Alice",100);
		gl.getPlayerMap().put(Alice.getId(), Alice);
		gl.getPlayerMap().put(Daniel.getId(),Daniel);
		gl.setMaxHandCards(10000);
		
		Class<?>[] o = new Class[2];
		o[0] =int.class;
		o[1] = TrackKind.class;
		tcc = TakeCard.class.getDeclaredConstructor(o);
		tcc.setAccessible(true);
		Class<?>[] o2 = new Class[1];
		o2[0] =int.class;
		tsc = TakeSecretCard.class.getDeclaredConstructor(o2);
		tsc.setAccessible(true);
		
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}
	
	@Before
	public void before() {
		Daniel.setHandCards(new ArrayList<TrackKind>());
		Daniel.setNumFails(0);
		gl.setMaxHandCards(10000);
		gl.getPlayerList().clear();
		gl.getPlayerList().add(Daniel);
		gl.getPlayerList().add(Alice);
		gl.setOpenResources(new ArrayList<TrackKind>());
		gl.setrClosedStack(new ArrayList<TrackKind>());
		
	}
	
	@AfterClass
	public static void afterClass() throws Exception{
	
		server.close();
	}
	
	@Test
	public void secondLocLegal() throws Exception  {
		
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.ALL);
		
		
		
		Command c= tcc.newInstance(0, TrackKind.BLUE);
		gl.decideCommand(c);
		
		c= tcc.newInstance(0, TrackKind.ALL);
		gl.decideCommand(c);
		
		assertTrue(Daniel.getHandCards().contains(TrackKind.BLUE));
		assertTrue(Daniel.getHandCards().contains(TrackKind.ALL));
		assertEquals(0,Daniel.getNumFails());
	}
	@Test
	public void firstLocIlleagal() throws Exception {
		
		gl.getOpenResources().add(TrackKind.ALL);
		Command c= tcc.newInstance(0, TrackKind.ALL);
		gl.decideCommand(c);
		
		assertEquals(0, Daniel.getHandCards().size());
		
	}
	
	@Test
	public void secondLocIlleagal() throws Exception, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.ALL);
		Command c= tcc.newInstance(0, TrackKind.ALL);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getHandCards().size());
		gl.decideCommand(c);
		assertEquals(1,Daniel.getNumFails());
		assertEquals(1,Daniel.getHandCards().size());
		
	}
	@Test
	public void drawOpenThenSecretCard() throws Exception {
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.GREEN);
		gl.getrClosedStack().add(TrackKind.GREEN);
		gl.getrClosedStack().add(TrackKind.GREEN);
		
		Command c= tcc.newInstance(0, TrackKind.GREEN);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getNumFails());
		assertEquals(0,Daniel.getHandCards().size());
		
		gl.decideCommand(c);
		assertEquals(2,Daniel.getNumFails());
		assertEquals(0,Daniel.getHandCards().size());
		
		Daniel.setNumFails(0);
		c= tcc.newInstance(0, TrackKind.BLACK);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getHandCards().size());
		 
		c= tcc.newInstance(0, TrackKind.ALL);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getNumFails());
		assertEquals(1,Daniel.getHandCards().size());
		assertTrue(Daniel.getHandCards().contains(TrackKind.BLACK));
		
		c= tcc.newInstance(0, TrackKind.ALL);
		gl.decideCommand(c);
		assertEquals(2,Daniel.getNumFails());
		assertEquals(1,Daniel.getHandCards().size());
		assertTrue(Daniel.getHandCards().contains(TrackKind.BLACK));
		
		c= tsc.newInstance(0);
		gl.decideCommand(c);
		assertEquals(0,Daniel.getNumFails());
		assertEquals(2,Daniel.getHandCards().size());
		assertTrue(Daniel.getHandCards().contains(TrackKind.BLACK));
		assertTrue(Daniel.getHandCards().contains(TrackKind.GREEN));
	}
	@Test
	public void drawSecretLocSecCard() throws Exception {
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.GREEN);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getrClosedStack().add(TrackKind.ALL);
		
		
		Command c= tcc.newInstance(0, TrackKind.GREEN);
		gl.decideCommand(c);
		assertEquals(0,Daniel.getNumFails());
		assertEquals(1,Daniel.getHandCards().size());
		assertTrue(Daniel.getHandCards().contains(TrackKind.GREEN));
		
		c= tcc.newInstance(0, TrackKind.ALL);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getNumFails());
		assertEquals(1,Daniel.getHandCards().size());
		
		c= tsc.newInstance(0);
		gl.decideCommand(c);
		assertEquals(0,Daniel.getNumFails());
		assertEquals(2,Daniel.getHandCards().size());
		assertTrue(Daniel.getHandCards().contains(TrackKind.ALL));
		
	}
	@Test
	public void drawTwoSecretLocs() throws Exception, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.GREEN);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getrClosedStack().add(TrackKind.ALL);
		
		Command c= tsc.newInstance(0);
		gl.decideCommand(c);
		assertEquals(0,Daniel.getNumFails());
		assertEquals(1,Daniel.getHandCards().size());
		assertTrue(Daniel.getHandCards().contains(TrackKind.ALL));
		
		gl.decideCommand(c);
		assertEquals(0,Daniel.getNumFails());
		assertEquals(2,Daniel.getHandCards().size());
		assertTrue(Daniel.getHandCards().contains(TrackKind.ALL));
		
	}
	
	@Test
	public void drawMoreThanMaxHandCards() throws Exception {
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.GREEN);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.setMaxHandCards(3);
		Daniel.getHandCards().add(TrackKind.ALL);
		Daniel.getHandCards().add(TrackKind.ALL);
		Daniel.getHandCards().add(TrackKind.ALL);
		Command c= tsc.newInstance(0);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getNumFails());
		assertEquals(3,Daniel.getHandCards().size());
		Daniel.setNumFails(0);
		
		
	}
	
	@Test
	public void drawMoreThanMaxHandCardsFirstMove() throws Exception {
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getOpenResources().add(TrackKind.GREEN);
		gl.getOpenResources().add(TrackKind.BLACK);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.setMaxHandCards(3);
		Daniel.getHandCards().add(TrackKind.ALL);
		Daniel.getHandCards().add(TrackKind.ALL);
		Command c= tsc.newInstance(0);
		gl.decideCommand(c);
		assertEquals(1,Daniel.getNumFails());
		assertEquals(2,Daniel.getHandCards().size());
		c= tcc.newInstance(0,TrackKind.BLACK);
		gl.decideCommand(c);
		assertEquals(2,Daniel.getNumFails());
		assertEquals(2,Daniel.getHandCards().size());
		
		c= tcc.newInstance(0,TrackKind.ALL);
		gl.decideCommand(c);
		assertEquals(3,Daniel.getNumFails());
		assertEquals(2,Daniel.getHandCards().size());
		
		
	}

}
