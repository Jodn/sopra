package de.unisaarland.sopra.zugumzug.TestsMegan;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class ParseFileTest {

	private static GameLogic gl;
	
	@BeforeClass
	public static void setUp() throws IOException {
		gl= new GameLogic(4000,null,1,2);
	}
	
	@Test
	public void CommentsTest() throws Exception {
		assertTrue(gl.parseFile("Maps/TaskMap2.txt"));
		assertTrue(gl.getBoard().getListTracks().size()>0);
	}

}
