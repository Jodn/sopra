package de.unisaarland.sopra.zugumzug.NancyTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.commands.BuildTrackCommand;
import de.unisaarland.sopra.zugumzug.commands.CommandP;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class BuildTrackC_T {
	private static GameLogic gl;
	private Board board;
	static private TrackKind kind = TrackKind.RED;
	private List<Contestant> Contestantlist = new ArrayList<Contestant>();
	private static Server server;
	private List<City> citieslist = new ArrayList<City>();
	private List<Track> tracklist = new ArrayList<Track>();
	private City city1;
	private City city2;
	private MersenneTwister twister = new MersenneTwister(666);
	private List<TrackKind> rClosedStack = new ArrayList<TrackKind>();

	@BeforeClass
	public static void setUpBefore() throws IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		gl = new GameLogic(4000, null, 0, 0);
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}

	@Before
	public void setUp() throws Exception {
		this.board = new Board();
		for (int i = 0; i <= 4; i++) {
			Contestant player = new Contestant(i, "Player " + i, 50);
			Contestantlist.add(player);
		
		}
		gl.setPlayerList(Contestantlist);
		city1 = new City("Saarbruecken", 0, 1, 1);
		city2 = new City("Kaiserslautern", 1, 2, 2);
		citieslist.add(city1);
		citieslist.add(city2);
		board.setListCities(citieslist);
		board.setListTracks(tracklist);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Test
	public void testAddCostsTunnelTest() throws IOException {
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(1).getHandCards().add(kind);
			Contestantlist.get(1).getHandCards().add(TrackKind.ALL);
		}
		Contestantlist.get(1).setAddCosts(3);
		Track track1 = new Track(1, city1, city2, 4, 10, true, kind);
		tracklist.add(track1);
		rClosedStack.add(TrackKind.ALL);
		rClosedStack.add(TrackKind.ALL);
		rClosedStack.add(kind);
		
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(1), 2, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(0, Contestantlist.get(1).getListTracks().size());
		assertTrue("Contestant addCosts should have been modified", Contestantlist.get(1).getAddCosts() >= 0);
		assertEquals(Contestantlist.get(1).getPoints(), 0);
		assertFalse("The contestant can't build the tunnel", Contestantlist.get(1).getTunnelID() == track1.getId());
	}
	
	@Test
	public void buuildTunnelTest() throws IOException {
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 3, 10, true, kind);
		tracklist.add(track1);
		rClosedStack.add(TrackKind.ALL);
		
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertTrue("Contestant addCosts should have been modified", Contestantlist.get(0).getAddCosts() >= 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
		assertTrue("The contestant can build the tunnel", Contestantlist.get(0).getTunnelID()== track1.getId());
	}
	
	@Test
	public void buildTunnelTest2() throws IOException {
		for (int i = 0; i <= 2; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 3, 10, true, kind);
		tracklist.add(track1);
		rClosedStack.add(kind);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 0, server, TrackKind.ALL, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertTrue("Contestant addCosts should have been modified", Contestantlist.get(0).getAddCosts() >= 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
		assertTrue("The contestant can build the tunnel", Contestantlist.get(0).getTunnelID() == track1.getId());
	}
	
	@Test
	public void buildwrongcolorTunnelTest2() throws IOException {
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(2, city1, city2, 3, 10, true, kind);
		tracklist.add(track1);
		rClosedStack.add(kind);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, kind, twister, rClosedStack);
		assertFalse("BuildTrackCommand did not work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertFalse("Contestant addCosts should have been modified", Contestantlist.get(0).getAddCosts() >= 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
		assertFalse("The contestant can't build the tunnel", Contestantlist.get(0).getTunnelID() == track1.getId());
	}
	
	@Test
	public void buildNumLocsTunnelTest() throws IOException {
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 3, 10, true, kind);
		tracklist.add(track1);
		rClosedStack.add(kind);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 4, 0, server, kind, twister, rClosedStack);
		assertFalse("BuildTrackCommand did not work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertFalse("Contestant addCosts should have been modified", Contestantlist.get(0).getAddCosts() >= 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
		assertTrue("The contestant can build the tunnel", Contestantlist.get(0).getTunnelID() == track1.getId());
	}
	
	@Test
	public void buildWithLocsTunnelTest2() throws IOException {
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
			Contestantlist.get(0).getHandCards().add(kind);
		}
		Track track1 = new Track(0, city1, city2, 3, 10, true, kind);
		tracklist.add(track1);
		rClosedStack.add(kind);
		rClosedStack.add(TrackKind.ALL);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 1, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertTrue("Contestant addCosts should have been modified", Contestantlist.get(0).getAddCosts() >= 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
		assertTrue("The contestant can't build the tunnel", Contestantlist.get(0).getTunnelID() == track1.getId());
	}


	@Test
	public void testWrongColorTest() throws IOException{
		
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, TrackKind.BLACK);
		tracklist.add(track1);
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 0, server, kind, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),0);
	}
	
	@Test
	public void testWrongNumLocsTest() throws IOException{
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 1, 0, server, kind, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),0);
	}
	
	@Test
	public void testNotParallelTrackTest() throws IOException{
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		City city3 = new City("Miami", 3, 4, 8);
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		Track track2 = new Track(1, city1, city3, 4, 10,  false, kind);
		tracklist.add(track1);
		tracklist.add(track2);
		
		Contestantlist.get(0).getListTracks().add(track1);
		Contestantlist.get(0).setPoints(10);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 1, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(2, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),20);
		assertEquals(rClosedStack.size(), 4);
	}
	
	@Test
	public void parallelTrackTest() throws IOException{
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, TrackKind.BLUE);
		Track track2 = new Track(2, city1, city2, 4, 10,  false, kind);
		city1.getMap().put(0, city2);
		city1.getMap().put(2, city2);
		tracklist.add(track1);
		tracklist.add(track2);
		
		track1.setOwnerId(Contestantlist.get(0).getId());
		Contestantlist.get(0).getListTracks().add(track1);
		Contestantlist.get(0).setPoints(10);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 2, server, kind, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(1, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),10);
	}
	
	@Test
	public void parallelTrackTunnelTest() throws IOException{
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, TrackKind.BLUE);
		Track track2 = new Track(2, city1, city2, 4, 10,  true, kind);
		city1.getMap().put(0, city2);
		city1.getMap().put(2, city2);
		tracklist.add(track1);
		tracklist.add(track2);
		
		track1.setOwnerId(Contestantlist.get(0).getId());
		Contestantlist.get(0).getListTracks().add(track1);
		Contestantlist.get(0).setPoints(10);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 2, server, kind, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(1, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),10);
	}
	@Test
	public void buildAlltrackTest() throws IOException{
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, TrackKind.ALL);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(1, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),10);
		assertEquals(rClosedStack.size(), 4);
	}
	
	@Test
	public void buildWithLocsTest() throws IOException{
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 4, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(1, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),10);
		assertEquals(rClosedStack.size(), 4 );
	}
	
	@Test
	public void buildLocsTest() throws IOException{
		
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 4, 0, server,TrackKind.ALL, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(1, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),10);
		assertEquals(rClosedStack.size(), 4 );
	}	
	
	@Test
	public void buildWithLocsTest2() throws IOException{
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, TrackKind.ALL, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(1, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),10);
		assertEquals(rClosedStack.size(), 4 );
	}
	
	@Test
	public void tooMuchpayTest() throws IOException{
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 3, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 5, 0, server, TrackKind.ALL, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),0);
		assertEquals(rClosedStack.size(), 0);
		assertEquals(5, Contestantlist.get(0).getHandCards().size());
	}
	
	@Test
	public void tooMuchPayTest2() throws IOException{
		for (int i = 0; i <= 2; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
			Contestantlist.get(0).getHandCards().add(kind);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 1, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(1, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),10);
		assertEquals(rClosedStack.size(), 4 );
		assertEquals(2, Contestantlist.get(0).getHandCards().size());
	}
	
	@Test
	public void buildLocs() throws IOException{
		for (int i = 0; i <= 2; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
			Contestantlist.get(0).getHandCards().add(kind);
		}
		Track track1 = new Track(0, city1, city2, 3, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 1, 0, server, TrackKind.ALL, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(1, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),10);
		assertEquals(rClosedStack.size(), 3);
		assertEquals(3, Contestantlist.get(0).getHandCards().size());
	}
	
	@Test
	public void WrongColorTest2() throws IOException{
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, kind, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),0);
		assertEquals(rClosedStack.size(), 0 );
	}
	
	@Test
	public void WrongNumLocs2() throws IOException{
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), -2, 0, server, TrackKind.ALL, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),0);
		assertEquals(rClosedStack.size(), 0 );
	}
	
	@Test
	public void WrongColor2() throws IOException{
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 1, 0, server, null, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),0);
		assertEquals(rClosedStack.size(), 0 );
	}
	
	@Test
	public void WrongTrack() throws IOException{
		for (int i = 0; i <= 4; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 1, -1, server, TrackKind.ALL, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),0);
		assertEquals(rClosedStack.size(), 0 );
	}
	
	@Test
	public void WrongNumLocs() throws IOException{
		for (int i = 0; i <= 7; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10,  false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 7, 0, server, TrackKind.ALL, twister, rClosedStack);
		assertFalse("BuildTrackCommand did work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertEquals(Contestantlist.get(0).getPoints(),0);
		assertEquals(rClosedStack.size(), 0 );
	}
	
}
