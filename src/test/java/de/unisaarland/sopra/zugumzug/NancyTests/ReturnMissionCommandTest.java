package de.unisaarland.sopra.zugumzug.NancyTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.commands.CommandP;
import de.unisaarland.sopra.zugumzug.commands.ReturnMissionCommand;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;

public class ReturnMissionCommandTest {

	List<MissionCard> mClosedStack = new ArrayList<MissionCard>();
	static MissionCard mission_1, mission_2, mission_3, mission_4, mission_5, mission_6, mission_7;
	static Server server;

	@BeforeClass
	public static void setUpBefore() throws IOException {
		server = new Server(5022);

		// Cities
		City LosAngeles = new City("Los Angeles", 0, 1, 2);
		City Miami = new City("Miami", 1, 3, 4);
		City Houston = new City("Houston", 2, 8, 11);
		City Dallas = new City("Dallas", 3, 1, 9);
		City Chicago = new City("Chicago", 4, 4, 14);
		City Phoenix = new City("Phoenix", 5, 8, 5);
		City SanDiego = new City("San Diego", 6, 11, 3);

		// Missions
		mission_1 = new MissionCard(LosAngeles, Houston, 20, 0);
		mission_2 = new MissionCard(Miami, Dallas, 9, 1);
		mission_3 = new MissionCard(Phoenix, SanDiego, 6, 2);
		mission_4 = new MissionCard(Dallas, Houston, 5, 3);
		mission_5 = new MissionCard(Chicago, SanDiego, 25, 4);
		mission_6 = new MissionCard(LosAngeles, Phoenix, 10, 5);
		mission_7 = new MissionCard(Miami, Phoenix, 5, 6);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Before
	public void setUp() {

		mClosedStack.clear();
		// mClosedStack
		mClosedStack.add(mission_7);
		mClosedStack.add(mission_6);
		mClosedStack.add(mission_5);
		mClosedStack.add(mission_4);
		mClosedStack.add(mission_3);
		mClosedStack.add(mission_2);
		mClosedStack.add(mission_1);
	}

	@Test
	public void allowedRetunMissionTest() throws IOException {
		Contestant Alice = new Contestant(0, "Alice", 20);
		Alice.getTempMissions().add(mClosedStack.get(0));
		Alice.getTempMissions().add(mClosedStack.get(1));
		Alice.getTempMissions().add(mClosedStack.get(2));
		mClosedStack.remove(0);
		mClosedStack.remove(1);
		mClosedStack.remove(2);
		int countstack = mClosedStack.size();

		Mission mis = new Mission((short) mission_7.getCity1().getId(), (short) mission_7.getCity2().getId(),
				mission_7.getPoints());
		Mission[] returnedMissions = { mis };
		CommandP returnMission = new ReturnMissionCommand(Alice, returnedMissions, mClosedStack, server);
		boolean ret = returnMission.execute();
		assertTrue("returnMission didn't work", ret);
		assertFalse("The contestant is not allowed to return all the taken mission Cards",
				Alice.getListMissions().isEmpty());
		assertTrue("There must be nothing in the TempMission list", Alice.getTempMissions().isEmpty());
		assertEquals("The contestant took missions Cards", mClosedStack.size(), countstack + 1);
	}

	// A player is not allowed to return Missions
	@Test
	public void notAllowedReturnMissionTest() throws IOException {
		Contestant Bob = new Contestant(1, "Bob", 20);
		ReturnMissionCommand returnMission = new ReturnMissionCommand(Bob, null, mClosedStack, server);
		boolean ret = returnMission.execute();
		assertFalse("This contestant did not take missions Cards", ret);
		assertTrue("This contestant did not take missions Cards", Bob.getListMissions().isEmpty());
		assertTrue("This contestant did not take missions Cards", Bob.getTempMissions().isEmpty());
	}

	// Did a player return the right Cards
	@Test
	public void rightCardsReturnTest() throws IOException {
		Contestant Dale = new Contestant(2, "Dale", 25);
		Mission mis = new Mission((short) mClosedStack.get(0).getCity1().getId(),
				(short) mClosedStack.get(0).getCity2().getId(), mClosedStack.get(0).getPoints());
		Dale.getListMissions().add(mClosedStack.remove(4));
		Dale.getListMissions().add(mClosedStack.remove(5));

		Dale.getTempMissions().add(mClosedStack.remove(0));
		Dale.getTempMissions().add(mClosedStack.remove(1));
		Dale.getTempMissions().add(mClosedStack.remove(2));

		int countstack = mClosedStack.size();
		int countmissions = Dale.getListMissions().size();

		Mission[] returnedMissions = { mis };
		ReturnMissionCommand returnMission = new ReturnMissionCommand(Dale, returnedMissions, mClosedStack, server);
		boolean ret = returnMission.execute();
		assertTrue("The contestant did return mission Cards", ret);
		assertTrue("This contestant returned a wrong mission Card", Dale.getListMissions().contains(mission_3));
		assertTrue("This contestant returned a wrong mission Card", Dale.getListMissions().contains(mission_2));
		assertEquals("This contestant returned a wrong mission Card", countmissions + 2, Dale.getListMissions().size());
		assertEquals("This contestant returned a wrong mission Card", countstack + 1, mClosedStack.size());
	}

	@Test
	public void gotTwoMissionsReturnTwo() throws IOException {
		Contestant Dave = new Contestant(3, "Dave", 25);
		mClosedStack.clear();
		Dave.addMission(mission_1);
		Dave.addMission(mission_2);
		Dave.addMission(mission_3);
		Dave.getTempMissions().add(mission_4);
		Dave.getTempMissions().add(mission_5);
		Mission m4 = new Mission((short) mission_4.getCity1().getId(), (short) mission_4.getCity2().getId(),
				mission_4.getPoints());
		Mission m5 = new Mission((short) mission_4.getCity1().getId(), (short) mission_4.getCity2().getId(),
				mission_4.getPoints());
		Mission[] retMissions = { m5, m4 };
		CommandP rmc = new ReturnMissionCommand(Dave, retMissions, mClosedStack, server);
		assertFalse("Command should not have worked", rmc.execute());
		assertEquals(3, Dave.getListMissions().size());
	}
	
	@Test
	public void testReturnWrongMission () throws IOException{
		Contestant cont = new Contestant (0, "Name", 30);
		cont.addMission(mission_1);
		cont.addMission(mission_2);
		cont.getTempMissions().add(mission_3);
		cont.getTempMissions().add(mission_4);
		cont.getTempMissions().add(mission_5);
		Mission mis = new Mission ((short)mission_1.getCity1().getId(),
				(short)mission_1.getCity2().getId(), mission_1.getPoints());
		Mission[] re = {mis};
		CommandP rmc = new ReturnMissionCommand(cont, re ,mClosedStack,server);
		assertFalse ("Contestant gave back old mission", rmc.execute());
	}
}
