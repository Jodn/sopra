package de.unisaarland.sopra.zugumzug.NancyTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.commands.RegisterCommand;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class RegisterCommandTest {

	static Server server;
	static GameLogic gameL;

	@Before
	public void setUp() {
		gameL.setPlayerList(new ArrayList<Contestant>());

	}

	@BeforeClass
	public static void beforeClass() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException,
			SecurityException, IOException {
		gameL = new GameLogic(4022, "Maps/TaskMap", 15, 4);
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gameL);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Test
	public void testNumPlayer() throws IOException {
		RegisterCommand registerC = new RegisterCommand(0, "Alice", server, gameL.getPlayerList(), gameL.getPlayerMap(),
				gameL.getMaxTracks());
		boolean reg = registerC.execute();
		assertTrue("There is a new player", reg);
		assertFalse("The Observer was not registered", gameL.getPlayerList().isEmpty());
		assertFalse("The Observer was not registered", gameL.getPlayerMap().isEmpty());
		assertTrue("Wrong name", gameL.getPlayerList().get(0).getName().equals("Alice")
				&& gameL.getPlayerMap().get(0).getName().equals("Alice"));
		gameL.getPlayerList().clear();
		gameL.getPlayerMap().clear();
	}

	/*
	 * @Test public void TestReqNumPlayer() throws IOException{
	 * System.out.println(gameL.getPlayerList().size()); Contestant Alice = new
	 * Contestant(0, "Alice", 50); Contestant Dale = new Contestant(1, "Dale",
	 * 50); Contestant Bob = new Contestant(2, "Bob", 50); Contestant Dany = new
	 * Contestant(3, "Dany", 50);
	 * 
	 * gameL.addContestant(Alice); gameL.addContestant(Dale);
	 * gameL.addContestant(Bob); gameL.addContestant(Dany);
	 * 
	 * RegisterCommand registerC = new RegisterCommand(4, "Amanda", server,
	 * gameL.getPlayerList(), gameL.getPlayerMap(),gameL.getMaxTracks());
	 * boolean reg = registerC.execute(); assertFalse(
	 * "registerC.execute() was true but shouldn't", reg);
	 * assertEquals(gameL.getPlayerList().size(), gameL.getRequiredPlayers());
	 * assertEquals(gameL.getPlayerMap().size(), gameL.getRequiredPlayers()); }
	 */

	@Test
	public void testNameChangebeforeGameStart() throws IOException {
		Contestant Alice = new Contestant(0, "Alice", 50);
		Contestant Dale = new Contestant(1, "Dale", 50);
		Contestant Bob = new Contestant(2, "Bob", 50);

		gameL.addContestant(Alice);
		gameL.addContestant(Dale);
		gameL.addContestant(Bob);

		RegisterCommand registerC = new RegisterCommand(0, "Amanda", server, gameL.getPlayerList(),
				gameL.getPlayerMap(), gameL.getMaxTracks());
		boolean reg = registerC.execute();
		assertTrue("This Player just changed her name", reg);
		assertTrue("This Player has changed her name", gameL.getPlayerList().contains(Alice));
		assertTrue("This Player has changed her name", Alice.getName().equals("Amanda"));
		assertTrue("This Player has changed her name", gameL.getPlayerMap().containsKey(0));
		assertEquals(3, gameL.getPlayerList().size());
		assertEquals(3, gameL.getPlayerMap().size());
		gameL.getPlayerList().clear();
		gameL.getPlayerMap().clear();
	}
}
