package de.unisaarland.sopra.zugumzug.NancyTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.commands.CommandP;
import de.unisaarland.sopra.zugumzug.commands.LeaveCommand;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.GameEngine;

public class LeaveCommandTest {

	List<Contestant> listPlayers;
	Contestant Dany = new Contestant(4, "Dany", 2);
	GameEngine gameE = new GameEngine();
	HashMap<Integer, Contestant> playerMap;
	static Server server;

	@BeforeClass
	public static void setUpBefore() throws IOException {
		server = new Server(5101);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Before
	public void setUp() {
		listPlayers = new ArrayList<Contestant>();
		playerMap = new HashMap<Integer, Contestant>();

		Contestant Alice = new Contestant(0, "Alice", 2);
		Contestant Bob = new Contestant(1, "Bob", 2);
		Contestant Dale = new Contestant(2, "Dale", 2);
		Contestant Amanda = new Contestant(3, "Amanda", 2);

		listPlayers.add(Alice);
		listPlayers.add(Bob);
		listPlayers.add(Dale);
		listPlayers.add(Amanda);
		listPlayers.add(Dany);

		playerMap.put(0, Alice);
		playerMap.put(1, Bob);
		playerMap.put(2, Dale);
		playerMap.put(3, Amanda);
		playerMap.put(4, Dany);

		gameE.setPlayerList(listPlayers);
		gameE.setPlayerMap(playerMap);
	}

	@Test
	public void leaveTest() throws IOException {
		int size = playerMap.size();
		int sizelist = listPlayers.size();
		CommandP leaveC = new LeaveCommand(listPlayers, listPlayers.get(4).getId(), playerMap, server);
		boolean leave = leaveC.execute();
		assertTrue("Contestant left", leave);
		assertFalse("Contestant with id 4 left the game", gameE.getPlayerList().contains(Dany));
		assertFalse("Contestant with id 4 left the game", gameE.getPlayerMap().containsValue(Dany));
		assertEquals(gameE.getPlayerMap().size(), size - 1);
		assertEquals(gameE.getPlayerList().size(), sizelist -1);
	}

	@Test
	public void unknownPlayerleaveTest() throws IOException {
		Contestant contestant = new Contestant(5, "unknown", 50);
		int sizelist = listPlayers.size();
		int sizemap = playerMap.size();
		CommandP leaveC = new LeaveCommand(listPlayers, contestant.getId(), playerMap, server);
		boolean leave = leaveC.execute();
		assertFalse("Unknown player", leave);
		assertEquals(listPlayers.size(), sizelist);
		assertEquals(playerMap.size(), sizemap);
	}
}
