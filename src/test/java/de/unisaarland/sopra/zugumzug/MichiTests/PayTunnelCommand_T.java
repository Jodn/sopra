package de.unisaarland.sopra.zugumzug.MichiTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.commands.CommandP;
import de.unisaarland.sopra.zugumzug.commands.PayTunnelCommand;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.Track;

public class PayTunnelCommand_T {

	private Board board;
	static private TrackKind kind = TrackKind.RED;
	private List<Contestant> Contestantlist = new ArrayList<Contestant>();
	private static Server server;
	private List<City> citieslist = new ArrayList<City>();
	private List<Track> tracklist = new ArrayList<Track>();
	private City city1;
	private City city2;
	private List<TrackKind> rClosedStack = new ArrayList<TrackKind>();

	@BeforeClass
	public static void setUpBefore() throws IOException {
		server = new Server(5001);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Before
	public void setUp() throws Exception {
		this.board = new Board();
		for (int i = 0; i <= 4; i++) {
			Contestant player = new Contestant(i, "Player " + i, 50);
			Contestantlist.add(player);
		}
		city1 = new City("Saarbruecken", 0, 1, 1);
		city2 = new City("Kaiserslautern", 1, 2, 2);
		citieslist.add(city1);
		citieslist.add(city2);
		board.setListCities(citieslist);
		board.setListTracks(tracklist);

	}

	@Test
	public void testBuildGreyTunnel() throws IOException {
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.ALL);
		tracklist.add(track);
		Contestantlist.get(0).setAddCosts(3);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		}
		for (int i = 0; i < 7; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Contestantlist.get(0).setTunnelKind(TrackKind.BLACK);
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 1, server, TrackKind.BLACK, rClosedStack, 0);
		assertTrue("PayTunnelCommand failed", ptc.execute());
		assertTrue("Wrong colors in rClosedStack", rClosedStack.contains(TrackKind.BLACK));
		assertFalse("Wrong color removed from contestant",
				Contestantlist.get(0).getHandCards().contains(TrackKind.BLACK));
	}

	@Test
	public void testPayTunnel() throws IOException {
		rClosedStack.add(TrackKind.YELLOW);
		rClosedStack.add(TrackKind.GREEN);
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.BLACK);
		tracklist.add(track);
		Contestantlist.get(0).setAddCosts(2);
		Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		for (int i = 0; i < 7; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		}
		int reso = Contestantlist.get(0).getHandCards().size();
		int ClosedStack = rClosedStack.size();
		int points = Contestantlist.get(0).getPoints();
		int trains = Contestantlist.get(0).getTrainsLeft();
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 1, server, TrackKind.BLACK, rClosedStack, 0);
		assertTrue("PayTunnelCommand fails", ptc.execute());
		assertTrue("Track wasn't added to contestant", Contestantlist.get(0).getListTracks().contains(track));
		assertEquals("OwnerID of track wasn't changed", Contestantlist.get(0).getId(), track.getOwnerId());
		assertEquals("Points weren't added", Contestantlist.get(0).getPoints(), points + 10);
		assertEquals("Trains of contestant weren't removed", trains - 4, Contestantlist.get(0).getTrainsLeft());
		assertEquals("AddCost wasn't set on -1", -1, Contestantlist.get(0).getAddCosts());
		assertEquals("Resourcen weren't removed.", reso - 6, Contestantlist.get(0).getHandCards().size());
		assertFalse("Wrong resources were removed", Contestantlist.get(0).getHandCards().contains(TrackKind.ALL));
		assertEquals("Resourcen weren't add to rClosedStack", ClosedStack + 6, rClosedStack.size());
		assertTrue("rClosedStack isn't sorted",
				rClosedStack.get(0).equals(TrackKind.YELLOW) && rClosedStack.get(1).equals(TrackKind.BLACK)
						&& rClosedStack.get(6).equals(TrackKind.GREEN) && rClosedStack.get(7).equals(TrackKind.ALL));
	}

	@Test
	public void testTrackisNotATunnel() throws IOException {
		Track track = new Track(0, city1, city2, 4, 10, false, TrackKind.BLACK);
		tracklist.add(track);
		Contestantlist.get(0).setAddCosts(0);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		}
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 0, server, TrackKind.BLACK, rClosedStack, 0);
		assertFalse("Track is build although it isn't a tunnel", ptc.execute());
	}

	@Test
	public void testWrongNumOfLocs() throws IOException {
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.BLACK);
		tracklist.add(track);
		Contestantlist.get(0).setAddCosts(1);
		Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 1, server, TrackKind.BLACK, rClosedStack, 0);
		assertFalse("Tunnel build although contestant didn't pay with enough locs", ptc.execute());
	}

	@Test
	public void testNotEnoughLocs() throws IOException {
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.BLACK);
		tracklist.add(track);
		Contestantlist.get(0).setAddCosts(1);
		Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		}
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 4, server, TrackKind.BLACK, rClosedStack, 0);
		assertFalse("Contestant paid with more locs than he owns", ptc.execute());
	}

	@Test
	public void testNoAddCosts() throws IOException {
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.BLACK);
		tracklist.add(track);
		Contestantlist.get(0).setAddCosts(-1);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		}
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 0, server, TrackKind.BLACK, rClosedStack, 0);
		assertFalse("AddCosts are not set", ptc.execute());
	}

	@Test
	public void testEnoughResources() throws IOException {
		for (int i = 0; i <= 6; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
		}
		Contestantlist.get(0).setAddCosts(2);
		Track track1 = new Track(0, city1, city2, 4, 10, true, kind);
		tracklist.add(track1);
		CommandP btc = new PayTunnelCommand(board, Contestantlist.get(0), 0, server, kind, rClosedStack, 0);
		assertTrue("BuildTrackCommand did not work", btc.execute());

		assertEquals(Contestantlist.get(0).getHandCards().size(), 1);
		assertEquals(Contestantlist.get(0).getListTracks().size(), 1);
		assertEquals(board.getListTracks().get(0).getOwnerId(), 0);
		assertEquals(Contestantlist.get(0).getPoints(), 10);
	}

	@Test
	public void testEnoughResourceswithLocs() throws IOException {
		for (int i = 0; i <= 3; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Contestantlist.get(0).setAddCosts(2);
		Track track1 = new Track(0, city1, city2, 4, 10, true, kind);
		tracklist.add(track1);
		CommandP btc = new PayTunnelCommand(board, Contestantlist.get(0), 2, server, kind, rClosedStack, 0);
		assertTrue("PayTunnelCommand did not work", btc.execute());

		assertEquals(Contestantlist.get(0).getHandCards().size(), 2);
		assertEquals(Contestantlist.get(0).getListTracks().size(), 1);
		assertEquals(board.getListTracks().get(0).getOwnerId(), 0);
		assertEquals(Contestantlist.get(0).getPoints(), 10);
	}

	@Test
	public void testPaidTooMuch() throws IOException {
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.BLACK);
		tracklist.add(track);
		Contestantlist.get(0).setAddCosts(0);
		Contestantlist.get(0).setTmpLocs(4);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 2, server, TrackKind.BLACK, rClosedStack, 0);
		assertFalse("Contestant paid with too much los", ptc.execute());
	}

	@Test
	public void testAvoidAddCosts() throws IOException {
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.BLACK);
		tracklist.add(track);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		Contestantlist.get(0).setAddCosts(3);
		Contestantlist.get(0).setTmpLocs(3);
		Contestantlist.get(0).setTunnelID(track.getId());
		Contestantlist.get(0).setTunnelKind(TrackKind.BLACK);
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 0, server,
				Contestantlist.get(0).getTunnelKind(), rClosedStack,
				Contestantlist.get(0).getTunnelID());
		assertFalse("Didn't pay enough", ptc.execute());
		assertEquals("Tunnel was build", -1, track.getOwnerId());
	}

	//Pay with more locs than AddCosts
	@Test
	public void testWrongNumLocs() throws IOException {
		for (int i = 0; i < 7; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.BLACK);
		board.getListTracks().add(track);
		city1.getMap().put(0, city2);
		city2.getMap().put(0, city1);
		Contestantlist.get(0).setTunnelID(track.getId());
		Contestantlist.get(0).setAddCosts(2);
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 3, server, TrackKind.BLACK, rClosedStack, 0);
		assertFalse ("Track was build", ptc.execute());
		assertEquals ("Track was built", -1, track.getOwnerId());
		assertEquals ("Resources were removed", 14, Contestantlist.get(0).getHandCards().size());
	}
	
	//Mix all and numLocs
	@Test
	public void testMixLocsAndALL() throws IOException {
		for (int i = 0; i < 10; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.BLACK);
		board.getListTracks().add(track);
		city1.getMap().put(0, city2);
		city2.getMap().put(0, city1);
		Contestantlist.get(0).setTunnelID(track.getId());
		Contestantlist.get(0).setAddCosts(3);
		CommandP ptc = new PayTunnelCommand(board, Contestantlist.get(0), 3, server, TrackKind.ALL, rClosedStack, 0);
		assertTrue ("PayTunnelCommand failed", ptc.execute());
		assertEquals ("Wrong handcards", 3, Contestantlist.get(0).getHandCards().size());
		assertEquals ("Track wasn't build", 0, track.getOwnerId());
	}
}
