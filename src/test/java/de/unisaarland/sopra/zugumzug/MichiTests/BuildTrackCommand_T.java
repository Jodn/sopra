package de.unisaarland.sopra.zugumzug.MichiTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.commands.BuildTrackCommand;

import de.unisaarland.sopra.zugumzug.commands.CommandP;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.Track;

public class BuildTrackCommand_T {
	private Board board;
	static private TrackKind kind = TrackKind.RED;
	private List<Contestant> Contestantlist = new ArrayList<Contestant>();
	private static Server server;
	private List<City> citieslist = new ArrayList<City>();
	private List<Track> tracklist = new ArrayList<Track>();
	private City city1;
	private City city2;
	private MersenneTwister twister = new MersenneTwister(666);
	private List<TrackKind> rClosedStack = new ArrayList<TrackKind>();

	@BeforeClass
	public static void setUpBefore() throws IOException {
		server = new Server(5000);
	}

	@Before
	public void setUp() throws Exception {
		this.board = new Board();
		for (int i = 0; i <= 4; i++) {
			Contestant player = new Contestant(i, "Player " + i, 50);
			Contestantlist.add(player);

		}

		city1 = new City("Saarbruecken", 0, 1, 1);
		city2 = new City("Kaiserslautern", 1, 2, 2);
		citieslist.add(city1);
		citieslist.add(city2);
		board.setListCities(citieslist);
		board.setListTracks(tracklist);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Test
	public void testNotEnoughResources() throws IOException {
		Contestantlist.get(0).getHandCards().add(kind);
		Track track1 = new Track(0, city1, city2, 4, 10, false, kind);
		tracklist.add(track1);
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", !btc.execute());

		assertEquals(Contestantlist.get(0).getHandCards().size(), 1);
		assertEquals(Contestantlist.get(0).getListTracks().size(), 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
	}

	// should be fine
	@Test
	public void testTrackHasOwner() throws IOException {
		for (int i = 0; i <= 5; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
		}

		Track track1 = new Track(0, city1, city2, 4, 10, false, kind);
		track1.setOwnerId(2);
		tracklist.add(track1);
		Contestantlist.get(2).getListTracks().add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, kind, twister, rClosedStack);
		assertEquals(0, rClosedStack.size());
		assertTrue("BuildTrackCommand should not work due to owner conflict", !btc.execute());
		assertTrue("Player has not the right amount of Cards", Contestantlist.get(0).getHandCards().size() == 6);
		assertEquals(board.getListTracks().get(0).getOwnerId(), 2);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
	}

	@Test
	public void testTracknotexist() throws IOException {
		for (int i = 0; i <= 5; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
		}
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 2, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work due to a existing track which should not be buildable",
				!btc.execute());
		assertEquals(Contestantlist.get(0).getListTracks().size(), 0);
		assertEquals(Contestantlist.get(0).getHandCards().size(), 6);
		assertEquals(Contestantlist.get(0).getPoints(), 0);

	}

	// should work
	@Test
	public void testNotEnoughTrains() throws IOException {
		for (int i = 0; i <= 5; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
		}
		Contestantlist.get(0).setTrainsLeft(2);

		Track track1 = new Track(0, city1, city2, 7, 10, false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work due to should have not enough trains", !btc.execute());
		assertEquals(Contestantlist.get(0).getHandCards().size(), 6);
		assertEquals(Contestantlist.get(0).getListTracks().size(), 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);

	}

//	@Test
//	public void sendEvent() {
//		for (int i = 0; i <= 5; i++) {
//			Contestantlist.get(0).getHandCards().add(kind);
//		}
//		Track track1 = new Track(0, city1, city2, 4, 10, false, kind);
//		tracklist.add(track1);
//
//		assertTrue("Not implemented yet due to missing informations about the combib", false);
//
//	}

	@Test
	public void testInvalidBuildTrack() throws IOException {
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 1, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work. It should have not enough locs", !btc.execute());
		assertEquals(Contestantlist.get(0).getPoints(), 0);
	}

	@Test
	public void testBuildwithLocSuc() throws IOException {
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10, false, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand should have worked building with locs", btc.execute());
		assertTrue("Locs should have been removed from the contestants handcards",!Contestantlist.get(0).getHandCards().contains(TrackKind.ALL));
		assertEquals(Contestantlist.get(0).getHandCards().size(),0);
		assertEquals(Contestantlist.get(0).getPoints(),10);
		assertEquals(rClosedStack.size(), 4);

	}

	@Test
	public void testBuildTunnelTrack() throws IOException {
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Track track1 = new Track(0, city1, city2, 4, 10, true, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 0, server, kind, twister, rClosedStack);

		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertTrue("Contestant addCosts should have been modified", Contestantlist.get(0).getAddCosts() >= 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
	}

	@Test
	public void testAddCostsTunnelTest() throws IOException {
		for (int i = 0; i <= 1; i++) {
			Contestantlist.get(0).getHandCards().add(kind);
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		Contestantlist.get(0).setAddCosts(3);
		Track track1 = new Track(0, city1, city2, 4, 10, true, kind);
		tracklist.add(track1);

		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 0, server, kind, twister, rClosedStack);
		assertTrue("BuildTrackCommand did not work", btc.execute());
		assertEquals(0, Contestantlist.get(0).getListTracks().size());
		assertTrue("Contestant addCosts should have been modified", Contestantlist.get(0).getAddCosts() >= 0);
		assertEquals(Contestantlist.get(0).getPoints(), 0);
	}
	
	@Test
	public void testPaidTooMuch() throws IOException{
		Track track = new Track(0, city1, city2, 4, 10, false, TrackKind.BLACK);
		tracklist.add(track);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		}
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 6,0, server, TrackKind.BLACK, twister, rClosedStack);
		assertFalse("Contestant paid with too much locs", btc.execute());
	}
	
	@Test
	public void testNotEnoughLocs() throws IOException {
		Track track = new Track(0, city1, city2, 4, 10, false, TrackKind.BLACK);
		tracklist.add(track);
		Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
		for (int i = 0; i < 6; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		}
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 4, 0, server, TrackKind.BLACK, twister, rClosedStack);
		assertFalse("Contestant paid with more locs than he owns", btc.execute());
	}

	//Try to build parallel track
	@Test
	public void testBuildParallelTrack() throws IOException {
		for (int i = 0; i < 7; i++) {
			Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
		}
		Track track = new Track(0, city1, city2, 4, 10, true, TrackKind.ALL);
		Track track2 = new Track(1, city1, city2, 4, 10, false, TrackKind.BLACK);
		board.getListTracks().add(track);
		board.getListTracks().add(track2);
		track.setOwnerId(0);
		Contestantlist.get(0).getListTracks().add(track);
		city1.getMap().put(0, city2);
		city2.getMap().put(1, city1);
		city1.getMap().put(1, city2);
		city2.getMap().put(0, city1);
		CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 1, server, TrackKind.BLACK, twister,
				rClosedStack);
		assertFalse ("Track was build although contestant already owns parallel track", btc.execute());
		assertEquals ("Track was built", -1, track2.getOwnerId());
		assertEquals ("Resources were removed", 7, Contestantlist.get(0).getHandCards().size());
	}
	
	//Mix all and numLocs
		@Test
		public void testMixLocsAndALL() throws IOException {
			for (int i = 0; i < 10; i++) {
				Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
			}
			Track track = new Track(0, city1, city2, 4, 10, false, TrackKind.BLACK);
			board.getListTracks().add(track);
			city1.getMap().put(0, city2);
			city2.getMap().put(0, city1);
			CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 2, 0, server, TrackKind.ALL, twister,
					rClosedStack);
			assertTrue ("BuildTrackCommand failed", btc.execute());
			assertEquals ("Wrong handcards", 6, Contestantlist.get(0).getHandCards().size());
			assertEquals ("Track wasn't build", 0, track.getOwnerId());
			assertEquals ("Trains weren't removed", 46, Contestantlist.get(0).getTrainsLeft());
			assertEquals ("Wrong points", 10, Contestantlist.get(0).getPoints());
		}
		
		
		//TunnelNotBuild
		@Test
		public void testTunnelNotBuild () throws IOException{
			for (int i = 0; i < 6; i++) {
				Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
				rClosedStack.add(TrackKind.BLACK);
			}
			Track track = new Track(0, city1, city2, 6, 10, true, TrackKind.BLACK);
			board.getListTracks().add(track);
			city1.getMap().put(0, city2);
			city2.getMap().put(0, city1);
			CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, TrackKind.BLACK, twister,
					rClosedStack);
			//AddCosts müssten 3 sein
			assertTrue ("BuildTrackCommand failed", btc.execute());
			assertEquals ("AddCosts was set", -1, Contestantlist.get(0).getAddCosts());
		}
		
		//Build Tunnel with empty rStack
		@Test
		public void testTunnelEmptyStack() throws IOException{
			for (int i = 0; i < 6; i++) {
				Contestantlist.get(0).getHandCards().add(TrackKind.BLACK);
			}
			Track track = new Track(0, city1, city2, 6, 10, true, TrackKind.BLACK);
			board.getListTracks().add(track);
			city1.getMap().put(0, city2);
			city2.getMap().put(0, city1);
			CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 0, 0, server, TrackKind.BLACK, twister,
					rClosedStack);
			assertTrue ("BuildTrackCommand failed", btc.execute());
			assertEquals ("AddCosts wasn't set", 0, Contestantlist.get(0).getAddCosts());
		}
		
		
		@Test
		public void testPayTooMuch () throws IOException {
			for (int i = 0; i < 10; i++) {
				Contestantlist.get(0).getHandCards().add(TrackKind.ALL);
			}
			Track track = new Track(0, city1, city2, 4, 10, false, TrackKind.BLACK);
			board.getListTracks().add(track);
			city1.getMap().put(0, city2);
			city2.getMap().put(0, city1);
			CommandP btc = new BuildTrackCommand(board, Contestantlist.get(0), 6, 0, server, TrackKind.BLACK, twister,
					rClosedStack);
			assertFalse ("BuildTrackCommand didn't fail", btc.execute());
			assertEquals ("Track was build", -1, track.getOwnerId());
			assertEquals ("removed handcards", 10, Contestantlist.get(0).getHandCards().size());
		}
}
