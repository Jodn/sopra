package de.unisaarland.sopra.zugumzug.RobinTests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.*;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Field;

import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.commands.TakeCardCommand;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class TakeCardTests {
	static GameLogic gl;
	static Server server;
	List<TrackKind> rClosedStack;
	List<TrackKind> openResources;
	Contestant player1;
	MersenneTwister twister;
	TakeCardCommand takeCard;

	@BeforeClass
	public static void setUpClass() throws IOException, NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {
		gl = new GameLogic(4003, null, 0, 0);
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}

	@Before
	public void setUp() throws IOException {

		rClosedStack = new ArrayList<TrackKind>();
		openResources = new ArrayList<TrackKind>();
		player1 = new Contestant(0, "Player1", 50);
		twister = new MersenneTwister(51480000);

		gl.setrClosedStack(rClosedStack);
		gl.setOpenResources(openResources);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Test
	public void testTakeBlackCard() throws IOException {
		gl.getrClosedStack().add(TrackKind.GREEN);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);

		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.BLACK, 100);
		assertTrue("TakeCardCommand false", takeCard.execute());

		assertTrue("Did not fill visible Cards!", openResources.size() == 5);
		assertTrue("Did not take the card from Closed Stack", openResources.contains(TrackKind.GREEN));
		assertTrue("Card is not in player's Hand Cards", player1.getHandCards().contains(TrackKind.BLACK));
		// assertTrue("Did not decrease turnsLeft", gl.getTurnsLeft() == 1);
	}

	@Test
	public void testTakeIllegalCard() throws IOException {
		gl.getOpenResources().add(TrackKind.GREEN);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.ALL);

		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.BLACK, 100);
		assertTrue("TakeCardCommand true, but has to be false", !takeCard.execute());
		assertTrue("Drew Black card althought openResources does not contain it",
				!(player1.getHandCards().contains(TrackKind.BLACK)));
		// assertTrue("Did not increase player's fail counter",
		// player1.getNumFails() == 1);
	}

	@Test
	public void testTakeLoc() throws IOException {
		gl.getrClosedStack().add(TrackKind.GREEN);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);
		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.ALL, 100);
		assertTrue("TakeCardCommand false", takeCard.execute());

		assertTrue("Did not fill visible Cards!", openResources.size() == 5);
		assertTrue("Did not take the card from Closed Stack", openResources.contains(TrackKind.GREEN));
		assertTrue("Card is not in player's Hand Cards", player1.getHandCards().contains(TrackKind.ALL));
	}

	// DecideCommand checks if player is allowed to take loc (TakeCardCommand
	// can't check TurnsLeft)
	/**
	 * @Test public void testTakeLocIllegal() throws IOException {
	 *       gl.getrClosedStack().add(TrackKind.GREEN);
	 *       gl.getOpenResources().add(TrackKind.BLUE);
	 *       gl.getOpenResources().add(TrackKind.BLUE);
	 *       gl.getOpenResources().add(TrackKind.BLUE);
	 *       gl.getOpenResources().add(TrackKind.ALL);
	 *       gl.getOpenResources().add(TrackKind.BLACK); gl.setTurnsLeft(1);
	 *       takeCard = new TakeCardCommand(player1, server, twister,
	 *       gl.getrClosedStack(), gl.getOpenResources(), TrackKind.ALL);
	 *       assertTrue("TakeCardCommand true, but has to be false",
	 *       !takeCard.execute()); assertTrue("Took loc although turnsLeft = 1",
	 *       player1.getHandCards().contains(TrackKind.ALL)); }
	 **/

	@Test
	public void testNotReplaceCardsWhen3Locs() throws IOException {
		gl.getrClosedStack().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.BLUE);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.BLACK);

		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.BLACK, 100);
		assertTrue("TakeCardCommand false", takeCard.execute());

		Collection<TrackKind> t = new ArrayList<TrackKind>();
		t.add(TrackKind.ALL);
		t.add(TrackKind.BLUE);
		t.add(TrackKind.BLUE);
		t.add(TrackKind.ALL);
		t.add(TrackKind.ALL);

		assertTrue("Did replace cards although closed stack is empty", gl.getOpenResources().containsAll(t));
	}

	/*
	 * OpenStack kann nach erneutem ziehen genauso aussehen wie vorher, da die
	 * fünf Karten vor dem Ziehen erst wieder in den rClosedStack gelget werden.
	 * 
	 * 
	 * @Test public void testReplaceCardsWhen3Locs() throws IOException {
	 * gl.getrClosedStack().add(TrackKind.ALL);
	 * gl.getrClosedStack().add(TrackKind.ALL);
	 * gl.getrClosedStack().add(TrackKind.ALL);
	 * gl.getrClosedStack().add(TrackKind.ALL);
	 * gl.getrClosedStack().add(TrackKind.ALL);
	 * gl.getrClosedStack().add(TrackKind.ALL);
	 * 
	 * gl.getOpenResources().add(TrackKind.ALL);
	 * gl.getOpenResources().add(TrackKind.BLUE);
	 * gl.getOpenResources().add(TrackKind.BLUE);
	 * gl.getOpenResources().add(TrackKind.ALL);
	 * gl.getOpenResources().add(TrackKind.BLACK);
	 * 
	 * takeCard = new TakeCardCommand(player1, server, twister,
	 * gl.getrClosedStack(), gl.getOpenResources(), TrackKind.BLACK, 100);
	 * assertTrue("TakeCardCommand false", takeCard.execute());
	 * 
	 * Collection<TrackKind> t = new ArrayList<TrackKind>();
	 * t.add(TrackKind.ALL); t.add(TrackKind.BLUE); t.add(TrackKind.BLUE);
	 * t.add(TrackKind.ALL); t.add(TrackKind.ALL);
	 * 
	 * assertTrue("Did not replace cards although there are 3 locs",
	 * !gl.getOpenResources().containsAll(t)); }
	 */

	@Test
	public void testNoCards() throws IOException {
		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.BLACK, 100);
		assertTrue("TakeCardCommand true, but has to be false", !takeCard.execute());
		assertTrue("Took card although stack is empty", player1.getHandCards().size() == 0);
	}

	@Test
	public void testCardsPlayerRelation() throws IOException {
		gl.getOpenResources().add(TrackKind.BLACK);

		Contestant player2 = new Contestant(1, "Player2", 50);
		Contestant player3 = new Contestant(2, "Player3", 50);
		gl.addContestant(player2);
		gl.addContestant(player1);
		gl.addContestant(player3);
		player1.getHandCards().add(TrackKind.BLACK);
		player1.getHandCards().add(TrackKind.BLACK);
		gl.setMaxHandCards(2);
		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.BLACK, gl.getMaxHandCards());
		assertTrue("Took Card alt{hough Cards / Player < 1", !takeCard.execute());
	}

	@Test
	public void testOpenResourcesEmpty() throws IOException {
		gl.getOpenResources().clear();
		Contestant player1 = new Contestant(1, "Player2", 50);
		Contestant player2 = new Contestant(2, "Player3", 50);
		gl.addContestant(player1);
		gl.addContestant(player2);
		gl.setMaxHandCards(20);
		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.BLACK, gl.getMaxHandCards());
		assertFalse("Took card although open Stack is empty", takeCard.execute());
	}

	// If three locs are open, draw new five cards.
	// If the new cards contains 3 locs, don't draw again.
	@Test
	public void testOpenLocs() throws IOException {
		for (int i = 0; i < 6; i++) {
			gl.getrClosedStack().add(TrackKind.ALL);
		}
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.RED);
		gl.getOpenResources().add(TrackKind.RED);
		gl.getOpenResources().add(TrackKind.YELLOW);
		Contestant player1 = new Contestant(1, "Player2", 50);
		gl.setMaxHandCards(20);
		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.RED, gl.getMaxHandCards());
		assertTrue("TakeCardCommand failed", takeCard.execute());
		// assertTrue("Draw twice",
		// !(gl.getOpenResources().contains(TrackKind.RED)));
		assertEquals("Wrong num of cards in open Stack", 5, gl.getOpenResources().size());
		// assertTrue("Open cards weren't added to rClosedStack",
		// gl.getrClosedStack().contains(TrackKind.YELLOW));
		assertTrue("Card wasn't added to Contestant", player1.getHandCards().contains(TrackKind.RED));
		assertTrue("Open Stack hasn't changed",
				!(gl.getOpenResources().get(0).equals(TrackKind.ALL)
						&& gl.getOpenResources().get(1).equals(TrackKind.ALL)
						&& gl.getOpenResources().get(2).equals(TrackKind.RED)
						&& gl.getOpenResources().get(3).equals(TrackKind.RED)
						&& gl.getOpenResources().get(4).equals(TrackKind.YELLOW)));
		// assertTrue("RClosedStack isn't sorted",
		// gl.getrClosedStack().get(0).equals(TrackKind.YELLOW)
		// && gl.getrClosedStack().get(1).equals(TrackKind.RED)
		// && gl.getrClosedStack().get(2).equals(TrackKind.ALL));
	}

	@Test
	public void testRClosedStackEmpty() throws IOException {
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.ALL);
		gl.getOpenResources().add(TrackKind.RED);
		gl.getOpenResources().add(TrackKind.RED);
		gl.getOpenResources().add(TrackKind.YELLOW);
		Contestant player1 = new Contestant(1, "Player2", 50);
		gl.setMaxHandCards(20);
		takeCard = new TakeCardCommand(player1, server, twister, gl.getrClosedStack(), gl.getOpenResources(),
				TrackKind.RED, gl.getMaxHandCards());
		assertTrue("TakeCardCommand failed", takeCard.execute());
		assertEquals("Wrong size of open stack", 4, gl.getOpenResources().size());
	}
}