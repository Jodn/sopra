package de.unisaarland.sopra.zugumzug.RobinTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.commands.GetMissionCommand;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class GetMissionTests {
	static GameLogic gl;
	static Server server;
	List<TrackKind> mClosedStack;
	Contestant player1;
	MersenneTwister twister;
	GetMissionCommand mission;

	@BeforeClass
	public static void setUpClass() throws IOException, NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {
		gl = new GameLogic(4002, null, 0, 0);
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}

	@Before
	public void setUp() throws IOException {
		mClosedStack = new ArrayList<TrackKind>();
		player1 = new Contestant(0, "Player1", 50);
		twister = new MersenneTwister(51480000);
		gl.setrClosedStack(mClosedStack);
		City c1 = new City("Mordor", 0, 1, 1);
		City c2 = new City("Thal", 1, 1, 2);
		City c3 = new City("Moria", 2, 1, 3);
		City c4 = new City("Minas Tirith", 3, 1, 4);
		City c5 = new City("Gondolin", 4, 2, 1);
		City c6 = new City("Bree", 5, 2, 2);
		MissionCard m1 = new MissionCard(c1, c2, 2, 0);
		MissionCard m2 = new MissionCard(c3, c4, 3, 1);
		MissionCard m3 = new MissionCard(c5, c6, 4, 2);
		gl.getmClosedStack().add(m1);
		gl.getmClosedStack().add(m2);
		gl.getmClosedStack().add(m3);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Test
	public void testGet3Missions() throws IOException {
		mission = new GetMissionCommand(gl.getmClosedStack(), player1, twister, server);
		assertTrue("Execute: false", mission.execute());
		assertTrue("player did not recieve all 3 missions", player1.getTempMissions().size() == 3);
		assertTrue("Did not remove missions from mClosedStack", gl.getmClosedStack().size() == 0);
	}

	@Test
	public void testGet2Missions() throws IOException {
		gl.getmClosedStack().remove(0);
		mission = new GetMissionCommand(gl.getmClosedStack(), player1, twister, server);
		assertTrue("Execute: false", mission.execute());
		assertTrue("Did not took 2 mission cards", player1.getTempMissions().size() == 2);
		assertTrue("Did not remove missions fom Stack", gl.getmClosedStack().size() == 0);
	}

	@Test
	public void testGet0Missions() throws IOException {
		gl.getmClosedStack().clear();
		mission = new GetMissionCommand(gl.getmClosedStack(), player1, twister, server);
		assertTrue("Execute: true, but has to be false", !mission.execute());
		assertEquals("Drew something although stack is empty", player1.getTempMissions().size(), 0);
	}
}
