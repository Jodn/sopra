package de.unisaarland.sopra.zugumzug.RobinTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.commands.TakeSecretCardCommand;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class TakeSecretCardTests {

	static GameLogic gl;
	static Server server;
	List<TrackKind> rClosedStack;
	Contestant player1;
	MersenneTwister twister;
	TakeSecretCardCommand takeCard;
	//static Player player;

	@BeforeClass
	public static void setUpClass() throws IOException, NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {
		gl = new GameLogic(4004, null, 0, 0);
		Field serverField = GameLogic.class.getDeclaredField("server");
		serverField.setAccessible(true);
		server = (Server) serverField.get(gl);
	}

	@Before
	public void setUp() throws IOException {

		rClosedStack = new ArrayList<TrackKind>();
		player1 = new Contestant(0, "Player1", 50);
		twister = new MersenneTwister(51480000);
		gl.setrClosedStack(rClosedStack);
		gl.addContestant(player1);
	}

	@AfterClass
	public static void closeUp() throws Exception {

		server.close();
	}

	@Test
	public void testTakeSCard() throws IOException {
		gl.getrClosedStack().add(TrackKind.GREEN);
		gl.getrClosedStack().add(TrackKind.GREEN);
		gl.getrClosedStack().add(TrackKind.GREEN);

		takeCard = new TakeSecretCardCommand(player1, server, twister, gl.getrClosedStack(), 100,1);
		assertTrue("Execute: false", takeCard.execute());
		assertTrue("Did not take card", player1.getHandCards().size() == 1);
		assertEquals("Did not remove card from closed stack", gl.getrClosedStack().size(), 2);
	}

	@Test
	public void testTakeSCardloc() throws IOException {
		gl.getrClosedStack().add(TrackKind.ALL);
		takeCard = new TakeSecretCardCommand(player1, server, twister, gl.getrClosedStack(), 100,1);
		assertTrue("Execute: false", takeCard.execute());
		assertTrue("Did not take card", player1.getHandCards().contains(TrackKind.ALL));
		assertEquals("Did not remove card from closed stack", gl.getrClosedStack().size(), 0);
	}

	@Test
	public void testNoCardsInStack() throws IOException {
		takeCard = new TakeSecretCardCommand(player1, server, twister, gl.getrClosedStack(), 100,1);
		assertTrue("Execute: true, but has to be false", !takeCard.execute());
		assertEquals("Took card althought there is no card in stack", player1.getHandCards().size(), 0);
	}
	
	@Test
	public void testTooMuchHandcards () throws IOException {
		rClosedStack.add(TrackKind.ALL);
		player1.getHandCards().add(TrackKind.BLACK);
		player1.getHandCards().add(TrackKind.RED);
		takeCard = new TakeSecretCardCommand(player1, server, twister, gl.getrClosedStack(),2, 1);
		assertFalse ("Contestant has too much handcards", takeCard.execute());
	}

}
