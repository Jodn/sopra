package de.unisaarland.sopra.zugumzug.commands;

import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.Mission;

import java.util.List;

import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;

public class ReturnMissionCommand implements CommandP {

	private Contestant contestant;
	private de.unisaarland.sopra.Mission[] returnedMissions;
	private List<MissionCard> mClosedStack;
	private Server server;

	public ReturnMissionCommand(Contestant contestant, Mission[] returnedMissions, List<MissionCard> mClosedStack,
			Server server) {
		this.contestant = contestant;
		if (returnedMissions == null){
			this.returnedMissions = null;
		}else{
		this.returnedMissions = returnedMissions.clone();
		}
		this.mClosedStack = mClosedStack;
		this.server = server;
	}

	/**
	 * Compares a Mission and a kommlib-Mission
	 * 
	 * @param m1
	 *            Mission
	 * @param m2
	 *            (kommlib) Mission
	 * @return true if the Missions are equal, false otherwise
	 */

	private boolean compareMissions(MissionCard m1, Mission m2) {
		return (short) m1.getCity1().getId() == m2.getStart()
				&& (short) m1.getCity2().getId() == m2.getDestination() && m1.getPoints() == m2.getPoints();
	}

	/**
	 * Checks if a Mission-List contains a specific mission given as
	 * Commlib-Mission
	 * 
	 * @param m1
	 *            List<Mission>
	 * @param m2
	 *            (Commlib) Mission
	 * @return true if m1 contains m2, false otherwise
	 */

	private boolean containsMission(List<MissionCard> misslist, Mission miss) {
		for (int i = 0; i < misslist.size(); i++) {
			if (compareMissions(misslist.get(i), miss)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * The command in which a player can hand missions back after he send the
	 * GetMissionCommand.
	 * 
	 * @return true if the command was successfully executed, false otherwise.
	 */

	@Override
	public boolean execute() throws IOException {

		// No returnedMissions
		if (returnedMissions == null) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					server.toClient(contestant.getId())
							.failure("Error in ReturnMissionCommand: Please send an empty Array instead");
				}
			} catch (Exception e) {
			}
			return false;
		}

		// Have to keep at least one mission
		if (returnedMissions.length >= contestant.getTempMissions().size()) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					server.toClient(contestant.getId())
							.failure("Error in ReturnMissionCommand: You have to keep at least one Mission");
				}
			} catch (Exception e) {
			}
			return false;
		}

		// check if returned missions are contained in tempMissionlist
		for (int i = 0; i < returnedMissions.length; i++) {
			if (!(containsMission(contestant.getTempMissions(), returnedMissions[i]))) {
				try {
					if (contestant.getNumFails() == 3) {
						return false;
					} else {
						server.toClient(contestant.getId()).failure(
								"Error in ReturnMissionCommand: You can only return missions you just received");
					}
				} catch (Exception e) {
				}
				return false;
			}
		}

		// Remove returned missions from tempMissionlist and add to mClosedStack
		for (int y = 0; y < returnedMissions.length; y++) {
			for (int i = 0; i < contestant.getTempMissions().size(); i++) {
				if (compareMissions(contestant.getTempMissions().get(i), returnedMissions[y])) {
					MissionCard m = contestant.getTempMissions().remove(i);
					mClosedStack.add(m);
				}
			}
		}
		// sort mClosedStack
		Collections.sort(mClosedStack);

		int missionsTook = 0;
		// Remove kept missions from tempMissionlist and add them to missionlist
		while (contestant.getTempMissions().size() != 0) {
			MissionCard m = contestant.getTempMissions().remove(0);
			contestant.addMission(m);
			missionsTook++;
		}

		// Send TookMissions Event to all
		for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
			itr.next().tookMissions(contestant.getId(), missionsTook);
		}
		return true;
	}
}
