package de.unisaarland.sopra.zugumzug.commands;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.data.Contestant;

public class LeaveCommand implements CommandP {

	private List<Contestant> listPlayers;
	private int playerId;
	private HashMap<Integer, Contestant> playerMap;
	private Server server;

	public LeaveCommand(List<Contestant> listPlayers, int playerId, HashMap<Integer, Contestant> playerMap,
			Server server) {
		this.listPlayers = listPlayers;
		this.playerId = playerId;
		this.playerMap = playerMap;
		this.server = server;
	}

	/**
	 * This command is send by the player when he wants to leave the game.
	 * 
	 * @return true if the command was successfully executed, false otherwise.
	 * @throws IOException
	 */

	@Override
	public boolean execute() throws IOException {

		// Remove Player from playerlist and send playerLeftEvent to all
		Contestant cont = this.playerMap.get(playerId);
		if (listPlayers.remove(cont)) {
			playerMap.remove(playerId);
			for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
				itr.next().playerLeft(playerId);
			}
			return true;
		}
		try { // TODO: if observer leave, notify all observers or only yourself?
			server.toClient(playerId).playerLeft(playerId);
		} catch (Exception e) {
		}
		return false;
	}
}
