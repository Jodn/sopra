package de.unisaarland.sopra.zugumzug.commands;

import java.io.IOException;

public interface CommandP {

	/**
	 * 
	 * @return true if the command was successfully exectued, false otherwise
	 */
	public abstract boolean execute() throws IOException;

}
