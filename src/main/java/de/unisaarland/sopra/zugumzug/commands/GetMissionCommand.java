package de.unisaarland.sopra.zugumzug.commands;

import java.io.IOException;
import java.util.List;
import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;

public class GetMissionCommand implements CommandP {

	private MersenneTwister twister;
	private Server server;
	private List<MissionCard> mClosedStack;
	private Contestant contestant;

	public GetMissionCommand(List<MissionCard> mClosedStack, Contestant contestant, MersenneTwister twister,
			Server server) {
		this.mClosedStack = mClosedStack;
		this.contestant = contestant;
		this.twister = twister;
		this.server = server;
	}

	/**
	 * The command that handles a turn in which a player wants to get new
	 * missions.
	 * 
	 * @return true if the command was successfully executed, false otherwise.
	 * @throws IOException
	 */

	@Override
	public boolean execute() throws IOException {

		// Check if mClosedStack is empty
		if (mClosedStack.size() == 0) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					server.toClient(contestant.getId()).failure("Error in GetMissionCommand: mission stack is empty.");
				}
			} catch (Exception e) {
			}
			return false;
		}

		// Check how much missions player can draw
		int mleft = 3;
		if (mClosedStack.size() < 3) {
			mleft = mClosedStack.size();
		}

		// Draw tempMissions, add to contestant and Mission array
		// ArrayList<Mission> mlist = new ArrayList<Mission>();
		Mission[] marr = new Mission[mleft];
		for (int i = 0; i < mleft; i++) {
			int tw = twister.nextInt(mClosedStack.size() - 1);
			MissionCard m1 = mClosedStack.remove(tw);
			contestant.getTempMissions().add(m1);
			Mission missEv1 = new Mission((short) m1.getCity1().getId(), (short) m1.getCity2().getId(), m1.getPoints());
			marr[i] = missEv1;
		}

		try {
			server.toClient(contestant.getId()).newMissions(marr);
		} catch (Exception e) {
		}
		return true;
	}
}
