package de.unisaarland.sopra.zugumzug.commands;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.Contestant;

public class PayTunnelCommand implements CommandP {

	private int numLocs, trackId;
	private Contestant contestant;
	private Server server;
	private Board board;
	private List<TrackKind> rClosedStack;
	private TrackKind kind; // Color contestant wants to pay with

	public PayTunnelCommand(Board board, Contestant contestant, int numLocs, Server server, TrackKind color,
			List<TrackKind> rClosedStack, int trackId) {
		this.numLocs = numLocs;
		this.trackId = trackId;
		this.contestant = contestant;
		this.server = server;
		this.board = board;
		this.rClosedStack = rClosedStack;
		this.kind = color;
	}

	/**
	 * Sorts cards in rClosedStack
	 * 
	 * @param list
	 * @param card
	 */
	private void AddCard(List<TrackKind> list, TrackKind card) {
		// If last element of list is less or equal to card, add card
		// else go through list and find equal or greater element.
		if (list.isEmpty() || list.get(list.size() - 1).num() <= card.num()) {
			list.add(card);
		} else {
			int x;
			for (x = 0; x < list.size() && list.get(x).num() < card.num(); x++) {
			}
			list.add(x, card);
		}
	}

	/**
	 * Sends failure event with given message
	 * 
	 * @param s String with given failure message
	 */
	private boolean failMessage(String s) {
		try {
			if (contestant.getNumFails() == 3) {
				return false;
			} else {
				server.toClient(contestant.getId()).failure(s);
			}
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * This command can only follow after the BuildTrack command (also handles
	 * tunnels). And handles the additional costs as well as the normal turn.
	 * 
	 * @return true if the command was successfully executed, false otherwise.
	 * @throws IOException
	 */

	@Override
	public boolean execute() throws IOException {
	// Check if Command is valid:
		// Check if numLocs greater than addCosts
		if (numLocs < 0 || numLocs > contestant.getAddCosts()) {
			return failMessage("Error in PayTunnelCommand: Number of locs isn't valid.");
		}

		// Check if AddCosts are set
		if (!(contestant.getAddCosts() >= 0)) {
			return failMessage("Error in PayTunnelCommand: AddCosts aren't set");
		}

		// Check if track is a tunnel
		if (board.getListTracks().get(trackId).isTunnel() == false) {
			return failMessage("Error in PayTunnelCommand: Track is not a tunnel");
		}

		// Check if contestant wants to pay with too much locs
		numLocs = numLocs + contestant.getTmpLocs();
		int costs = board.getListTracks().get(trackId).getLength() + contestant.getAddCosts();
		if (numLocs > costs) {
			return failMessage("Error in PayTunnelCommand: Too much locs paid");
		}

		// Check if contestant want to pay with right color
		TrackKind color = board.getListTracks().get(trackId).getColor();
		if (!(kind.equals(color)) && !color.equals(TrackKind.ALL) && !kind.equals(TrackKind.ALL)) {
			return failMessage("Error in PayTunnelCommand: Wrong colors.");
		}

		// Check if contestant wants to pay with right num of locs / has enough
		// resources
		// count cards in right color and locs
		int rescount = 0;
		int loccount = 0;
		for (Iterator<TrackKind> itr = contestant.getHandCards().iterator(); itr.hasNext();) {
			TrackKind current = itr.next();
			if (current.equals(kind)) {
				rescount++;
			}
			if (current.equals(TrackKind.ALL)) {
				loccount++;
			}
		}

		// Check if cont has enough resources
		if (loccount < numLocs || rescount < costs - numLocs) {
			return failMessage("Error in PayTunnelCommand: Not enough resources.");
		}

		// If TrackKind is ALL, rescount and loccount are the same
		if (kind.equals(TrackKind.ALL) && loccount < costs) {
			return failMessage("Error in PayTunnelCommand: Not enouth locs.");
		}

	// Build Tunnel:

		// Remove locs from contestant and add them to rClosedStack
		for (int i = numLocs; i > 0; i--) {
			contestant.getHandCards().remove(TrackKind.ALL);
			rClosedStack.add(TrackKind.ALL);
		}

		// Remove resources from contestant and add them to rClosedStack
		for (int x = costs - numLocs; x > 0; x--) {
			contestant.getHandCards().remove(kind);
			AddCard(rClosedStack, kind);
		}

		// sort rClosedStack
		// Collections.sort(rClosedStack);

		// Build tunnel: change OwnerId and add track to contestant
		board.getListTracks().get(trackId).setOwnerId(contestant.getId());
		contestant.getListTracks().add(board.getListTracks().get(trackId));

		// remove trains, add points and set AddCosts -1
		int trains = contestant.getTrainsLeft() - board.getListTracks().get(trackId).getLength();
		contestant.setTrainsLeft(trains);
		int newPoints = contestant.getPoints() + board.getListTracks().get(trackId).getPoints();
		contestant.setPoints(newPoints);

		// Send TunnelBuild event to all contestants
		for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
			itr.next().tunnelBuilt(contestant.getId(), (short) trackId, contestant.getAddCosts(), kind, numLocs);
		}

		contestant.setAddCosts(-1);
		return true;
	}
}
