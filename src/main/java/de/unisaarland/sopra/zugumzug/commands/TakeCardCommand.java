package de.unisaarland.sopra.zugumzug.commands;

import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.Server;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TakeCardCommand implements CommandP {

	private Contestant contestant;
	private Server server;
	private MersenneTwister twister;
	private List<TrackKind> rClosedStack;
	private List<TrackKind> openResources;
	private TrackKind chosenKind;
	private int maxHandCards;

	public TakeCardCommand(Contestant contestant, Server server, MersenneTwister twister, List<TrackKind> rClosedStack,
			List<TrackKind> openResources, TrackKind chosenKind, int maxHandCards) {
		this.contestant = contestant;
		this.server = server;
		this.twister = twister;
		this.rClosedStack = rClosedStack;
		this.openResources = openResources;
		this.chosenKind = chosenKind;
		this.maxHandCards = maxHandCards;
	}

	/**
	 * Draw new open card and send NewOpenCardEvent to all clients.
	 * 
	 * @return the drawn card or null if rClosedStack is empty
	 * @throws IOException
	 */

	private TrackKind drawOpenCard() throws IOException {
		// Draw new open card
		Collections.sort(rClosedStack);
		if (rClosedStack.size() > 0) {
			int tw = twister.nextInt(rClosedStack.size() - 1);
			TrackKind card = rClosedStack.remove(tw);
			openResources.add(card);

			// Send NewOpenCardEvent to all clients.
			for (Iterator<ClientConnector> it = server.allClients().iterator(); it.hasNext();) {
				it.next().newOpenCard(card);
			}
			return card;
		}
		return null;
	}

	/**
	 * The command hat handles a normal turn in which a player wants to draw a
	 * open resource card.
	 * 
	 * @return true if the command was successfully executed, false otherwise.
	 * @throws IOException
	 */

	@Override
	public boolean execute() throws IOException {

		if (contestant.getHandCards().size() >= maxHandCards) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					server.toClient(contestant.getId())
							.failure("Error in TakeCardCommand: You already have the max amount of hand cards");
				}
			} catch (IndexOutOfBoundsException | IllegalStateException e) {
			}
			return false;
		}

		// Check if cards left on OpenStack
		if (openResources.size() == 0) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					server.toClient(contestant.getId()).failure("Error in TakeCardCommand: There are no open cards");
				}
			} catch (IndexOutOfBoundsException | IllegalStateException e) {
			}
			return false;
		}

		// Check if openResourceStack contains wanted card
		int position = openResources.indexOf(chosenKind);
		if (position == -1) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					server.toClient(contestant.getId())
							.failure("Error in TakeCardCommand: The color you chose is not on the open stack");
				}
			} catch (IndexOutOfBoundsException | IllegalStateException e) {
			}
			return false;
		}

		// Remove card from openStack and add it to contestant
		TrackKind card = openResources.remove(position);
		contestant.getHandCards().add(card);

		// Send TookCard Event to all clients.
		for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
			itr.next().tookCard(contestant.getId(), chosenKind);
		}

		// If rClosedStack has cards left:
		// Draw new open card and send NewOpenCardEvent to all clients.
		if (rClosedStack.size() > 0) {
			TrackKind newCard = drawOpenCard();

			// check if there a three locs open
			int OpenLocs = 0;
			if (newCard.equals(TrackKind.ALL)) {
				for (int i = 0; i < openResources.size(); i++) {
					if (openResources.get(i).equals(TrackKind.ALL)) {
						OpenLocs++;
					}
				}

				// If more than three locs open, add old cards in rClosedStack
				// an draw 5 new open cards.
				if (OpenLocs >= 3) {
					while (!openResources.isEmpty()) {
						rClosedStack.add(openResources.remove(0));
					}
					Collections.sort(rClosedStack);

					for (int i = 0; i < 5; i++) {
						drawOpenCard();
					}
				}
			}
		}
		return true;
	}
}
