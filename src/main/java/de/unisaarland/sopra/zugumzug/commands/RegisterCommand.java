package de.unisaarland.sopra.zugumzug.commands;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.data.Contestant;

public class RegisterCommand implements CommandP {

	private int id;
	private Server server;
	private String name;
	private List<Contestant> playerList;
	private HashMap<Integer, Contestant> playerMap;
	private int maxTracks;

	public RegisterCommand(int id, String name, Server server, List<Contestant> playerList,
			HashMap<Integer, Contestant> playerMap, int maxTracks) {
		this.id = id;
		this.name = name;
		this.server = server;
		this.playerList = playerList;
		this.playerMap = playerMap;
		this.maxTracks = maxTracks;
	}

	/**
	 * This command upgrades a observer(see KomLib) to a Player.
	 * 
	 * @return true if the command was successfully executed, false otherwise.
	 * @throws IOException
	 */

	@Override
	public boolean execute() throws IOException {

		// If id already exists, change name of player
		if (playerMap.containsKey(id)) {
			playerMap.get(id).setName(name);

			// Send newPlayer Event to all clients
			for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
				itr.next().newPlayer(id, name);
			}
			return true;
		}

		// Create new contestant and add in playerList and playerMap
		Contestant cont = new Contestant(id, name, maxTracks);
		addPlayer(cont);
		playerMap.put(id, cont);

		// Send newPlayer Event to all clients
		for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
			itr.next().newPlayer(id, name);
		}
		return true;
	}

	/**
	 * Adds new player in right order in playerlist
	 */
	private void addPlayer(Contestant cont) {
		int i = 0;
		while (i < playerList.size() && playerList.get(i).getId() < cont.getId()) {
			i++;
		}
		if (i > playerList.size() - 1) {
			playerList.add(playerList.size(), cont);
		} else
			playerList.add(i, cont);
	}
}
