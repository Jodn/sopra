package de.unisaarland.sopra.zugumzug.commands;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.Server;

public class TakeSecretCardCommand implements CommandP {

	private Contestant contestant;
	private Server server;
	private MersenneTwister twister;
	private List<TrackKind> rClosedStack;
	private int maxHandCards;
	private int numCards;
	private int[] cardArray;

	public TakeSecretCardCommand(Contestant contestant, Server server, MersenneTwister twister,
			List<TrackKind> rClosedStack, int maxHandCards, int numCards) {
		this.contestant = contestant;
		this.server = server;
		this.twister = twister;
		this.rClosedStack = rClosedStack;
		this.maxHandCards = maxHandCards;
		this.numCards = numCards;
		this.cardArray = new int[9];
	}

	/**
	 * Handles the turn in which a player wants to draw a covered secret Card
	 * from the closed resource Stack.
	 * 
	 * @return true if the command was successfully executed, false otherwise.
	 * @throws IOException
	 */

	@Override
	public boolean execute() throws IOException {

		if (contestant.getHandCards().size() >= maxHandCards) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					server.toClient(contestant.getId()).failure(
							"Error in TakeSecretCardCommand: You already have the max amount of allowed Handcards");
				}
			} catch (IndexOutOfBoundsException | IllegalStateException e) {
			}
			return false;
		}

		// Check if cards left on rClosedStack
		if (rClosedStack.size() == 0) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					server.toClient(contestant.getId())
							.failure("Error in TakeSecretCardCommand: There are no Cards left on the Closed Stack");
				}
			} catch (Exception e) {
			}
			return false;
		}

		for (int z = 0; z < numCards; z++) {
			// Draw random card and add it to contestant
			int tw = twister.nextInt(rClosedStack.size() - 1);
			TrackKind card = rClosedStack.remove(tw);
			contestant.getHandCards().add(card);

			switch (card) {
			case ALL:
				cardArray[0]++;
				break;
			case BLACK:
				cardArray[1]++;
				break;
			case BLUE:
				cardArray[2]++;
				break;
			case GREEN:
				cardArray[3]++;
				break;
			case ORANGE:
				cardArray[4]++;
				break;
			case RED:
				cardArray[5]++;
				break;
			case VIOLET:
				cardArray[6]++;
				break;
			case WHITE:
				cardArray[7]++;
				break;
			case YELLOW:
				cardArray[8]++;
				break;
			default:
				break;

			}
		}
		// send newCards Event to contestant
		for (int i = 0; i < 9; i++) {
			TrackKind card;
			switch (i) {
			case 0:
				card = TrackKind.ALL;
				break;
			case 1:
				card = TrackKind.BLACK;
				break;
			case 2:
				card = TrackKind.BLUE;
				break;
			case 3:
				card = TrackKind.GREEN;
				break;
			case 4:
				card = TrackKind.ORANGE;
				break;
			case 5:
				card = TrackKind.RED;
				break;
			case 6:
				card = TrackKind.VIOLET;
				break;
			case 7:
				card = TrackKind.WHITE;
				break;
			case 8:
				card = TrackKind.YELLOW;
				break;
			default:
				card = null;
				break;
			}
			if (cardArray[i] != 0) {
				try {
					server.toClient(contestant.getId()).newCards(card, cardArray[i]);
				} catch (Exception e) {
				}
				if (numCards != 4) {
					// send TookSecretCard Event to all
					for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
						ClientConnector cc = itr.next();
						cc.tookSecretCard(contestant.getId());
					}
				}
			}
		}
		return true;
	}
}
