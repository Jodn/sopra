package de.unisaarland.sopra.zugumzug.commands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.Track;

public class BuildTrackCommand implements CommandP {

	private Board board;
	private Contestant contestant;
	private int numLocs;
	private TrackKind color;
	private int trackId;
	private Server server;
	private MersenneTwister twister;
	List<TrackKind> rClosedStack;

	public BuildTrackCommand(Board board, Contestant contestant, int numLocs, int trackId, Server server,
			TrackKind color, MersenneTwister twister, List<TrackKind> rClosedStack) {
		this.board = board;
		this.contestant = contestant;
		this.numLocs = numLocs;
		this.trackId = trackId;
		this.color = color;
		this.server = server;
		this.twister = twister;
		this.rClosedStack = rClosedStack;
	}

	/**
	 * The buildTrackCommand handles a turn in which a player wants to build a
	 * track.
	 * 
	 * @return true if the command was successfully executed, false otherwise
	 * @throws IOException
	 */
	@Override
	public boolean execute() throws IOException {
		// Build a Track
		
		if(color == null){
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					ClientConnector fail = server.toClient(contestant.getId());
					fail.failure("The color has to be valid.");
				}
			} catch (Exception e) {
				// kein client?
			}
			return false;
		}
		
		if (numLocs < 0 || numLocs > 6) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					ClientConnector fail = server.toClient(contestant.getId());
					fail.failure("Number of locomotives has to be positiv");
				}
			} catch (Exception e) {
				// kein client?
			}
			return false;
		}

		// not existing track
		if (trackId < 0 || trackId >= board.getListTracks().size()) {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					ClientConnector fail = server.toClient(contestant.getId());
					fail.failure("Number of locomotives has to be positiv");
				}
			} catch (Exception e) {
				// kein client?
			}
			return false;
		}
		Track track = board.getListTracks().get(trackId);

		// Check if contestant already build parallel track
		ArrayList<Integer> parallel = new ArrayList<Integer>();
		for (Iterator<Integer> it = track.getCity1().getMap().keySet().iterator(); it.hasNext();) {
			int tmpTrack = it.next();
			if (track.getCity2().equals(track.getCity1().getMap().get(tmpTrack)) && tmpTrack != track.getId())
				parallel.add(tmpTrack);
		}

		for (int l = 0; l < parallel.size(); l++) {
			Track road = board.getListTracks().get(parallel.get(l));
			if (road.getOwnerId() == contestant.getId()) {
				try {
					if (contestant.getNumFails() == 3) {
						return false;
					} else {
						ClientConnector fail = server.toClient(contestant.getId());
						fail.failure("You are not allowed to build 2 parallel tracks");
					}
				} catch (Exception e) {
					// kein client?
				}
				return false;

			}
		}

		if (!track.isTunnel()) {
			if (track.getOwnerId() == -1) // free track
			{
				if (track.getColor() == TrackKind.ALL || color == track.getColor() || color == TrackKind.ALL) {
					int count_kind = 0;
					int count_locs = 0;
					if (color == TrackKind.ALL) {
						for (int i = 0; i < contestant.getHandCards().size(); i++) {
							if (contestant.getHandCards().get(i) == TrackKind.ALL)
								count_locs++;
						}
					} else {
						for (int i = 0; i < contestant.getHandCards().size(); i++) {
							if (contestant.getHandCards().get(i) == color)
								count_kind++;
							if (contestant.getHandCards().get(i) == TrackKind.ALL)
								count_locs++;
						}
					}

					if (count_locs >= numLocs) {
						if ((numLocs + count_kind >= track.getLength() && numLocs <= track.getLength())
								|| (count_locs >= track.getLength() && color == TrackKind.ALL
										&& numLocs <= track.getLength())) {
							if (contestant.getTrainsLeft() >= track.getLength()) {
								int x = contestant.getTrainsLeft();
								track.setOwnerId(contestant.getId());
								contestant.setPoints(contestant.getPoints() + track.getPoints());
								contestant.getListTracks().add(track);
								contestant.setTrainsLeft(x - track.getLength());

								if (color == TrackKind.ALL) {
									for (int b = 0; b < track.getLength(); b++) {
										contestant.getHandCards().remove(TrackKind.ALL);
										rClosedStack.add(TrackKind.ALL);
									}
								} else {
									for (int a = 0; a < track.getLength() - numLocs; a++) {
										contestant.getHandCards().remove(color);
										rClosedStack.add(color);
									}
									for (int b = 0; b < numLocs; b++) {
										contestant.getHandCards().remove(TrackKind.ALL);
										rClosedStack.add(TrackKind.ALL);
									}
								}

								Collections.sort(rClosedStack);

								try {

									for (Iterator<ClientConnector> send = server.allClients().iterator(); send
											.hasNext();)
										send.next().trackBuilt(contestant.getId(), (short) track.getId(), color,
												numLocs);
								} catch (Exception e) {
									// kein client?
								}

								return true;
							} else {
								try {
									if (contestant.getNumFails() == 3) {
										return false;
									} else {
										ClientConnector fail = server.toClient(contestant.getId());
										fail.failure("You don't have enough trains to build this track.");
									}

								} catch (Exception e) {
									// kein client?
								}
								return false; // not enough trains
							}
						}

						else {
							try {
								if (contestant.getNumFails() == 3) {
									return false;
								} else {
									ClientConnector fail = server.toClient(contestant.getId());
									fail.failure("You don't have enough Cards to buld this track.");
								}
							} catch (Exception e) {
								// kein client?
							}
							return false; // not enough cards
						}
					} else {
						try {
							if (contestant.getNumFails() == 3) {
								return false;
							} else {
								ClientConnector fail = server.toClient(contestant.getId());
								fail.failure("You don't have enough locomotives.");
							}
						} catch (Exception e) {
							// kein client?
						}
						return false; // not enough locomotives
					}
				} else {
					try {
						if (contestant.getNumFails() == 3) {
							return false;

						} else {
							ClientConnector fail = server.toClient(contestant.getId());
							fail.failure("You don't have the right color.");
						}
					} catch (Exception e) {
						// kein client?
					}
					return false; // Wrong color
				}
			} else {
				try {
					if (contestant.getNumFails() == 3) {
						return false;
					} else {
						ClientConnector fail = server.toClient(contestant.getId());
						fail.failure("This track was already built.");
					}
				} catch (Exception e) {
					// kein client?
				}
				return false; // Not a free Track
			}
		} else
			// Tunnel
			if (track.getOwnerId() == -1) {
			contestant.setTmpLocs(numLocs);
			int count_color = 0;
			int countLocs = 0;
			contestant.setTunnelKind(color);
			if (color == TrackKind.ALL) {
				for (int c = 0; c < contestant.getHandCards().size(); c++) {
					if (contestant.getHandCards().get(c) == TrackKind.ALL)
						countLocs++;
				}
			} else {
				for (int c = 0; c < contestant.getHandCards().size(); c++) {

					if (contestant.getHandCards().get(c) == color && color != TrackKind.ALL)
						count_color++;
					if (contestant.getHandCards().get(c) == TrackKind.ALL)
						countLocs++;
				}
			}

			if (countLocs >= numLocs) {
				if ((numLocs + count_color >= track.getLength() && numLocs <= track.getLength())
						|| (countLocs >= track.getLength() && color == TrackKind.ALL
								&& numLocs <= track.getLength())) {
					if (color == track.getColor() || track.getColor() == TrackKind.ALL || color == TrackKind.ALL) {
						int count_costs = 0;
						List<TrackKind> tempCards = new ArrayList<TrackKind>();

						for (int i = 0; i < 3; i++) {
							if (!rClosedStack.isEmpty()) {
								int seed = twister.nextInt(rClosedStack.size() - 1);
								TrackKind tmp = rClosedStack.remove(seed);
								tempCards.add(tmp);
								if (tmp == color || tmp == TrackKind.ALL) {
									count_costs++;
								}
							}
						}

						for (int i = 0; i < tempCards.size(); i++)
							rClosedStack.add(tempCards.get(i));
						Collections.sort(rClosedStack);

						if ((track.getLength() + count_costs <= countLocs + count_color && color != TrackKind.ALL)
								|| (color == TrackKind.ALL && countLocs >= (track.getLength() + count_costs)
										&& numLocs <= track.getLength())) {
							contestant.setTunnelID(track.getId());
							contestant.setAddCosts(count_costs);

							try {
								ClientConnector send_costs = server.toClient(contestant.getId());
								send_costs.additionalCost(count_costs);
							} catch (Exception e) {
								// kein client?
							}
							return true;
						} else //// Not enough cards + costs
						{
							try {
								for (Iterator<ClientConnector> send = server.allClients().iterator(); send.hasNext();) {
									send.next().tunnelNotBuilt(contestant.getId(), (short) track.getId());
								}
							} catch (Exception e) {
								// kein client?
							}
							return true;
						}
					}

					else {
						try {
							if (contestant.getNumFails() == 3) {
								return false;
							} else {
								ClientConnector fail = server.toClient(contestant.getId());
								fail.failure("You don't have enough Cards to build this tunnel.");

							}
						} catch (Exception e) { // No Contestant?

						}
						return false; // Not enough Cards for that tunnel
					}
				} else {
					try {
						if (contestant.getNumFails() == 3) {
							return false;
						} else {
							ClientConnector fail = server.toClient(contestant.getId());
							fail.failure("You don't have the right color.");
						}
					} catch (Exception e) { // No Contestant?

					}
					return false; // Wrong color
				}
			} else {
				try {
					if (contestant.getNumFails() == 3) {
						return false;
					} else {
						ClientConnector fail = server.toClient(contestant.getId());
						fail.failure("You don't have enough locomotives.");
					}
				} catch (Exception e) { // No Contestant?

				}
				return false; // Not enough locomotives
			}
		} else {
			try {
				if (contestant.getNumFails() == 3) {
					return false;
				} else {
					ClientConnector fail = server.toClient(contestant.getId());
					fail.failure("This tunnel was already built.");
				}
			} catch (Exception e) {
				// kein client?
			}
			return false; // Not a free tunnel
		}
	}
}
