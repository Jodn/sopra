package de.unisaarland.sopra.zugumzug.data;

import de.unisaarland.sopra.TrackKind;

public class Track {

	private int id;
	private City city1, city2;
	private int length;
	private TrackKind color;
	private int points;
	private int ownerId;
	private boolean isTunnel;

	public Track(int id, City city1, City city2, int length, int points, boolean isTunnel, TrackKind color) { // private
																												// TrackKind
		this.id = id; // color
		this.setCity1(city1);
		this.setCity2(city2);
		this.setLength(length);
		this.setPoints(points);
		this.ownerId = -1;
		this.setTunnel(isTunnel);
		this.color = color;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public City getCity1() {
		return city1;
	}

	public void setCity1(City city1) {
		this.city1 = city1;
	}

	public City getCity2() {
		return city2;
	}

	public void setCity2(City city2) {
		this.city2 = city2;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	public boolean isTunnel() {
		return isTunnel;
	}

	public void setTunnel(boolean isTunnel) {
		this.isTunnel = isTunnel;
	}

	public TrackKind getColor() {
		return color;
	}

	public void setColor(TrackKind color) {
		this.color = color;
	}

	public boolean equals(Object e) {

		if (!(e instanceof Track))
			return false;

		Track t = (Track) e;
		if (this.id == t.getId()) {
			return true;
		} else {
			return false;
		}
	}

	public int hashCode() {
		return id;
	}

	@Override
	public String toString() {
		return id + " " + city1.toString() + " - " + city2.toString();
	}

}
