package de.unisaarland.sopra.zugumzug.data;

import java.util.HashMap;
import java.util.List;

import de.unisaarland.sopra.TrackKind;

public class GameEngine {

	private List<Contestant> playerList;
	private static Board board;
	private List<TrackKind> openResources;
	private int maxTracks;
	private HashMap<Integer, Contestant> playerMap;

	/**
	 * adds a Contestant to HashMap playerMap and to List playerList
	 * 
	 * @param contestant
	 */
	public void addContestant(Contestant contestant) {
		playerList.add(contestant);
		playerMap.put(playerMap.size(), contestant);
	}

	public List<Contestant> getPlayerList() {
		return playerList;
	}

	public void setPlayerList(List<Contestant> playerList) {
		this.playerList = playerList;
	}

	public Board getBoard() {
		return board;
	}

	public static void setBoard(Board board) {
		GameEngine.board = board;
	}

	public int getMaxTracks() {
		return maxTracks;
	}

	public void setMaxTracks(int maxTracks) {
		this.maxTracks = maxTracks;
	}

	public List<TrackKind> getOpenResources() {
		return openResources;
	}

	public void setOpenResources(List<TrackKind> openResources) {
		this.openResources = openResources;
	}

	public HashMap<Integer, Contestant> getPlayerMap() {
		return playerMap;
	}

	public void setPlayerMap(HashMap<Integer, Contestant> playerMap) {
		this.playerMap = playerMap;
	}

}
