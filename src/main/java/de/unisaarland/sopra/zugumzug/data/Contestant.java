package de.unisaarland.sopra.zugumzug.data;

import java.util.ArrayList;
import java.util.List;

import de.unisaarland.sopra.TrackKind;

public class Contestant {

	private int id;
	private int points;
	private String name;
	private int numFails;
	private int trainsLeft;
	private List<MissionCard> listMissions;
	private List<MissionCard> tempMissions;
	private List<TrackKind> handCards;
	private List<Track> listTracks;
	private int addCosts;
	// private Track tempTunnelTrack;
	private int numFinishedMissions;
	private TrackKind TunnelKind;
	private int TunnelID;
	private int tmpLocs;

	public Contestant(int id, String name, int trainsLeft) {
		this.setId(id);
		this.setName(name);
		this.setTrainsLeft(trainsLeft);
		this.setListMissions(new ArrayList<MissionCard>());
		this.setListTracks(new ArrayList<Track>());
		this.setTempMissions(new ArrayList<MissionCard>());
		this.handCards = new ArrayList<TrackKind>();
		this.setAddCosts(-1);
		this.numFails = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPoints() {
		return points;
	}

	public boolean equals(Object e) {
		if (e instanceof Contestant) {
			Contestant c = (Contestant) e;
			if (this.id == c.getId()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTrainsLeft() {
		return trainsLeft;
	}

	public void setTrainsLeft(int trainsLeft) {
		this.trainsLeft = trainsLeft;
	}

	public int getNumFails() {
		return numFails;
	}

	public void setNumFails(int numFails) {
		this.numFails = numFails;
	}

	public List<MissionCard> getListMissions() {
		return listMissions;
	}

	public void setListMissions(List<MissionCard> listMissions) {
		this.listMissions = listMissions;
	}

	public List<MissionCard> getTempMissions() {
		return tempMissions;
	}

	public void setTempMissions(List<MissionCard> tempMissions) {
		this.tempMissions = tempMissions;
	}

	public List<Track> getListTracks() {
		return listTracks;
	}

	public void setListTracks(List<Track> listTracks) {
		this.listTracks = listTracks;
	}

	public boolean isOwner(Track road) {
		return listTracks.contains(road);
	}

	public List<TrackKind> getHandCards() {
		return handCards;
	}

	public void setHandCards(List<TrackKind> handCards) {
		this.handCards = handCards;
	}

	/*
	 * public Track getTempTunnelTrack() { return tempTunnelTrack; }
	 * 
	 * public void setTempTunnelTrack(Track tempTunnelTrack) {
	 * this.tempTunnelTrack = tempTunnelTrack; }
	 */

	public int getAddCosts() {
		return addCosts;
	}

	public void setAddCosts(int addCosts) {
		this.addCosts = addCosts;
	}

	public int getNumFinishedMissions() {
		return numFinishedMissions;
	}

	public void setNumFinishedMissions(int numFinishedMissions) {
		this.numFinishedMissions = numFinishedMissions;
	}

	public TrackKind getTunnelKind() {
		return TunnelKind;
	}

	public void setTunnelKind(TrackKind kind) {
		this.TunnelKind = kind;
	}

	public int getTunnelID() {
		return TunnelID;
	}

	public void setTunnelID(int ID) {
		this.TunnelID = ID;
	}

	public int hashCode() {
		return id;
	}

	public void addTrack(Track track) {
		this.listTracks.add(track);
	}

	public void addResource(TrackKind color) {
		this.handCards.add(color);
	}

	public void addMission(MissionCard m) {
		this.listMissions.add(m);
	}

	public void removeResource(TrackKind color) {
		this.handCards.remove(color);
	}

	@Override
	public String toString() {
		return this.id + " " + this.name;
	}

	public int getTmpLocs() {
		return tmpLocs;
	}

	public void setTmpLocs(int tmpLocs) {
		this.tmpLocs = tmpLocs;
	}

}
