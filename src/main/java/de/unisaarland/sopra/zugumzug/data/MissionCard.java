package de.unisaarland.sopra.zugumzug.data;

public class MissionCard implements Comparable<MissionCard> {

	private City city1, city2;
	private int points;
	private int id;

	public MissionCard(City city1, City city2, int points, int id) {
		this.setCity1(city1);
		this.setCity2(city2);
		this.setPoints(points);
		this.setId(id);

	}

	public City getCity1() {
		return city1;
	}

	public void setCity1(City city1) {
		this.city1 = city1;
	}

	public City getCity2() {
		return city2;
	}

	public void setCity2(City city2) {
		this.city2 = city2;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean equals(Object e) {

		if (e instanceof MissionCard) {

			MissionCard m = (MissionCard) e;
			if (this.id == m.getId()) {
				return true;
			} else if (m.getCity1().equals(city1) && m.getCity2().equals(city2)) {
				return true;
			} else if (m.getCity2().equals(city1) && m.getCity1().equals(city2)) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		return id;
	}

	@Override
	public int compareTo(MissionCard o) {
		return Integer.compare(this.id, o.getId());
	}

}
