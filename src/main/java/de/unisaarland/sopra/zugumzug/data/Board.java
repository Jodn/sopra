package de.unisaarland.sopra.zugumzug.data;

import java.util.ArrayList;
import java.util.List;

public class Board {

	private List<Track> listTracks;
	private List<City> listCities;

	public Board() {
		this.setListCities(new ArrayList<City>());
		this.setListTracks(new ArrayList<Track>());
	}

	public List<Track> getListTracks() {
		return listTracks;
	}

	public void setListTracks(List<Track> listTracks) {
		this.listTracks = listTracks;
	}

	public List<City> getListCities() {
		return listCities;
	}

	public void setListCities(List<City> listCities) {
		this.listCities = listCities;
	}

	public void update(Track track) {
		listTracks.add(track);
		City city1 = track.getCity1();
		City city2 = track.getCity2();
		city1.getMap().put(track.getId(), city2);
		city2.getMap().put(track.getId(), city1);

	}

}
