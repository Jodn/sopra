package de.unisaarland.sopra.zugumzug.data;

import java.util.HashMap;

public class City {

	private String name;
	private int id, x, y;
	private HashMap<Integer, City> map; // Name!!! track id auf City

	public City(String name, int id, int x, int y) {
		this.setName(name);
		this.id = id;
		this.setX(x);
		this.setY(y);
		setMap(new HashMap<Integer, City>());

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public HashMap<Integer, City> getMap() {
		return map;
	}

	public void setMap(HashMap<Integer, City> map) {
		this.map = map;
	}

	public boolean equals(Object e) {

		if (!(e instanceof City))
			return false;

		City c = (City) e;
		if (this.id == c.getId()) {
			return true;
		} else {
			return false;
		}
	}

	public int hashCode() {
		return id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
