package de.unisaarland.sopra.zugumzug.moves;

import java.io.IOException;

import de.unisaarland.sopra.Player;

public class TakeSecretCardMove implements Moves {
	
	private Player player;
	
	
	public TakeSecretCardMove(Player player) {
		this.player = player;
	}

	@Override
	public int getId() {
		return 6;
	}

	@Override
	public void execute() throws IOException {
		player.takeSecretCard();

	}

}
