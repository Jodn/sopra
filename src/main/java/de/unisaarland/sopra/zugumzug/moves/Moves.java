package de.unisaarland.sopra.zugumzug.moves;

import java.io.IOException;

public interface Moves {

	public int getId();

	public void execute() throws IOException;
}
