package de.unisaarland.sopra.zugumzug.moves;

import java.io.IOException;

import de.unisaarland.sopra.Player;
import de.unisaarland.sopra.TrackKind;

public class TakeCardMove implements Moves {

	private TrackKind kind;
	private Player player;
	
	public TakeCardMove(TrackKind kind, Player player) {
		this.kind = kind;
		this.player = player;
	}

	public TrackKind getKind() {
		return kind;
	}

	@Override
	public int getId() {
		return 5;
	}

	@Override
	public void execute() throws IOException {
		player.takeCard(kind);

	}

}
