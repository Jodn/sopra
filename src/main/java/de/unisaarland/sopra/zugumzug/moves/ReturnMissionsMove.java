package de.unisaarland.sopra.zugumzug.moves;

import java.io.IOException;
import java.util.List;

import de.unisaarland.sopra.zugumzug.data.MissionCard;

public class ReturnMissionsMove implements Moves {

	private List<MissionCard> missions;

	public ReturnMissionsMove(List<MissionCard> missions) {
		this.missions = missions;
	}

	public List<MissionCard> getMissions() {
		return missions;
	}

	@Override
	public int getId() {
		return 4;
	}

	@Override
	public void execute() throws IOException {
		// TODO Auto-generated method stub

	}

}
