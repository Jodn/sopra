package de.unisaarland.sopra.zugumzug.moves;

import java.io.IOException;

import de.unisaarland.sopra.TrackKind;

public class BuildTrackMove implements Moves {

	private TrackKind kind;
	private int trackId;
	private int numlocs;

	public BuildTrackMove(TrackKind kind, int trackId, int numlocs) {
		this.kind = kind;
		this.trackId = trackId;
		this.numlocs = numlocs;

	}

	public int getTrackId() {
		return trackId;
	}

	public int getNumlocs() {
		return numlocs;
	}

	public TrackKind getKind() {
		return kind;
	}

	@Override
	public int getId() {

		return 0;
	}

	@Override
	public void execute() throws IOException {
		// TODO Auto-generated method stub

	}

}
