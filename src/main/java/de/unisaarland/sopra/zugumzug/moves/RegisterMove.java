package de.unisaarland.sopra.zugumzug.moves;

import java.io.IOException;

import de.unisaarland.sopra.zugumzug.client.PlayerLogic;

public class RegisterMove implements Moves {

	private String myName;

	public RegisterMove(String myName) {

		this.myName = myName;
	}

	public String getMyName() {
		return myName;
	}

	@Override
	public int getId() {
		return 3;
	}

	@Override
	public void execute() throws IOException {
		// TODO Auto-generated method stub

	}

}
