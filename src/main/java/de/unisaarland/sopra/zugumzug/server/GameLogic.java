package de.unisaarland.sopra.zugumzug.server;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.BuildTrack;
import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.Command;
import de.unisaarland.sopra.Leave;
import de.unisaarland.sopra.MersenneTwister;
import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.NewObserver;
import de.unisaarland.sopra.PayTunnel;
import de.unisaarland.sopra.Register;
import de.unisaarland.sopra.ReturnMissions;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.TakeCard;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.internal.LoggingServer;
import de.unisaarland.sopra.zugumzug.commands.BuildTrackCommand;
import de.unisaarland.sopra.zugumzug.commands.CommandP;
import de.unisaarland.sopra.zugumzug.commands.GetMissionCommand;
import de.unisaarland.sopra.zugumzug.commands.LeaveCommand;
import de.unisaarland.sopra.zugumzug.commands.PayTunnelCommand;
import de.unisaarland.sopra.zugumzug.commands.RegisterCommand;
import de.unisaarland.sopra.zugumzug.commands.ReturnMissionCommand;
import de.unisaarland.sopra.zugumzug.commands.TakeCardCommand;
import de.unisaarland.sopra.zugumzug.commands.TakeSecretCardCommand;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.GameEngine;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;


public class GameLogic extends GameEngine {

	private Server server;
	private MersenneTwister twister;
	private List<TrackKind> rClosedStack;
	private List<MissionCard> mClosedStack;
	private int requiredPlayers;
	private int maxHandCards;
	private int turnsLeft = 2;
	private String fileName;
	private EndGame endGame;
	private boolean nextPayTunnel;
	private boolean nextReturnMission;
	private static int trackId = 0;
	private static int cityId = 0;
	private static int missionId = 0;
	private HashMap<Integer, City> mapCities;
	private HashMap<Integer, Integer> failingObserver;
	private ArrayList<TrackKind> parsedColors;

	public GameLogic(int port, String fileName, int seed, int requiredPlayers) throws IOException {
		this.setRequiredPlayers(requiredPlayers);
		this.setFileName(fileName);
		this.server = new Server(port);
		this.setmClosedStack(new ArrayList<MissionCard>());

		this.setrClosedStack(new ArrayList<TrackKind>());
		GameEngine.setBoard(new Board());
		this.failingObserver = new HashMap<Integer, Integer>();

		mapCities = new HashMap<Integer, City>();
		this.setPlayerList(new ArrayList<Contestant>());
		this.setPlayerMap(new HashMap<Integer, Contestant>());
		twister = new MersenneTwister(seed);
		this.setOpenResources(new ArrayList<TrackKind>());
		parsedColors = new ArrayList<TrackKind>();
	}

	public GameLogic(int port, String fileName, int seed, int requiredPlayers, LoggingServer logServer)
			throws IOException {
		this.setRequiredPlayers(requiredPlayers);
		this.setFileName(fileName);
		this.server = logServer;
		this.setmClosedStack(new ArrayList<MissionCard>());

		this.setrClosedStack(new ArrayList<TrackKind>());
		GameEngine.setBoard(new Board());

		this.endGame = new EndGame(getBoard(), getPlayerList(), server);
		mapCities = new HashMap<Integer, City>();
		this.setPlayerList(new ArrayList<Contestant>());
		this.setPlayerMap(new HashMap<Integer, Contestant>());
		twister = new MersenneTwister(seed);
		this.setOpenResources(new ArrayList<TrackKind>());
		this.failingObserver = new HashMap<Integer, Integer>();
		parsedColors = new ArrayList<TrackKind>();
	}

	/**
	 * Creates new resources, add them to rClosedStack and send NewPlayerEvent
	 * to all clients.
	 * 
	 * @param color
	 *            color of the resource that should be created
	 * @param count
	 *            num of new resource cards in given color
	 * @param lineNumber
	 * 
	 * @return boolean
	 * @throws Exception
	 */

	private boolean createResources(String color, int count, int lineNumber) throws Exception {
		TrackKind curr = null;
		switch (color) {

		case "GREEN":
			curr = TrackKind.GREEN;
			break;
		case "BLUE":
			curr = TrackKind.BLUE;
			break;
		case "RED":
			curr = TrackKind.RED;
			break;
		case "BLACK":
			curr = TrackKind.BLACK;
			break;
		case "YELLOW":
			curr = TrackKind.YELLOW;
			break;
		case "WHITE":
			curr = TrackKind.WHITE;
			break;
		case "ORANGE":
			curr = TrackKind.ORANGE;
			break;
		case "VIOLET":
			curr = TrackKind.VIOLET;
			break;
		case "ALL":
			curr = TrackKind.ALL;
			break;
		default:
			killEveryone("Error in file, line " + lineNumber + ": " + color + " is not a valid color.");
			return false;
		}

		if (parsedColors.contains(curr)) {
			killEveryone("Error while Parsing Resources");
			return false;
		}
		parsedColors.add(curr);

		// send to all Clients
		Iterator<ClientConnector> iterC = server.allClients().iterator();
		while (iterC.hasNext()) {
			iterC.next().newCards(curr, count);
		}

		while (count > 0) {
			this.rClosedStack.add(curr);
			count--;
		}

		return true;
	}

	/**
	 * Sends die event to all clients an close server.
	 * 
	 * @param message
	 *            The failure message for the die event
	 * 
	 * @throws Exception
	 */
	private void killEveryone(String message) throws Exception {
		while (getPlayerList().size() != 0) {
			server.toClient(getPlayerList().get(0).getId()).die(message);
			CommandP lc = new LeaveCommand(getPlayerList(), getPlayerList().get(0).getId(), getPlayerMap(), server);
			lc.execute();
		}
		while (failingObserver.size() != 0) {
			for (Iterator<Integer> it = failingObserver.keySet().iterator(); it.hasNext();) {
				server.toClient(it.next()).die(message);
			}
		}
		server.close();
	}


	/**
	 * Draws new open card from rClosedStack.
	 * 
	 * @throws IOException
	 */

	private void drawOpenCard() throws IOException {
		// Draw new open card
		if (rClosedStack.size() > 0) {
			int tw = twister.nextInt(rClosedStack.size() - 1);
			TrackKind card = rClosedStack.remove(tw);
			this.getOpenResources().add(card);

			// Send NewOpenCardEvent to all clients.
			for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
				itr.next().newOpenCard(card);
			}
		}
	}

	/**
	 * Parses the file and creates (if possible) a board, cities, tracks,
	 * missions, resources on information given by the file.
	 * 
	 * output if error should be starting with "Error in mapfile:"
	 * 
	 * @param fileName
	 * @throws Exception
	 * @return boolean
	 */
	public boolean parseFile(String fileName) throws Exception {
		cityId=0;
		trackId=0;
		missionId=0;
		int count = 0;
		BufferedReader br = null;
		int lineNumber = 1;

		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
			// br = new BufferedReader(new FileReader(fileName));

		} catch (FileNotFoundException e) {
			System.out.println("Error : File " + fileName + " not found.");
			return false;
		}

		try {
			String line = br.readLine();
			while (line != null) {

				if (line.equals("Limits:")) {

					line = br.readLine();
					lineNumber++;
					// resource loop
					while (line != null) {

						String[] splitLine = line.split(",");

						if (splitLine.length == 2) {

							try {
								count = Integer.parseInt(splitLine[1]);
							} catch (NumberFormatException e) {
								killEveryone("Error in file, line " + lineNumber + ": " + splitLine[1] + ""
										+ " is not a valid integer.");
								br.close();
								return false;
							}

							if (count < 0) {
								killEveryone(
										"Error in file, line " + lineNumber + ": " + splitLine[1] + " is negativ.");
								br.close();
								return false;
							}

							if (splitLine[0].equals("MaxTracks")) {
								if (parsedColors.size() != 9) {
									killEveryone("Error while parsing resources");
									br.close();
									return false;
								}

								if (rClosedStack.size() == 0) {
									killEveryone("Error in file, the resources should not be empty");
									br.close();
									return false;
								}
								lineNumber++;
								setMaxTracks(count);
								line = br.readLine();
								// send Max Tracks to all the Players
								Iterator<ClientConnector> iterC = server.allClients().iterator();
								while (iterC.hasNext()) {
									iterC.next().maxTrackLength(this.getMaxTracks());

								}
								for (int i = 0; i < getPlayerList().size(); i++) {
									getPlayerList().get(i).setTrainsLeft(this.getMaxTracks());
								}

								// staedte loop
								while (line != null && !line.equals("Staedte:")) {

									if (line.length() == 0) {
										killEveryone("an Empty line is an invalid entry");
										br.close();
										return false;
									}
									if (line.charAt(0) == '#') {
										line = br.readLine();
										lineNumber++;
										continue;
									} else {
										killEveryone(
												"Error in file, line " + lineNumber + ": Missing keyword 'Limits:'.");
										br.close();
										return false;
									}
								}
								lineNumber++;
								line = br.readLine();

								while (line != null && !(line.equals("Strecken:"))) {

									if (!createCities(line, lineNumber)) {
										br.close();
										killEveryone("error while parsing Cities");
										return false;
									}
									line = br.readLine();
									lineNumber++;

								}

								if (getBoard().getListCities().size() < 2) {
									killEveryone("Map must contain at least 2 cities");
									br.close();
									return false;
								}

								line = br.readLine();
								lineNumber++;

								// track loop
								while (line != null && !(line.equals("Auftraege:"))) {

									if (!createTracks(line, lineNumber)) {
										br.close();
										return false;
									}
									line = br.readLine();
									lineNumber++;

								}

								line = br.readLine();
								lineNumber++;

								while (line != null) {

									if (!(createMission(line, lineNumber))) {
										br.close();
										return false;
									}
									line = br.readLine();
									lineNumber++;

								}

								if (mClosedStack.size() < 3 * requiredPlayers) {
									killEveryone("Error in file: Not enough missions for " + requiredPlayers
											+ " players. You need at least " + (3 * requiredPlayers));
									br.close();
									return false;
									// could be changed later to at least 4
									// * reqPlayers + 5 open resources
								} else if (rClosedStack.size() - 5 < 4 * requiredPlayers) {
									killEveryone("Error in file: Not enough resources for" + requiredPlayers
											+ " players. You need at least " + (4 * requiredPlayers));
									br.close();
									return false;
								}

							} else {
								if (!createResources(splitLine[0], count, lineNumber)) {
									br.close();
									return false;
								}
								lineNumber++;
								line = br.readLine();
							}

						} else if (line.length() == 0) {
							killEveryone("an Empty line is an invalid entry");
							br.close();
							return false;
						} else if (line.charAt(0) == '#') {
							line = br.readLine();
							lineNumber++;
							continue;
						} else {
							killEveryone(
									"Error in file, " + lineNumber + ": " + line + " is" + " not a valid resource.");
							br.close();
							return false;
						}

					}

					double tmp = (double) rClosedStack.size() / (double) requiredPlayers;
					maxHandCards = (int) Math.ceil(tmp);
					Collections.sort(rClosedStack);
					br.close();
					return true;

				} else if (line.length() == 0) {
					killEveryone("an Empty line is an invalid entry");
					br.close();
					return false;
				} else if (line.charAt(0) == '#') {
					line = br.readLine();
					lineNumber++;
					continue;
				} else {
					killEveryone("Error in file, line " + lineNumber + ": Missing keyword 'Limits:'.");
					br.close();
					return false;
				}

			}
		} catch (IOException e) {
			killEveryone("Error IOException whie reading file.");
			return false;
		}
		br.close();
		return false;
	}

	private boolean createMission(String line, int lineNumber) throws Exception {

		int id1 = -1;
		int id2 = -1;
		int points = -1;

		String[] splitLine = line.split(",");

		if (splitLine.length == 3) {

			try {
				id1 = Integer.parseInt(splitLine[0]);
				id2 = Integer.parseInt(splitLine[1]);
				points = Integer.parseInt(splitLine[2]);
			} catch (NumberFormatException e) {
				killEveryone("Error in file, line " + lineNumber + ": Invalid id or points.");
				return false;
			}

			if (id1 == id2) {
				killEveryone("Illeagal Map, Invalid id for cities while parsing Missions ");
			}

			if (id1 > cityId || id2 > cityId) {
				killEveryone("Error in file, line " + lineNumber + ": Invalid id for cities");
			} else {
				City city1 = mapCities.get(id1);
				City city2 = mapCities.get(id2);
				MissionCard newMission = new MissionCard(city1, city2, points, missionId);
				if (mClosedStack.contains(newMission)) {
					killEveryone("Error in file, line " + lineNumber + ": mission already exists.");
					return false;
				}

				getmClosedStack().add(newMission);
				missionId++;
				return true;
			}

		} else if (line.length() == 0) {
			killEveryone("an Empty line is an invalid entry");
			return false;
		} else if (line.charAt(0) == '#') {
			return true;
		} else {
			killEveryone("Error in file, line " + lineNumber + ": invalid entry for mission.");
			return false;
		}
		return false;
	}

	/**
	 * Check if given information is valid and create a new track.
	 * 
	 * @param line
	 *            String that contains information for the new track
	 * @param lineNumber
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean createTracks(String line, int lineNumber) throws Exception {

		String[] splitLine = line.split(",");
		boolean isTunnel = false;
		int id1 = -1;
		int id2 = -1;
		int length = -1;

		if (splitLine.length == 5) {

			try {
				id1 = Integer.parseInt(splitLine[0]);
				id2 = Integer.parseInt(splitLine[1]);
				length = Integer.parseInt(splitLine[2]);

				if (id1 == id2) {
					killEveryone("Illeagal Map, Track must connect two different cities ");
					return false;
				}

				if (id1 >= getBoard().getListCities().size() || id2 >= getBoard().getListCities().size()) {
					killEveryone("Error in file, line " + lineNumber + ": ids do not match a city.");
					return false;
				} else if (length < 1) {
					killEveryone("Error in file, line " + lineNumber + ": track length cannot be" + " less than 1.");
					return false;
				} else if (length > 6) {
					killEveryone("Error in file, line " + lineNumber + ": track length cannot be" + " greater than 6.");
					return false;
				}

			} catch (NumberFormatException e) {
				killEveryone("Error in file, line " + lineNumber + ": Invalid id.");
				return false;
			}

			if (splitLine[4].equals("N")) {
				isTunnel = false;
			} else if (splitLine[4].equals("T")) {
				isTunnel = true;
			} else {
				killEveryone("Error in file, line " + lineNumber + ": You need a 'N' or 'T'");
				return false;
			}

			City city1 = mapCities.get(id1);
			City city2 = mapCities.get(id2);
			city1.getMap().put(trackId, city2);
			city2.getMap().put(trackId, city1);

			HashMap<Integer, Integer> mapPoints = new HashMap<Integer, Integer>();
			mapPoints.put(1, 1);
			mapPoints.put(2, 2);
			mapPoints.put(3, 4);
			mapPoints.put(4, 7);
			mapPoints.put(5, 10);
			mapPoints.put(6, 15);

			int p = mapPoints.get(length);
			Track newTrack = null;
			switch (splitLine[3]) {

			case "GREEN":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.GREEN);
				trackId++;
				break;
			case "BLUE":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.BLUE);
				trackId++;
				break;
			case "RED":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.RED);
				trackId++;
				break;
			case "BLACK":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.BLACK);
				trackId++;
				break;
			case "YELLOW":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.YELLOW);
				trackId++;
				break;
			case "WHITE":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.WHITE);
				trackId++;
				break;
			case "ORANGE":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.ORANGE);
				trackId++;
				break;
			case "VIOLET":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.VIOLET);
				trackId++;
				break;
			case "ALL":
				newTrack = new Track(trackId, city1, city2, length, p, isTunnel, TrackKind.ALL);
				trackId++;
				break;

			default:
				killEveryone("Error in file, line " + lineNumber + ": Invalid TrackKind " + splitLine[3] + ".");
				return false;

			}
			if (!rClosedStack.contains(newTrack.getColor()) && newTrack.getColor() != TrackKind.ALL) {
				killEveryone("Illegal color");
				return false;
			}

			// this should always be true!

			getBoard().getListTracks().add(newTrack);
			// send out Tracks to all Clients
			Iterator<ClientConnector> iterC = server.allClients().iterator();
			while (iterC.hasNext()) {

				iterC.next().newTrack((short) newTrack.getId(), (short) newTrack.getCity1().getId(),
						(short) newTrack.getCity2().getId(), newTrack.getLength(), newTrack.getColor(),
						newTrack.isTunnel());
			}
			return true;

		} else if (line.length() == 0) {
			killEveryone("an Empty line is an invalid entry");
			return false;
		} else if (line.charAt(0) == '#') {

			return true;

		} else {

			killEveryone("Error in file, line " + lineNumber + ": Invalid entry for a track.");
			return false;
		}
	}

	/**
	 * Check if given information is valid and create a new city.
	 * 
	 * @param line
	 *            String that contains information of the new city
	 * @param lineNumber
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean createCities(String line, int lineNumber) throws Exception {

		String[] lineCity = line.split(",");

		if (lineCity.length == 3) {

			try {
				int x = Integer.parseInt(lineCity[1]);
				int y = Integer.parseInt(lineCity[2]);
				City newCity = new City(lineCity[0], cityId, x, y);
				getBoard().getListCities().add(newCity);
				mapCities.put(cityId, newCity);
				cityId++;
				// Send Cities to all Clients
				Iterator<ClientConnector> iterC = server.allClients().iterator();
				while (iterC.hasNext()) {
					iterC.next().newCity((short) newCity.getId(), newCity.getName(), newCity.getX(), newCity.getY());

				}

				return true;
			} catch (NumberFormatException e) {
				killEveryone("Error in file, line " + lineNumber + ": invalid coordinates.");
				return false;
			}

		} else if (line.length() == 0) {
			killEveryone("an Empty line is an invalid entry");
			return false;
		}

		else if (line.charAt(0) == '#') {

			return true;
		} else {
			killEveryone("Error in file, line " + lineNumber + ": " + line + "" + " is not a valid entry for a city.");
			return false;
		}

	}

	/**
	 * Waits until requiredPlayers are registered for the game.
	 * 
	 * @throws Exception
	 * 
	 */
	public void waitPlayers() throws Exception {
		server.start();
		while (getPlayerList().size() < requiredPlayers) {
			Command com = server.pollNextCommand();

			if (com instanceof Register) {

				Register reg = (Register) com;
				RegisterCommand rc = new RegisterCommand(reg.getPlayerId(), reg.getMyName(), server, getPlayerList(),
						getPlayerMap(), getMaxTracks());
				rc.execute();
				failingObserver.remove(reg.getPlayerId());
			} else if (com instanceof NewObserver) {
				NewObserver newObs = (NewObserver) com;
				failingObserver.put(newObs.getPlayerId(), 0);
				server.toClient(newObs.getPlayerId()).gameState(requiredPlayers);
				Iterator<Contestant> iterC = getPlayerList().iterator();
				while (iterC.hasNext()) {
					Contestant cont = iterC.next();
					server.toClient(newObs.getPlayerId()).newPlayer(cont.getId(), cont.getName());
				}
			} else if (com instanceof Leave) {
				LeaveCommand lc = new LeaveCommand(getPlayerList(), com.getPlayerId(), getPlayerMap(), server);
				lc.execute();
			} else {
				if (getPlayerMap().get(com.getPlayerId()) != null) {
					if (getPlayerMap().get(com.getPlayerId()).getNumFails() < 3) {
						server.toClient(com.getPlayerId()).failure("Please wait until the game starts");
					}
					checkFails(com.getPlayerId());
				} else {
					failObserver(com.getPlayerId());
				}
				continue;

			}
		}

	}

	/**
	 * Hands out all cards for the start of the game (4 resource cards and 3
	 * mission cards for the players as well as 5 open resource cards.
	 * 
	 * @throws Exception
	 * 
	 */
	public void distributeCards() throws Exception {

		int countall;
		int numRedo = -1;
		List<TrackKind> tmpCards = new ArrayList<TrackKind>();
		// rOpenStack alias the 5 open cards or less
		do {
			countall = 0;
			numRedo++;
			if (numRedo > 1)
				break;
			if (numRedo == 1) {
				rClosedStack.addAll(tmpCards);
				Collections.sort(rClosedStack);
			}
			for (int i = 5; i > 0; i--) {

				int randInt = twister.nextInt(rClosedStack.size() - 1);
				TrackKind openCard = rClosedStack.remove(randInt);
				if (openCard == TrackKind.ALL) {
					countall++;
				}
				tmpCards.add(openCard);
				Iterator<ClientConnector> playerIter = server.allClients().iterator();
				this.getOpenResources().add(openCard);

				while (playerIter.hasNext()) {
					playerIter.next().newOpenCard(openCard);
				}

			}
		} while (countall > 2);

		Iterator<Contestant> iter = getPlayerList().iterator();

		while (iter.hasNext()) {

			Contestant player = iter.next();

			TakeSecretCardCommand tcc = new TakeSecretCardCommand(player, server, twister, rClosedStack, maxHandCards,
					4);
			tcc.execute();

			GetMissionCommand gmc = new GetMissionCommand(mClosedStack, player, twister, server);
			gmc.execute();

		}

		ArrayList<Integer> hasMissionsReturned = new ArrayList<Integer>();

		while (hasMissionsReturned.size() != getPlayerList().size()) {
			Command com = server.pollNextCommand();

			switch (com.getId()) {
			case 28: // return Missions
				if (getPlayerMap().get(com.getPlayerId()) != null) {
					ReturnMissions rm = (ReturnMissions) com;
					Contestant cont = getPlayerMap().get(rm.getPlayerId());

					ReturnMissionCommand rmc = new ReturnMissionCommand(cont, rm.getMissions(), mClosedStack, server);

					if (!hasMissionsReturned.contains(rm.getPlayerId())) {
						if (rmc.execute()) {
							hasMissionsReturned.add(rm.getPlayerId());
						} else
							checkFails(rm.getPlayerId());

					} else if (getPlayerMap().get(rm.getPlayerId()).getNumFails() < 3) {
						server.toClient(rm.getPlayerId()).failure("You already returned Missions");
					} else {
						checkFails(rm.getPlayerId());
					}
				} else {
					failObserver(com.getPlayerId());
				}
				break;

			case 33: // New Observer
				NewObserver newObs = (NewObserver) com;
				failingObserver.put(newObs.getPlayerId(), 0);
				server.toClient(newObs.getPlayerId()).gameState(requiredPlayers);
				for (int i = 0; i < getPlayerList().size(); i++) {
					server.toClient(com.getPlayerId()).newPlayer(getPlayerList().get(i).getId(),
							getPlayerList().get(i).getName());
				}

				break;

			case 26: // Leave
				if (getPlayerMap().get(com.getPlayerId()) != null) {
					Leave leave = (Leave) com;
					LeaveCommand lc = new LeaveCommand(getPlayerList(), leave.getPlayerId(), getPlayerMap(), server);
					lc.execute();
					if (hasMissionsReturned.contains(leave.getPlayerId())) {
						hasMissionsReturned.remove(leave.getPlayerId());
					}
				} else {
					failObserver(com.getPlayerId());
				}
				break;

			default:
				if (getPlayerMap().get(com.getPlayerId()) != null) {
					if (getPlayerMap().get(com.getPlayerId()).getNumFails() < 3) {
						server.toClient(com.getPlayerId()).failure("You should return Missions now!");
					}
					checkFails(com.getPlayerId());
				} else {
					failObserver(com.getPlayerId());
				}
				break;
			}

		}

	}

	/**
	 * Check if player with given Id is allowed to make turn.
	 * 
	 * @param playerId
	 * @return true if the player is allowed to make a turn, false otherwise.
	 * @throws IOException
	 */
	private boolean playerTurn(int playerId) throws IOException {

		Contestant p0 = getPlayerList().get(0);
		Contestant p1 = getPlayerMap().get(playerId);

		if (p1 == null) {
			return false;
		} else if (p0.getId() == p1.getId()) {
			return true;
		} else
			try {
				if (getPlayerMap().get(playerId).getNumFails() < 3) {
					server.toClient(playerId).failure("It's not your turn right now");
				}
			} catch (Exception e) {

			}
		checkFails(playerId);
		return false;

	}

	/**
	 * 
	 * Increases numFails of player with given Id. If numFails == 3, send die
	 * event and kick player out.
	 * 
	 * @param playerId
	 * @throws IOException
	 */
	private void checkFails(int playerId) throws IOException {

		Contestant cont = getPlayerMap().get(playerId);

		if (!(cont == null)) {
			if (cont.getNumFails() == 3) {
				try {
					server.toClient(playerId).die("Too many fails motherfucker");
				} catch (Exception e) {
				}
				for (Iterator<ClientConnector> it = server.allClients().iterator(); it.hasNext();) {
					it.next().playerLeft(playerId);
				}
				// Set nextReturnMission and nextPayTunnel false
				if (playerId == getPlayerList().get(0).getId()) {
					this.nextPayTunnel = false;
					this.nextReturnMission = false;
				}
				getPlayerMap().remove(playerId);
				getPlayerList().remove(cont);
				this.setTurnsLeft(2);
			} else {
				cont.setNumFails(cont.getNumFails() + 1);
			}
		}
	}

	/**
	 * Check if player with given Id has turns left.
	 * 
	 * @param turnValue
	 * @param playerId
	 * @param s
	 *            String for failure message
	 * 
	 * @return true if the player has turns left
	 * @throws IOException
	 */
	private boolean playerHasTurnsLeft(int turnValue, int id, String s) throws IOException {
		if (turnsLeft < turnValue) {
			if (getPlayerMap().get(id).getNumFails() < 3) {
				server.toClient(id).failure("You are not allowed to " + s + " at this point of the game");
			}
			checkFails(id);
			return false;
		}
		return true;
	}

	/**
	 * Increases numFails of observer with given Id. If numFails == 3, remove
	 * observer.
	 * 
	 * @param Id
	 * @throws IOException
	 */
	private void failObserver(int id) throws Exception {
		if (failingObserver.containsKey(id)) {
			int numFails = failingObserver.get(id);
			if (numFails == 3) {
				server.toClient(id).die("You tried to play too often");
				failingObserver.remove(id);
			} else {
				server.toClient(id).failure("You can't play, you are only an observer");
				numFails++;
				failingObserver.put(id, numFails);
			}
		} else {
			failingObserver.put(id, 1);
			server.toClient(id).failure("You can't play, you are only an observer");
		}

	}

	/**
	 * Handles commands form the komlib and creates fitting command from the
	 * server side's commands and executes them.
	 * 
	 * @throws Exception
	 */
	public void decideCommand(Command command) throws Exception {

		int id = command.getPlayerId();
		Contestant player = getPlayerMap().get(id);

		// check if new observer
		if (command.getId() == 33) { // newobs
			NewObserver newObs = (NewObserver) command;
			failingObserver.put(newObs.getPlayerId(), 0);
			server.toClient(newObs.getPlayerId()).gameState(requiredPlayers);
			for (int i = 0; i < getPlayerList().size(); i++) {
				server.toClient(command.getPlayerId()).newPlayer(getPlayerList().get(i).getId(),
						getPlayerList().get(i).getName());
			}
			return;
		}

		// Check if command is leave command
		if (command.getId() == 26) { // leave
			Leave leave = (Leave) command;
			// If first player in playerlist leaves set nextReturnMission and
			// nextPayTunnel false and turnsleft = 2
			if (id == getPlayerList().get(0).getId()) {
				this.nextPayTunnel = false;
				this.nextReturnMission = false;
				this.setTurnsLeft(2);
			}
			LeaveCommand lc = new LeaveCommand(getPlayerList(), leave.getPlayerId(), getPlayerMap(), server);
			lc.execute();
			return;
		}

		if (command.getId() == 34) { // timeout
			// Timeout to = (Timeout) command;
			server.toClient(getPlayerList().get(0).getId()).die("Timeout"); // get(0)
																			// ==
																			// player
																			// turn
			LeaveCommand lc = new LeaveCommand(getPlayerList(), getPlayerList().get(0).getId(), getPlayerMap(), server);
			lc.execute();

			// Set nextReturnMission and nextPayTunnel false
			this.nextPayTunnel = false;
			this.nextReturnMission = false;
			this.setTurnsLeft(2);
			return;
		}

		if (player == null) { // so he's an Observer
			failObserver(id);
			return;
		}

		// If nextPayTunnel true, Command has to be PayTunnel
		if (nextPayTunnel && command.getId() != 30) {
			if (player.getNumFails() < 3) {
				try {
					server.toClient(id).failure("Next Command has to be PayTunnel");
				} catch (Exception e) {
				}
			}
			checkFails(id);
			return;
		}

		// If nextReturnMission is true, command has to be ReturnMission
		if (nextReturnMission && command.getId() != 28) {
			if (player.getNumFails() < 3) {
				try {
					server.toClient(id).failure("Next Command has to be ReturnMission");
				} catch (Exception e) {
				}
			}
			checkFails(id);
			return;
		}

		switch (command.getId()) {

		case 25: // register
			Register reg = (Register) command;
			if (getPlayerMap().get(id).getNumFails() < 3) {
				try {
					server.toClient(reg.getPlayerId()).failure("You are not allowed to register anymore");
				} catch (Exception e) {
				}
			}
			checkFails(reg.getPlayerId());
			break;
		case 27: // getmissions
			if (playerTurn(id) && playerHasTurnsLeft(2, id, "get missions")) {
				GetMissionCommand gmc = new GetMissionCommand(mClosedStack, player, twister, server);
				if (gmc.execute()) {
					nextReturnMission = true;
					turnsLeft = 0;
					player.setNumFails(0);
					break;
				} else {
					checkFails(id);
				}
			}
			break;
		case 28: // return missions
			if (playerTurn(id)) {
				if (nextReturnMission) {
					ReturnMissions rm = (ReturnMissions) command;
					Mission[] rMissions = rm.getMissions();
					ReturnMissionCommand rmc = new ReturnMissionCommand(player, rMissions, mClosedStack, server);
					if (rmc.execute()) {
						nextReturnMission = false;
						player = getPlayerList().remove(0);
						getPlayerList().add(player);
						turnsLeft = 2;
						player.setNumFails(0);
						break;
					} else {
						checkFails(id);
					}
				} else {
					if (getPlayerMap().get(id).getNumFails() < 3) {
						try {
							server.toClient(id).failure("You can only return missions after taking new ones");
						} catch (Exception e) {

						}
					}
					checkFails(id);
				}
			}
			break;
		case 29: // buildTrack
			if (playerTurn(id) && playerHasTurnsLeft(2, id, "build track")) {
				BuildTrack bt = (BuildTrack) command;
				BuildTrackCommand btc = new BuildTrackCommand(getBoard(), player, bt.getLocomotives(), bt.getTrackId(),
						server, bt.getKind(), twister, rClosedStack);
				if (btc.execute()) {
					player.setNumFails(0);
					// Check if player has to build tunnel next
					if (player.getAddCosts() != -1) {
						nextPayTunnel = true;
						turnsLeft--;
					} else {
						player = getPlayerList().remove(0);
						getPlayerList().add(player);
						turnsLeft = 2;
					}
					// If less than 5 cards open, draw new card
					while (!rClosedStack.isEmpty() && this.getOpenResources().size() < 5) {
						drawOpenCard();
					}
					break;
				} else {
					checkFails(id);
				}
			}
			break;
		case 30: // paytunnel
			if (playerTurn(id)) {
				if (nextPayTunnel) {
					PayTunnel pt = (PayTunnel) command;
					PayTunnelCommand ptc = new PayTunnelCommand(getBoard(), player, pt.getLocomotives(), server,
							player.getTunnelKind(), rClosedStack, player.getTunnelID());
					if (ptc.execute()) {
						nextPayTunnel = false;
						player = getPlayerList().remove(0);
						getPlayerList().add(player);
						turnsLeft = 2;
						player.setNumFails(0);
						// If less than 5 cards open draw new
						while (!rClosedStack.isEmpty() && this.getOpenResources().size() < 5) {
							drawOpenCard();
						}
						break;
					} else {
						checkFails(id);
					}
				} else {
					if (getPlayerMap().get(id).getNumFails() < 3) {
						try {
							server.toClient(id).failure("You did not try to build a tunnel in your last command");
						} catch (Exception e) {
						}
					}
					checkFails(id);
				}
			}
			break;
		case 31: // takecard
			TakeCard tc = (TakeCard) command;
			if (playerTurn(id)) {
				// Check if open stack is empty
				if (getOpenResources().size() == 0) {
					if (getPlayerMap().get(id).getNumFails() < 3) {
						server.toClient(id).failure("The open stack is empty");
					}
					checkFails(id);
					return;
				}
				// Check if player reaches maxHandCards
				if (player.getHandCards().size() >= this.maxHandCards - 1 && turnsLeft == 2) {
					if (player.getNumFails() < 3) {
						try {
							server.toClient(player.getId()).failure("You can't take more cards, you'd reach max hand cards");
						} catch (Exception e) {
						}
					}
					checkFails(player.getId());
					return;
				}

				// Check if player is allowed to take last card
				if (getOpenResources().size() == 1) {
					if (turnsLeft == 2) {
						try {
							if (getPlayerMap().get(id).getNumFails() < 3) {
								server.toClient(id).failure("You are not allowed to take the last open card");
							}
						} catch (Exception e) {
						}
						checkFails(id);
						break;
					} else {
						TakeCardCommand tkc = new TakeCardCommand(player, server, twister, rClosedStack,
								getOpenResources(), tc.getKind(), maxHandCards);
						if (tkc.execute()) {
							player = getPlayerList().remove(0);
							getPlayerList().add(player);
							turnsLeft = 2;
							player.setNumFails(0);
							break;
						} else {
							checkFails(id);
							break;
						}
					}
				}

				// Check if player is allowed to take loc
				if (tc.getKind() == TrackKind.ALL && turnsLeft < 2) {
					try {
						if (getPlayerMap().get(id).getNumFails() < 3) {
							server.toClient(id).failure("You can't take a locomotive as your second card");
						}
					} catch (Exception e) {
					}
					checkFails(id);
					break;
				}

				// Draw Card from open stack
				TakeCardCommand tkc = new TakeCardCommand(player, server, twister, rClosedStack, getOpenResources(),
						tc.getKind(), maxHandCards);
				if (tkc.execute()) {
					player.setNumFails(0);
					if (tc.getKind() == TrackKind.ALL) {
						player = getPlayerList().remove(0);
						getPlayerList().add(player);
						turnsLeft = 2;
						break;
					} else {
						turnsLeft--;
						if (turnsLeft == 0) {
							turnsLeft = 2;
							player = getPlayerList().remove(0);
							getPlayerList().add(player);
						}
						break;
					}
				} else {
					checkFails(id);
				}
			}
			break;
		case 32: // takesecretcard
			if (playerTurn(id)) {

				// Check if player reaches maxHandCards
				if (player.getHandCards().size() >= this.maxHandCards - 1 && turnsLeft == 2) {
					try {
						if (player.getNumFails() < 3) {
							server.toClient(id).failure("Almost reached max HandCards");
						}
					} catch (Exception e) {
					}
					checkFails(id);
					break;
				}

				TakeSecretCardCommand tscc = new TakeSecretCardCommand(player, server, twister, rClosedStack,
						maxHandCards, 1);
				if (tscc.execute()) {
					player.setNumFails(0);
					turnsLeft--;
					if (turnsLeft == 0) {
						player = getPlayerList().remove(0);
						getPlayerList().add(player);
						turnsLeft = 2;
					}
					break;
				} else {
					checkFails(id);
				}
			}
			break;
		default:
			// Should never reach this case
			break;
		}

	}

	/**
	 * The run loop in which all events and commands are handled and send.
	 * Decides when the game is over and also controls the player turns.
	 * 
	 * @throws Exception
	 */
	public void run() throws Exception {

		// game start
		Iterator<ClientConnector> ccIter = server.allClients().iterator();
		while (ccIter.hasNext()) {
			ClientConnector cc = ccIter.next();
			cc.gameStart();
		}

		while (maxTracksNotReached()) {
			if (this.getPlayerList().size() == 0) {
				waitForPlayersToLeave();
				return;
			}
			Command command = server.pollNextCommand();
			decideCommand(command);
		}

		// winner and winner loop
		this.endGame = new EndGame(getBoard(), getPlayerList(), server);
		List<Integer> listWinner = endGame.checkWinner();

		ccIter = server.allClients().iterator();

		while (ccIter.hasNext()) {
			ClientConnector cc = ccIter.next();
			for (int k = 0; k < listWinner.size(); k++) {
				int WinnerId = listWinner.get(k);
				cc.winner(WinnerId);
			}
		}
		waitForPlayersToLeave();

	}

	/**
	 * Wait for players and observers to leave after the game ends or all
	 * players left the game.
	 * 
	 * @throws Exception
	 */
	private void waitForPlayersToLeave() throws Exception {
		while (getPlayerList().size() > 0 || failingObserver.size() > 0) {
			Command command = server.pollNextCommand();
			if (command instanceof Leave) {

				Leave leave = (Leave) command;
				LeaveCommand lc = new LeaveCommand(getPlayerList(), leave.getPlayerId(), getPlayerMap(), server);
				if (!lc.execute()) {
					failingObserver.remove(leave.getPlayerId());
				}
			} else if (command instanceof NewObserver) {
				try {
					failingObserver.put(command.getPlayerId(), 0);
					server.toClient(command.getPlayerId()).gameState(requiredPlayers);
					for (int i = 0; i < getPlayerList().size(); i++) {
						server.toClient(command.getPlayerId()).newPlayer(getPlayerList().get(i).getId(),
								getPlayerList().get(i).getName());
					}
				} catch (Exception e) {

				}
			} else {
				Contestant player = getPlayerMap().get(command.getPlayerId());
				if (player == null) {
					failObserver(command.getPlayerId());
				} else {
					if (player.getNumFails() < 3) {
						server.toClient(player.getId()).failure("The Game is over, please disconnect");
					}
					checkFails(player.getId());

				}
			}
		}
		server.close();
	}

	/**
	 * Checks if maxTracks is reached.
	 * 
	 * @return true if maxTrack is not reached, false otherwise
	 */
	private boolean maxTracksNotReached() {

		boolean result = true;
		Iterator<Contestant> iterC = getPlayerList().iterator();

		while (iterC.hasNext()) {
			Contestant cont = iterC.next();

			if (cont.getTrainsLeft() <= 3) { // maybe < 3
				result = false;
			}
		}
		return result;
	}

	public List<MissionCard> getmClosedStack() {
		return mClosedStack;
	}

	public void setmClosedStack(List<MissionCard> mClosedStack) {
		this.mClosedStack = mClosedStack;
	}

	public int getRequiredPlayers() {
		return requiredPlayers;
	}

	public void setRequiredPlayers(int requiredPlayers) {
		this.requiredPlayers = requiredPlayers;
	}

	public int getMaxHandCards() {
		return maxHandCards;
	}

	public void setMaxHandCards(int maxHandCards) {
		this.maxHandCards = maxHandCards;
	}

	public int getTurnsLeft() {
		return turnsLeft;
	}

	public void setTurnsLeft(int turnsLeft) {
		this.turnsLeft = turnsLeft;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public EndGame getEndGame() {
		return endGame;
	}

	public void setEndGame(EndGame endGame) {
		this.endGame = endGame;
	}

	public List<TrackKind> getrClosedStack() {
		return rClosedStack;
	}

	public void setrClosedStack(List<TrackKind> rClosedStack) {
		this.rClosedStack = rClosedStack;
	}

}
