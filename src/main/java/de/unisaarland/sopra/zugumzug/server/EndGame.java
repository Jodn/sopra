package de.unisaarland.sopra.zugumzug.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.ClientConnector;
import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.Server;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;

public class EndGame {

	private Server server;
	private Board board;
	private List<Contestant> listContestant;
	private HashMap<Integer, Data> infoMap;
	private int LongestTrack;

	public EndGame(Board board, List<Contestant> listContestant, Server server) {
		this.setBoard(board);
		this.setListContestant(listContestant);
		this.server = server;
		this.infoMap = new HashMap<Integer, Data>();
		LongestTrack = 0;
	}

	/**
	 * The checkMissionPoints method that calculates the points a player gets or
	 * loses for his missions and adds this value to his points. it also
	 * calculates the number of successfully finished missions and write them in
	 * the Contestant. NOTE that calculateMaxTrak() needs to be called before
	 * this function is called
	 */
	private void checkMissionPoints() {
		// check Mission for every contestant

		for (int i = 0; i < listContestant.size(); i++) {
			int numSuccMissions = 0;
			Contestant currContestant = listContestant.get(i);
			List<MissionCard> contMissions = currContestant.getListMissions();

			for (int j = 0; j < contMissions.size(); j++) {
				MissionCard currMission = contMissions.get(j);
				City city1 = currMission.getCity1();
				Data data = infoMap.get(currContestant.getId());

				// is city1 connected with city2?
				if (data.connectedCities.containsKey(city1)) {
					if (data.connectedCities.get(city1).contains(currMission.getCity2())) {
						data.succeeded.add(currMission);
						numSuccMissions++;
						currContestant.setPoints(currContestant.getPoints() + currMission.getPoints());
					} else {
						data.failed.add(currMission);
						currContestant.setPoints(currContestant.getPoints() - currMission.getPoints());
					}
				} else {
					data.failed.add(currMission);
					currContestant.setPoints(currContestant.getPoints() - currMission.getPoints());
				}

			}

			currContestant.setNumFinishedMissions(numSuccMissions);
		}

	}

	public int findLongestTrack(City city, HashSet<City> connectedCities, List<Integer> forbiddenTrack, int contId,
			int res) {
		HashMap<Integer, City> map = city.getMap();

		for (Iterator<Integer> i = map.keySet().iterator(); i.hasNext();) {
			int cur = i.next();
			if (board.getListTracks().get(cur).getOwnerId() == contId && !forbiddenTrack.contains(cur)) {
				City tmp = map.get(cur);
				forbiddenTrack.add(cur);
				ArrayList<Integer> tmpforbidden = new ArrayList<Integer>();
				for (Iterator<Integer> itr = forbiddenTrack.iterator(); itr.hasNext();) {
					tmpforbidden.add(itr.next());
				}
				connectedCities.add(tmp);
				int calc = findLongestTrack(tmp, connectedCities, tmpforbidden, contId, 0)
						+ board.getListTracks().get(cur).getLength();
				if (res < calc)
					res = calc;
			}
		}

		return res;

	}

	/**
	 * The calculateMaxTrack method searches for the the longest track of every
	 * player and writes the them in the mapIdTrack Hashmap.
	 */

	public List<Contestant> calculateMaxTrack() {

		// needed for returning the Id of the player with the longest Track
		List<Contestant> longestTrackId = new ArrayList<Contestant>();
		int currlongestTrack = -1;

		for (int i = 0; i < listContestant.size(); i++) {
			Contestant currCont = listContestant.get(i);
			Data data = new Data(); // new Data instance for this contestant

			HashSet<City> cityMap = new HashSet<City>(); // Set of all the
															// cities the curr
															// contestant has
															// visited
			// put into Data class later. Needed to find missions more easily
			HashMap<City, HashSet<City>> connectedCities = new HashMap<City, HashSet<City>>();

			// get all cities of curr Contestant, save in a Map
			for (int j = 0; j < currCont.getListTracks().size(); j++) {
				Track currTrack = currCont.getListTracks().get(j);
				// System.out.println(currTrack.getCity1().getId()+ "" +
				// currTrack.getCity2().getId());

				cityMap.add(currTrack.getCity1());
				cityMap.add(currTrack.getCity2());
			}

			// find longestTrack, starting from every City of the Map
			int currMaxLength = 0;
			for (Iterator<City> it = cityMap.iterator(); it.hasNext();) {
				City currCity = it.next();

				HashSet<City> Cities = new HashSet<City>();
				int tmp = findLongestTrack(currCity, Cities, new ArrayList<Integer>(), currCont.getId(), 0);

				if (currMaxLength < tmp)
					currMaxLength = tmp;

				connectedCities.put(currCity, Cities);
			}

			// now the maxTrackLength for this contestant is calculated
			data.longestTrack = currMaxLength;
			data.connectedCities = connectedCities;
			infoMap.put(currCont.getId(), data);

			// see if his Track is currently the longest
			if (currMaxLength > currlongestTrack) {
				currlongestTrack = currMaxLength;
				longestTrackId.clear();
				longestTrackId.add(currCont);
			} else {
				if (currMaxLength == currlongestTrack) {
					longestTrackId.add(currCont);
				}
			}

		}

		// now give all the Contestants in longest Track List 10 points!
		for (int i = 0; i < longestTrackId.size(); i++)
			longestTrackId.get(i).setPoints(longestTrackId.get(i).getPoints() + 10);
		LongestTrack = currlongestTrack;
		return longestTrackId;
	}

	/**
	 * The checkWinner method uses the above methodes to decide the winner(s).
	 * And sends the fitting event to all players.
	 * 
	 * @return List<Integer> (all winner ids)
	 * @throws IOException
	 */
	public List<Integer> checkWinner() throws IOException {

		List<Integer> winnerList = new ArrayList<Integer>();
		List<Contestant> LongestTrackList = calculateMaxTrack();
		checkMissionPoints();
		int maxPoints = -1;
		List<Contestant> currMaxPoints = new ArrayList<Contestant>();

		sentInfos(LongestTrackList);

		for (int i = 0; i < listContestant.size(); i++) {
			Contestant currContestant = listContestant.get(i);
			if (currContestant.getPoints() > maxPoints) {
				maxPoints = currContestant.getPoints();
				currMaxPoints.clear();
				currMaxPoints.add(currContestant);

			} else if (currContestant.getPoints() == maxPoints)
				currMaxPoints.add(currContestant);

		}

		if (currMaxPoints.size() == 1) {
			winnerList.add(currMaxPoints.get(0).getId());
			return winnerList;
		}

		// if points are Equal, check who completed more missions
		int maxSuccMissions = -1;
		List<Contestant> listMaxMiss = new ArrayList<Contestant>();

		for (int i = 0; i < currMaxPoints.size(); i++) {
			if (currMaxPoints.get(i).getNumFinishedMissions() > maxSuccMissions) {
				listMaxMiss.clear();
				listMaxMiss.add(currMaxPoints.get(i));
				maxSuccMissions = currMaxPoints.get(i).getNumFinishedMissions();
			} else if (currMaxPoints.get(i).getNumFinishedMissions() == maxSuccMissions)
				listMaxMiss.add(currMaxPoints.get(i));
		}

		if (listMaxMiss.size() == 1) {
			winnerList.add(listMaxMiss.get(0).getId());
			return winnerList;
		}

		// if points are still equal, check who build the longest Track
		for (int i = 0; i < listMaxMiss.size(); i++) {
			if (LongestTrackList.contains(listMaxMiss.get(i)))
				winnerList.add(listMaxMiss.get(i).getId());
		}

		return winnerList;

	}

	private void sentInfos(List<Contestant> LongestTrackList) throws IOException {
		try {

			// send Missions
			for (int i = 0; i < listContestant.size(); i++) {
				Mission[] misSuc = convertMissionCard(infoMap.get(listContestant.get(i).getId()).succeeded);
				Mission[] misFail = convertMissionCard(infoMap.get(listContestant.get(i).getId()).failed);
				for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
					itr.next().finalMissions(listContestant.get(i).getId(), misSuc, misFail);
				}
			}
			// sendLongestTrack
			for (int j = 0; j < LongestTrackList.size(); j++) {
				for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
					itr.next().longestTrack(LongestTrackList.get(j).getId(), LongestTrack);

				}
			}

			for (int i = 0; i < listContestant.size(); i++) {
				for (Iterator<ClientConnector> itr = server.allClients().iterator(); itr.hasNext();) {
					itr.next().finalPoints(listContestant.get(i).getId(), listContestant.get(i).getPoints());
				}
			}
		} catch (IndexOutOfBoundsException e) {
		}
	}

	public Mission[] convertMissionCard(List<MissionCard> mission) {
		Mission[] res = new Mission[mission.size()];

		for (int i = 0; i < mission.size(); i++) {
			MissionCard tmp = mission.get(i);
			Mission mi = new Mission((short) tmp.getCity1().getId(), (short) tmp.getCity2().getId(), tmp.getPoints());
			res[i] = mi;
		}
		return res;

	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public List<Contestant> getListContestant() {
		return listContestant;
	}

	public void setListContestant(List<Contestant> listContestant) {
		this.listContestant = listContestant;
	}

	public int getMaxTrack(int playerId) {
		return infoMap.get(playerId).longestTrack;
	}

	// Private Class Data includes all the information calculated in the End
	// Game Class
	private static class Data {
		private int longestTrack;
		private HashMap<City, HashSet<City>> connectedCities;
		private List<MissionCard> succeeded;
		private List<MissionCard> failed;

		public Data() {
			longestTrack = -1;

			connectedCities = new HashMap<City, HashSet<City>>();
			succeeded = new ArrayList<MissionCard>();
			failed = new ArrayList<MissionCard>();

		}

	}

}
