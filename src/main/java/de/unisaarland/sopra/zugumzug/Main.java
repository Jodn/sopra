package de.unisaarland.sopra.zugumzug;

import java.io.File;

import de.unisaarland.sopra.internal.LoggingServer;
import de.unisaarland.sopra.zugumzug.client.GUI;
import de.unisaarland.sopra.zugumzug.client.Gamer;
import de.unisaarland.sopra.zugumzug.client.KI;
import de.unisaarland.sopra.zugumzug.client.PlayerLogic;
import de.unisaarland.sopra.zugumzug.server.GameLogic;

public class Main {

	private final static int DEFAULT_PORT = 4000;

	private final static int DEFAULT_NUMPLAYERS = 4;

	private final static String DEFAULT_HOST = "localhost";

	private final static int DEFAULT_SEED = (int) System.currentTimeMillis();

	private final static String DEFAULT_NAME = "LordOfTheSeed";

	private final static boolean DEFAULT_ISGUI = false;

	private final static boolean DEFAULT_ISSERVER = false;

	private final static boolean DEFAULT_ISLOGGINGSERVER = false;

	static boolean isServer = DEFAULT_ISSERVER;
	static boolean isGui = DEFAULT_ISGUI;
	static int port = DEFAULT_PORT;
	static int numPlayers = DEFAULT_NUMPLAYERS;
	static int seed = DEFAULT_SEED;
	static String host = DEFAULT_HOST;
	static String name = DEFAULT_NAME;
	static String fileName = null;
	static boolean isLoggingServer = DEFAULT_ISLOGGINGSERVER;
	static File logdir;

	/**
	 * The help method that prints out a help about the zugumzug program.
	 */
	private static void help() {
		String namezug = "\033[1mNAME\033[0m"; // bold text in linux terminal
		String opt = "\033[1mOPTIONS\033[0m";
		String ki = "\033[1m--ki\033[0m";
		String gui = "\033[1m--gui\033[0m";
		String server = "\033[1m--server\033[0m";
		String help = "\033[1m--help\033[0m";
		String synopsis = "\033[1mSYNOPSIS\033[0m";
		String description = "\033[1mDESCRIPTION\033[0m";
		System.out.println(namezug);
		System.out.println("     zugumzug -a fun game with trains\n");
		System.out.println(synopsis);
		System.out.println("     java -jar zugumzug [--server] [--gui] [--ki] [--help] [<args>]\n");
		System.out.println(description);
		System.out.println("     Zugumzug is a game which was original a board game. It is about\n"
				+ "     building tracks, tunnels, fullfill mission and denie your opponents");
		System.out.println("     goals. The program provides a GUI for a human to play the game.\n"
				+ "     A KI that plays automatically and a SERVER on which players can connect.\n");
		System.out.println(opt);
		System.out.println("      " + help);
		System.out.println("          Prints a help about the zugumzug program input parameters.\n"
				+ "          And provides some more information.\n");
		System.out.println("      " + server + " <port> -map <mapfile> [-s <seed>] -p <num players>");
		System.out.println("         Starts a server which players can connect to. Set <port> to choose \n"
				+ "         the communication port. Choose a <mapfile> which provides sufficient\n"
				+ "         information for the server to build a map. Set a custom <seed> for\n"
				+ "         all random calculations(OPTIONAL) and determine the <num players>\n"
				+ "         required to start a game.\n");
		System.out.println("     " + gui + " <ip>:<port> -n <name>");
		System.out.println("         Start a graphical user interface for a human player. Choose an <ip>\n"
				+ "         adress and a <port> to choose a server to connect. With the -n <name>\n"
				+ "         parameters you can set a custom username.\n");
		System.out.println("     " + ki + " <ip>:<port> [-n <name>]");
		System.out.println("          Start an artificial intelligence. Choose an <ip> adress and a <port>\n"
				+ "          to choose a server to connect. Note that the name is optional and a\n"
				+ "          default name is given by the program.\n");
		System.out.println("     --debug\n" + "          start with debug settings(not yet implemented.)\n");
		System.out.println("This program was created by team 07 from Saarland University in 2015.");
		System.out.println("For more information use google.");

	}

	private static boolean isValidIp(String ipString) {

		if (!ipString.equals("localhost")) {
			String[] splitIp = ipString.split("\\.");
			if (splitIp.length == 4) {

				for (int i = 0; i < 4; i++) {
					try {
						int ip = Integer.parseInt(splitIp[i]);
						if (ip < 0 || ip > 255) {
							return false;
						}
					} catch (NumberFormatException e) {
						return false;
					}

				}
				return true;

			} else
				return false;
		} else
			return true;
	}

	/**
	 * The parseCommandline method that parses the arguments given to main. It
	 * will detect error and if possible evaluate them and either call the
	 * createPlayerLogic method or the createGameLogic method.
	 * 
	 * NOTE: output if error should start with "Error in args:"
	 * 
	 * @param args
	 * 
	 */
	public static boolean parseCommandLine(String[] args) {

		if (args.length == 0) {
			System.out.println("Error in args:");
			System.out.println("-You need arguments to pass");
			System.out.println("--help for more information");
		} else if (args[0].equals("--help")) {
			help();
			return false;
		} else if (args[0].equals("--debug")) {
			System.out.println("Maybe comeing");
			return false;
		} else if (args[0].equals("--server")) {

			isServer = true;

			if (args.length == 6 || args.length >= 8) {
				try {
					port = Integer.parseInt(args[1]);
				} catch (NumberFormatException e) {
					System.out.println("Error in args: " + args[1] + " should be an integer.\n"
							+ "See --help for more information.");
					return false;
				}
				// maybe we can restrict this more then < 0
				if (port < 0) {
					System.out.println("Error in args: Port cannot be less then 0.");
					System.out.println("See --help for more information.");
					return false;
				}

				if (args[2].equals("-map")) {

					fileName = args[3];

					if (args[4].equals("-s")) {
						try {
							seed = Integer.parseInt(args[5]);
						} catch (NumberFormatException e) {
							System.out.println("Error in args: " + args[5] + " Seed should be an integer.\n"
									+ "See --help for more information.");
							return false;
						}

						if (args[6].equals("-p")) {

							try {
								numPlayers = Integer.parseInt(args[7]);

							} catch (NumberFormatException e) {
								System.out.println("Error in args: Number players should be an integer, but was "
										+ args[7] + ".\n" + "See --help for more information.");
								return false;
							}
							if (numPlayers <= 0) {
								System.out.println(
										"Error in args: Number of players should be greater than 0 but was " + args[7]);

								System.out.println("See --help for more information.");
								return false;
							}

							if (args.length == 8) {
								return true;
							} else { // the logging server
								if (args[9].equals("-log")) {
									isLoggingServer = true;
									logdir = new File(args[10]);
								}

							}

						} else {

							System.out.println("Error in args: " + args[4] + " should be '-p'.");

							System.out.println("See --help for more information.");
							return false;
						}

					} else if (args[4].equals("-p")) {

						try {
							numPlayers = Integer.parseInt(args[5]);
						} catch (NumberFormatException e) {
							System.out.println("Error in args: Number Players should be an integer, but was " + args[5]
									+ ".\n" + "See --help for more information.");
							return false;
						}
						if (numPlayers <= 0) {
							System.out.println(
									"Error in args: Number of players should be greater than 0 but was " + args[5]);
							return false;
						}
						return true;

					} else {
						System.out.println("Error in args: " + args[4] + " should be '-s' or '-p'.");
						System.out.println("See --help for more information.");
						return false;
					}
				} else {
					System.out.println("Error in args: " + args[2] + " should be '-map'.");
					System.out.println("See --help for more information");
					return false;

				}
			} else {
				System.out.println("Error in args: Expected 6 or 8 arguments, but were " + args.length
						+ "\nSee --help for more information.");
				return false;
			}
		} else if (args[0].equals("--ki")) {

			isGui = false;

			if (args.length == 2) {
				String[] hostPort = args[1].split(":");
				if (hostPort.length == 2) {
					host = hostPort[0];
					if (!(isValidIp(host))) {
						System.out.println("Error in args: " + host + " is not a valid IPv4 adress.");
						return false;
					}
					try {
						port = Integer.parseInt(hostPort[1]);
					} catch (NumberFormatException e) {
						System.out.println("Error in args: " + hostPort[1] + " is not a valid port.\n"
								+ "See --help for more information.");
						return false;
					}

					if (port <= 0) {
						System.out.println("Error in args: Port has to be greater than 0.");
						return false;
					}
				}
				return true;
			} else if (args.length == 4) {

				String[] hostPort = args[1].split(":");
				if (hostPort.length == 2) {
					host = hostPort[0];
					if (!(isValidIp(host))) {
						System.out.println("Error in args: " + host + " is not a valid IPv4.");
						return false;
					}
					try {
						port = Integer.parseInt(hostPort[1]);
					} catch (NumberFormatException e) {
						System.out.println("Error in args: " + hostPort[1] + " is not a valid port.\n"
								+ "See --help for more information.");
						return false;
					}

					if (port <= 0) {
						System.out.println("Error in args: Port has to be greater than 0.");
						return false;
					}
				}

				if (args[2].equals("-n")) {
					name = args[3];
					return true;
				} else {
					System.out.println("Error in args: Parameter " + args[2] + " should be '-n'");
					System.out.println("See --help for more information");
					return false;
				}

			} else {
				System.out.println("Number args does not match: Expected 2 or 4 but was " + args.length
						+ "\nSee --help for more information.");
				return false;
			}

		} else if (args[0].equals("--gui")) {

			isGui = true;

			if (args.length == 4) {
				String[] hostPort = args[1].split(":");
				if (hostPort.length == 2) {
					host = hostPort[0];
					if (!(isValidIp(host)) && !(host.equals("localhost"))) {
						System.out.println("Error in args: " + host + " is not a valid IPv4.");
						return false;
					}
					try {
						port = Integer.parseInt(hostPort[1]);
					} catch (NumberFormatException e) {
						System.out.println("Error in args: " + hostPort[1] + " is not a valid port.\n"
								+ "See --help for more information.");
						return false;
					}

					if (port <= 0) {
						System.out.println("Error in args: Port has to be greater than 0.");
						return false;
					}

				}

				if (args[2].equals("-n")) {
					name = args[3];
					return true;
				} else {
					System.out.println("Error in args: Parameter " + args[2] + " should be '-n'");
					System.out.println("See --help for more information");
					return false;
				}

			} else {
				System.out.println("Number args does not match: Expected 4 but was " + args.length
						+ "\nSee --help for more information.");
				return false;
			}
		} else {
			System.out.println("Error in args:\n" + "-You need one of those as first argument: --server\n"
					+ "                                          --ki\n"
					+ "                                          --gui\n"
					+ "                                          --help");
			System.out.println("See --help for more informtion.");
			return false;

		}
		return false;
	}

	/**
	 * @return Ki(--ki) or Gui(--gui)
	 */
	private static Gamer createGamer() {
		if (isGui) {
			return new GUI();
		} else
			return new KI(name);
	}

	/**
	 * The start of the zuzumzug program
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		if (parseCommandLine(args)) {
			if (isServer) {
				if (isLoggingServer) {
					LoggingServer logServer = new LoggingServer(port, logdir, new File(fileName), seed, numPlayers);

					GameLogic gl = new GameLogic(port, fileName, seed, numPlayers, logServer);

					gl.waitPlayers();
					gl.parseFile(fileName);
					gl.distributeCards();
					gl.run();

				} else {
					GameLogic gl = new GameLogic(port, fileName, seed, numPlayers);

					gl.waitPlayers();
					if (gl.parseFile(fileName)) {
						gl.distributeCards();
						gl.run();
					} else {
						System.exit(1);
					}
				}

			} else {
				Gamer gamer = createGamer();
				PlayerLogic pl = new PlayerLogic(host, port, name);
				gamer.go(pl);
			}

		} else {
			System.exit(1);
		}
	}
}