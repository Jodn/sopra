package de.unisaarland.sopra.zugumzug.events;

import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.zugumzug.data.Contestant;

public class PlayerLeftEvent implements Event {

	private int id;
	private List<Contestant> listPlayers;

	public PlayerLeftEvent(int id, List<Contestant> listPlayers) {

		this.id = id;
		this.listPlayers = listPlayers;
	}

	/**
	 * Tells the player that an other player has left the current game. (Is send
	 * to all players and observers).
	 */
	@Override
	public void execute() {
		Iterator<Contestant> iter = listPlayers.iterator();

		while (iter.hasNext()) {
			int i = 0;
			Contestant temp = iter.next();
			if (temp.getId() == id) {
				listPlayers.remove(i);
				break; // if you're sure that there is only a player once in the
						// list.
			}
			i++;
		}

	}

}
