package de.unisaarland.sopra.zugumzug.events;

import java.util.Map;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.client.PlayerLogic;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.Track;

public class TrackBuiltEvent implements Event {
	private int myId;
	private int numLocs;
	private Track track;
	private TrackKind color;
	private Contestant contestant;

	public TrackBuiltEvent(int myId, int playerId, int trackId, int numLocs, Board board, Map<Integer, Contestant> playermap,
			TrackKind color) {
		this.numLocs = numLocs;
		this.track = board.getListTracks().get(trackId);
		this.color = color;
		this.contestant = playermap.get(playerId);
		this.myId = myId;
	}

	/**
	 * Tells the player that a player has built a new track. (Is send to all
	 * players and observers).
	 */
	@Override
	public void execute() {
		int trains = contestant.getTrainsLeft();
		track.setOwnerId(contestant.getId());
		contestant.getListTracks().add(track);

		contestant.setPoints(contestant.getPoints() + track.getPoints());
		if (contestant.getId() == myId) {
			for (int i = numLocs; i > 0; i--) {
				contestant.removeResource(TrackKind.ALL);
			}
			for (int i = track.getLength() - numLocs; i > 0; i--) {
				contestant.removeResource(color);
			}
		}
		contestant.setTrainsLeft(trains-track.getLength());
	}
}
