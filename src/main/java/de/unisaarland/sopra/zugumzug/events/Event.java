/**
 * Note: Not all events have their own class
 */

package de.unisaarland.sopra.zugumzug.events;

import java.io.IOException;

public interface Event {

	/**
	 * The execute method that handles the events action on player side.
	 * @throws IOException 
	 */
	public abstract void execute() throws IOException;
}
