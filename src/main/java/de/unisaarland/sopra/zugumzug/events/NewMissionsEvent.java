package de.unisaarland.sopra.zugumzug.events;

import java.io.IOException;

import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.zugumzug.client.Gamer;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;

public class NewMissionsEvent implements Event {

	private Mission[] missions;
	private Contestant contestant;
	private Board board;
	static Gamer gamer;

	public NewMissionsEvent(Mission[] missions, Contestant contestant, Board board,Gamer gamer) {
		this.missions = missions.clone();
		this.contestant = contestant;
		this.board = board;
		NewMissionsEvent.setGamer(gamer);
	}

	@Override
	public void execute() throws IOException {
		for (int i = missions.length - 1; i >= 0; i--) {
			contestant.getTempMissions().add(createMission(missions[i]));
		}
		gamer.newMissions();
		
	}

	private MissionCard createMission(Mission singleMission) {

		City city1 = board.getListCities().get(singleMission.getStart());
		City city2 = board.getListCities().get(singleMission.getDestination());
		MissionCard tempMission = new MissionCard(city1, city2, singleMission.getPoints(), singleMission.getId());

		return tempMission;

	}
	
	public static void setGamer (Gamer gamer){
		NewMissionsEvent.gamer = gamer;
	}

}
