package de.unisaarland.sopra.zugumzug.events;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Track;

public class NewTrackEvent implements Event {

	private int trackId, length;
	private City city1, city2;
	private TrackKind color;
	private Board board;
	private boolean tunnel;

	public NewTrackEvent(int trackId, int length, City city1, City city2, Board board, TrackKind color,
			boolean tunnel) { // color
		// trackind
		this.board = board;
		this.trackId = trackId;
		this.city1 = city1;
		this.city2 = city2;
		this.length = length;
		this.color = color;
		this.tunnel = tunnel;
	}

	/**
	 * Tells the player that a new track is on the map. (Is send to all players
	 * and observers).
	 */
	@Override
	public void execute() {

		Track temptreck = new Track(trackId, city1, city2, length, this.calcPoints(length), tunnel, color);
		if (!board.getListTracks().contains(temptreck) && trackId >= 0) {
			board.getListTracks().add(temptreck);
			city1.getMap().put(trackId, city2);
			city2.getMap().put(trackId, city1);
		}
	}

	private int calcPoints(int x) { // Kidding

		return (int) Math.round(((int) x * x * 0.428571 - 0.228571 * x + 0.8));
	}

}
