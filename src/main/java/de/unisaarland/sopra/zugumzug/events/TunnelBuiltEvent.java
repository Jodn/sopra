package de.unisaarland.sopra.zugumzug.events;

import java.util.Map;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.client.PlayerLogic;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.Track;

public class TunnelBuiltEvent implements Event {

	private int numLocs, costs;
	private TrackKind color;
	private Track track;
	private Contestant contestant;

	private static PlayerLogic pl;


	public TunnelBuiltEvent(int playerId, int trackId, int numLocs, TrackKind color, int additionalCosts, Board board,
			Map<Integer, Contestant> playerMap, PlayerLogic pl) {
		this.numLocs = numLocs;
		this.track = board.getListTracks().get(trackId);
		this.color = color;
		this.contestant = playerMap.get(playerId);
		this.costs = additionalCosts + this.track.getLength();
		this.pl = pl;


	}

	/**
	 * Tells the player that a new tunne is built. (Is send to all players and
	 * observers).
	 */
	@Override
	public void execute() {
		int trains = contestant.getTrainsLeft();
		int points = contestant.getPoints();
		track.setOwnerId(contestant.getId());
		contestant.getListTracks().add(track);
		if (contestant.getId() == pl.myId) {
			for (int i = numLocs; i > 0; i--) {
				contestant.removeResource(TrackKind.ALL);
			}
			for (int i = costs - numLocs; i > 0; i--) {
				contestant.removeResource(color);
			}
	}

			contestant.setTrainsLeft(trains-track.getLength());
			contestant.setPoints(points+track.getPoints());
		
	}
}
