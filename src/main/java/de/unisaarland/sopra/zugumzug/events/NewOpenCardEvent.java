package de.unisaarland.sopra.zugumzug.events;

import java.util.List;

import de.unisaarland.sopra.TrackKind;

public class NewOpenCardEvent implements Event {

	private TrackKind color;
	private List<TrackKind> openResource;

	public NewOpenCardEvent(TrackKind color, List<TrackKind> openResources) {
		this.color = color;
		this.openResource = openResources;
	}

	/**
	 * Tells the player that a new open card is on the board. (Is send to all
	 * players and observers).
	 */
	@Override
	public void execute() {
		
		if (openResource.size() < 5) {
			openResource.add(color);
		}
		else
		{
			openResource.remove(0);
			openResource.add(color);
		}
	}

}
