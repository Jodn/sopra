package de.unisaarland.sopra.zugumzug.events;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Contestant;

public class NewCardsEvent implements Event {

	private TrackKind color;
	private Contestant contestant;
	private int numCards;

	public NewCardsEvent(Contestant contestant, TrackKind color, int numCards) {
		this.contestant = contestant;
		this.color = color;
		this.numCards=numCards;
	}

	/**
	 * This events is send if a takeSecretCommand was requested by the player.
	 * (Is send to all players and observers).
	 */
	@Override
	public void execute() {
		for(int i=0; i<numCards;i++)
		contestant.getHandCards().add(color);
	}

}
