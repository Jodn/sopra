package de.unisaarland.sopra.zugumzug.events;

import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;

public class NewCityEvent implements Event {

	private int cityId;
	private String name;
	private int x, y; // x and y coordinate
	private Board board;

	public NewCityEvent(int cityId, String name, int x, int y, Board board) {
		this.cityId = cityId;
		this.name = name;
		this.x = x;
		this.y = y;
		this.board = board;
	}

	/**
	 * Tells the player that the map has a new City. (Is send to all players and
	 * observers).
	 */
	@Override
	public void execute() {
		City tempCity = new City(name, cityId, x, y);

		if (!board.getListCities().contains(tempCity) && cityId >= 0) {
			board.getListCities().add(tempCity);
		}
	}

}
