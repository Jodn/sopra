package de.unisaarland.sopra.zugumzug.events;

import java.util.List;
import java.util.Map;

import de.unisaarland.sopra.zugumzug.data.Contestant;

public class NewPlayerEvent implements Event {

	private int id;
	private String name;
	private List<Contestant> listPlayer;
	private Map<Integer, Contestant> playerMap;

	public NewPlayerEvent(int id, String name, List<Contestant> listPlayer, Map<Integer, Contestant> playerMap) {

		this.id = id;
		this.name = name;
		this.listPlayer = listPlayer;
		this.playerMap = playerMap;
	}

	/**
	 * Tells the player that a new player (not observer) has entered the game.
	 * (Is send to all players and observers).
	 */
	@Override
	public void execute() {
		// may just call the GL.addcontestant?!

	
		
		Contestant contestant = new Contestant(id, name, 0);
		

		if(listPlayer.isEmpty() || !(playerMap.containsKey(id)))
		{
			addPlayer(contestant);
			playerMap.put(id, contestant);
		}
		
		playerMap.get(id).setName(name);
		

	/*	playerMap.put(contestant.getId(), contestant);
		if (listPlayer.size() < 2) {
			listPlayer.add(contestant);
		}

		int i = 0;
		while (i < listPlayer.size() && listPlayer.get(i).getId() < contestant.getId()) {
			i++;
		}
		if (i > listPlayer.size() - 1) {
			listPlayer.add(listPlayer.size(), contestant);
		} else {
			listPlayer.add(i, contestant);
		}*/
	}
	
	public void addPlayer(Contestant cont){
		int i=0;
		while(i<listPlayer.size()&& listPlayer.get(i).getId()<cont.getId()) {
			i++;
		}
		if(i>listPlayer.size()-1) {
			listPlayer.add(listPlayer.size(), cont);
		}
		else
			listPlayer.add(i, cont);
	}

}
