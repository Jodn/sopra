package de.unisaarland.sopra.zugumzug.events;

import java.io.IOException;
import java.util.List;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Contestant;

public class TookCardEvent implements Event{
	
	private Contestant c;
	private TrackKind kind;
	private List<TrackKind> openStack;
	private Contestant me;

	public TookCardEvent(Contestant me,Contestant c, TrackKind kind, List<TrackKind> openStack) {
		this.c=c;
		this.kind=kind;
		this.openStack=openStack;
		this.me = me;
	}

	@Override
	public void execute() throws IOException {
		if(me.equals(c)){
		me.getHandCards().add(kind);
		}
		else
		{
			c.getHandCards().add(kind);
		}
		openStack.remove(kind);
		
		
	}

}
