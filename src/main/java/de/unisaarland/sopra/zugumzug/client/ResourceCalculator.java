package de.unisaarland.sopra.zugumzug.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Track;

public class ResourceCalculator {
	
	List<ConnectedCities> cc;
	private HashMap<Track,TrackKind> trackMap;
	private HashMap<TrackKind,Integer> handCards;
	private PlayerLogic pl;

	public ResourceCalculator(List<ConnectedCities> cc, PlayerLogic pl) {
		this.cc=cc;
		this.pl=pl;
		handCards= new HashMap<TrackKind,Integer>();
		trackMap= new HashMap<Track,TrackKind>();
		calculatehandCards();
	}
	

	private void calculatehandCards() {
		
		for(TrackKind tk : TrackKind.values()) {
			handCards.put(tk, 0);
		}
		
		for(int i=0; i< pl.getContestant().getHandCards().size();i++) {
			handCards.put(pl.getContestant().getHandCards().get(i), handCards.get(pl.getContestant().getHandCards().get(i))+1);
		}
	}
	
	
	public HashMap<Track,TrackKind> filter() {
		int addCostCalc=1;
		calculatehandCards();
		List<Track> notDecidedTracks= new ArrayList<Track>();
		List<ConnectedCities> parallelTracks= new ArrayList<ConnectedCities>();
		
		
		//filter all tracks with fixed color and only one possibility
		//sub from handcards
		for(int i=0; i< cc.size(); i++) {
			if(cc.get(i).tracks.size()==1) {
				if (cc.get(i).tracks.get(0).getOwnerId()==pl.myId) { 
					continue;
				}
				
				TrackKind color=cc.get(i).tracks.get(0).getColor();
				if(color!=TrackKind.ALL) {
				trackMap.put(cc.get(i).tracks.get(0), color );
				if(cc.get(i).tracks.get(0).isTunnel()) {
					handCards.put(color, handCards.get(color)-cc.get(i).tracks.get(0).getLength()-addCostCalc);
				} else {
					handCards.put(color, handCards.get(color)-cc.get(i).tracks.get(0).getLength());
				}
					
				} else {
					notDecidedTracks.add(cc.get(i).tracks.get(0));
				}
			} else { //parallel Track, save for later
				parallelTracks.add(cc.get(i));
			}
		}
		
		//checkParallelTracks, pick one now
		for(int i=0; i<parallelTracks.size();i++) {
			
			ConnectedCities tmpcc= parallelTracks.get(i);
			List<Track> tunnel= new ArrayList<Track>();
			List<Track> betterTracks = new ArrayList<Track>();
			
			//find out, if one track already belongs to me
			boolean owning=false;
			for(int f=0; f<tmpcc.tracks.size();f++) {
				if(tmpcc.tracks.get(f).getOwnerId()==pl.myId) {
					owning=true;
				}
			}
			if(owning) {
				continue;
			}
			
			for(int u=0; u<tmpcc.tracks.size();u++) {
				if(tmpcc.tracks.get(u).getOwnerId()!=-1) {
					tmpcc.tracks.remove(u);
				}
			}
			
			
			//pick shortest Track
			int max2=tmpcc.tracks.get(0).getLength();
			for(int z= 0; z<tmpcc.tracks.size();z++) {
				if(tmpcc.tracks.get(z).getLength()>max2) {
					tmpcc.tracks.remove(tmpcc.tracks.get(z));
				}
			}
			
			
			for(int j=0; j<tmpcc.tracks.size();j++) {
				if(tmpcc.tracks.get(j).isTunnel()) {
					tunnel.add(tmpcc.tracks.remove(j));
				}
			}
			
			if(tmpcc.tracks.size()>0) {
				betterTracks.addAll(tmpcc.tracks);
			} else {
				betterTracks= tunnel;
			}
			

				boolean added=false;
				for(int k=0; k<betterTracks.size();k++) {
					if(betterTracks.get(k).getColor()==TrackKind.ALL) {
						notDecidedTracks.add(betterTracks.get(k));
						added=true;
						break;
					} 
						
				} 
				if(!added) {
					int max= handCards.get(betterTracks.get(0).getColor());
					Track tmpTrack=betterTracks.get(0);
					for(int l=0; l<betterTracks.size();l++) {
						if(handCards.get(betterTracks.get(l).getColor())>max) {
							tmpTrack= betterTracks.get(l);
							max= handCards.get(betterTracks.get(l).getColor());
						}
					}
				if(tmpTrack.isTunnel()) {
					handCards.put(tmpTrack.getColor(), handCards.get(tmpTrack.getColor())-tmpTrack.getLength()-addCostCalc);
				} else {
					handCards.put(tmpTrack.getColor(), (handCards.get(tmpTrack.getColor())-tmpTrack.getLength()));
				}
				trackMap.put(tmpTrack, tmpTrack.getColor());
				}
			
		}
		
		//now pick a color for ALL
		notDecidedTracks=sortList(notDecidedTracks);
		for(int g=0; g<notDecidedTracks.size();g++) {
			int max1= handCards.get(TrackKind.RED);
			TrackKind tmpkind= TrackKind.RED;
			for(TrackKind tk: TrackKind.values()) {
				if(handCards.get(tk)>max1 &&tk!=TrackKind.ALL){
					max1= handCards.get(tk);
					tmpkind=tk;
				}
			}
			if(notDecidedTracks.get(g).isTunnel()) {
				handCards.put(tmpkind, handCards.get(tmpkind)-notDecidedTracks.get(g).getLength()-addCostCalc);
			} else {
				handCards.put(tmpkind, handCards.get(tmpkind)-notDecidedTracks.get(g).getLength());
			}
			trackMap.put(notDecidedTracks.get(g), tmpkind);
		}
		
		
	return trackMap;	
		
	}
	
	public boolean canBuildAll() {
		int neg=0;
		int trackLength= calcTrackLength();
		
		for(TrackKind tk: TrackKind.values()) {
			if(handCards.get(tk)<0&& tk!=TrackKind.ALL) {
				neg+=handCards.get(tk);
			}
		}
		
		if(neg+handCards.get(TrackKind.ALL)>0)
			return true;
		
		return false;
	}
	
	public HashMap<TrackKind, Integer> getHandCards() {
		return handCards;
	}
	
	private List<Track> sortList(List<Track> notDecidedTracks) {
		Comparator<Track> trackComp = new TrackComparator();
		java.util.Collections.sort(notDecidedTracks, trackComp);
		return notDecidedTracks;
		
	}
	
	private int calcTrackLength() {
		int res=0;
		for(Iterator<Track> it= trackMap.keySet().iterator(); it.hasNext();) {
			res += it.next().getLength();
		}
		return res;
	}

}




class TrackComparator implements Comparator<Track> {

	public int compare(Track track1, Track track2) {
		if (track1.getLength() > track2.getLength()) {
			return -1;
		} else if (track1.getLength() < track2.getLength()) {
			return 1;
		} else
			return 0;
	}

}

