package de.unisaarland.sopra.zugumzug.client;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.moves.ReturnMissionsMove;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextArea;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseEvent;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class missionController implements Initializable{
	
	static private PlayerLogic pl;
	static private List<MissionCard> tempmissionsGUI = new ArrayList<MissionCard>();
	static private List<BorderPane> tempPanesGUI = new ArrayList<BorderPane>();
	static private InnerShadow innerShadow = new InnerShadow();
	static final private SimpleStringProperty mission1prop = new SimpleStringProperty(); //TESTING 
	static final private SimpleStringProperty mission2prop = new SimpleStringProperty(); // FINAL?!?!
	static final private SimpleStringProperty mission3prop = new SimpleStringProperty();
	
	private double x,y;
@FXML
TextArea mission1text;

@FXML
TextArea mission2text;

@FXML
TextArea mission3text;

@FXML
BorderPane mission3pane, mission2pane, mission1pane;


@FXML
public void mission1select(){
	if(!tempmissionsGUI.contains(pl.getContestant().getTempMissions().get(0))){
		mission1pane.setEffect(innerShadow);
		tempmissionsGUI.add(pl.getContestant().getTempMissions().get(0));
	}
	else
	{
		mission1pane.setEffect(null);
		tempmissionsGUI.remove(tempmissionsGUI.indexOf(pl.getContestant().getTempMissions().get(0)));
		
	}
}

@FXML
public void mission2select(){
	if(!tempmissionsGUI.contains(pl.getContestant().getTempMissions().get(1))){
		mission2pane.setEffect(innerShadow);
		tempmissionsGUI.add(pl.getContestant().getTempMissions().get(1));
	}
	else
	{
		mission2pane.setEffect(null);
		tempmissionsGUI.remove(tempmissionsGUI.indexOf(pl.getContestant().getTempMissions().get(1)));
		
	}
}
@FXML
public void mission3select(){
	if(!tempmissionsGUI.contains(pl.getContestant().getTempMissions().get(2))){
		mission3pane.setEffect(innerShadow);
		tempmissionsGUI.add(pl.getContestant().getTempMissions().get(2));
	}
	else
	{
		mission3pane.setEffect(null);
		tempmissionsGUI.remove(tempmissionsGUI.indexOf(pl.getContestant().getTempMissions().get(2)));
		
	}
	
}

@FXML
public void dragmissionwindow(){


}
@FXML
Pane missionpane;

@FXML
public void missionsok() throws Exception{
	List<MissionCard> ReturnMission = new ArrayList<MissionCard>(pl.getContestant().getTempMissions());
	if(tempmissionsGUI != null && tempmissionsGUI.size()>0){
	for(MissionCard e: tempmissionsGUI){
		 ReturnMission.remove(ReturnMission.indexOf(e));
	}
	
	
	ReturnMissionsMove rmm = new ReturnMissionsMove(ReturnMission);
	pl.handleMove(rmm);
	
	
	stage.close();
	}
	else
	{
		Alert dialog = new Alert(AlertType.INFORMATION);
		DialogPane dialogPane = dialog.getDialogPane();
		dialogPane.setStyle("-fx-background-color: rgba(0, 0, 0, 0.1);  -fx-border-radius: 10 10 0 0; -fx-background-radius: 10 10 0 0;"); 	 
		dialog.setTitle("Chose your missions");
		dialog.setHeaderText(null);
		dialog.setContentText("You need to chose at lease one mission.");
		dialog.initOwner(stage);
		dialog.showAndWait();
	}
}

static private Stage stage;

public void setStage(Stage stage){
	this.stage = stage;
	
	
}
public void missionTakeGUI() throws IOException{
	
  }
@Override
public void initialize(URL arg0, ResourceBundle arg1) {

	//mission1text.setStyle("-fx-border-radius: 10 10 0 0; -fx-background-radius: 10 10 0 0;");
	innerShadow.setChoke(0.3);
	innerShadow.setWidth(100.0);
	tempPanesGUI = new ArrayList<BorderPane>();
	mission1text.textProperty().bind(mission1prop);
	mission2text.textProperty().bind(mission2prop);
	mission3text.textProperty().bind(mission3prop);
	tempPanesGUI.add(mission1pane);
	tempPanesGUI.add(mission2pane);
	tempPanesGUI.add(mission3pane);
	 
	missionpane.setOnMousePressed(new EventHandler<MouseEvent>() {
		  @Override public void handle(MouseEvent mouseEvent) {
		    x = stage.getX() - mouseEvent.getScreenX();
		    y = stage.getY() - mouseEvent.getScreenY();
		  }
		});
		missionpane.setOnMouseDragged(new EventHandler<MouseEvent>() {
		  @Override public void handle(MouseEvent mouseEvent) {
		      stage.setX(mouseEvent.getScreenX() + x);
		    stage.setY(mouseEvent.getScreenY() + y);
		  }
		});	
	
}

public void setPL(PlayerLogic pl){
	this.pl = pl;
	if(this.pl.getContestant().getTempMissions()!=null && this.pl.getContestant().getTempMissions().size()>0){
	List<MissionCard> missions = pl.getContestant().getTempMissions();
	Platform.runLater(new Runnable() {
		final String mission1string = "From " +missions.get(0).getCity1() + " to " + missions.get(0).getCity2() + " \n" + "Points: "+ missions.get(0).getPoints();
		final String mission2string = "From " +missions.get(1).getCity1() + " to " + missions.get(1).getCity2() + " \n" + "Points: "+ missions.get(1).getPoints();
		final String mission3string = "From " +missions.get(2).getCity1() + " to " + missions.get(2).getCity2() + " \n" + "Points: "+ missions.get(2).getPoints();
		  @Override public void run() {
			 
			  mission1prop.set(mission1string);
			  mission2prop.set(mission2string);
			  mission3prop.set(mission3string);
		  }
	});	
	}
}



}
