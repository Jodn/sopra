package de.unisaarland.sopra.zugumzug.client;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Track;

public class TrackCalculator {
	
	private HashMap<Track,TrackKind> tracksToBuild;
	private List<TrackKind> cards;
	private HashMap<TrackKind,Integer> handCards;
	

	public TrackCalculator(HashMap<Track,TrackKind> tracksToBuild, List<TrackKind> handCards) {
		this.tracksToBuild=tracksToBuild;
		this.cards=handCards;
		this.handCards= new HashMap<TrackKind,Integer>();
		calculatehandCards();
	}
	
	private void calculatehandCards() {
		
		for(TrackKind tk : TrackKind.values()) {
			handCards.put(tk, 0);
		}
		
		for(int i=0; i< cards.size();i++) {

			handCards.put(cards.get(i), handCards.get(cards.get(i))+1);

		}
	}

	public Track pickTrack() {
		int max=0;
		Track maxTrack=null;
		for(Iterator<Track> it= tracksToBuild.keySet().iterator(); it.hasNext();){
			Track tmpt= it.next();
			int canBuild= handCards.get(tracksToBuild.get(tmpt)) + handCards.get(TrackKind.ALL);
		
			if(canBuild>=tmpt.getLength()) {
			if(max<tmpt.getLength()) {
				
				max=tmpt.getLength();
				maxTrack=tmpt;
				
		
			}else if( max== tmpt.getLength()) {
				
				if(maxTrack.getColor()==TrackKind.ALL) {
					maxTrack=tmpt;
			
					}
				}
			}
		}
		return maxTrack;
	}
	
}
