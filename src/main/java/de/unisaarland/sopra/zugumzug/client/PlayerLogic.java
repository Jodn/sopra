package de.unisaarland.sopra.zugumzug.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.unisaarland.sopra.AdditionalCost;
import de.unisaarland.sopra.Die;
import de.unisaarland.sopra.Failure;
import de.unisaarland.sopra.FinalMissions;
import de.unisaarland.sopra.FinalPoints;
import de.unisaarland.sopra.GameState;
import de.unisaarland.sopra.LongestTrack;
import de.unisaarland.sopra.MaxTrackLength;
import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.NewCards;
import de.unisaarland.sopra.NewCity;
import de.unisaarland.sopra.NewMissions;
import de.unisaarland.sopra.NewOpenCard;
import de.unisaarland.sopra.NewPlayer;
import de.unisaarland.sopra.NewTrack;
import de.unisaarland.sopra.Player;
import de.unisaarland.sopra.PlayerLeft;
import de.unisaarland.sopra.ServerEvent;
import de.unisaarland.sopra.TookCard;
import de.unisaarland.sopra.TookMissions;
import de.unisaarland.sopra.TookSecretCard;
import de.unisaarland.sopra.TrackBuilt;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.TunnelBuilt;
import de.unisaarland.sopra.TunnelNotBuilt;
import de.unisaarland.sopra.Winner;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.GameEngine;
import de.unisaarland.sopra.zugumzug.events.Event;
import de.unisaarland.sopra.zugumzug.events.NewCardsEvent;
import de.unisaarland.sopra.zugumzug.events.NewCityEvent;
import de.unisaarland.sopra.zugumzug.events.NewMissionsEvent;
import de.unisaarland.sopra.zugumzug.events.NewOpenCardEvent;
import de.unisaarland.sopra.zugumzug.events.NewPlayerEvent;
import de.unisaarland.sopra.zugumzug.events.NewTrackEvent;
import de.unisaarland.sopra.zugumzug.events.TookCardEvent;
import de.unisaarland.sopra.zugumzug.events.TrackBuiltEvent;
import de.unisaarland.sopra.zugumzug.events.TunnelBuiltEvent;
import de.unisaarland.sopra.zugumzug.moves.BuildTrackMove;
import de.unisaarland.sopra.zugumzug.moves.Moves;
import de.unisaarland.sopra.zugumzug.moves.PayTunnelMove;
import de.unisaarland.sopra.zugumzug.moves.RegisterMove;
import de.unisaarland.sopra.zugumzug.moves.ReturnMissionsMove;
import de.unisaarland.sopra.zugumzug.moves.TakeCardMove;

public class PlayerLogic extends GameEngine {

	static private Gamer gamer;
	static private Player player;
	static private Contestant contestant;
	final public int myId;
	private int numCurrentPlayers = 0;
	private int numRequiredPlayers;
	private int numCards;
	private boolean connected = true;
	private int turnsLeft = 2;
	private boolean gamestartboolean;
	private int maxHandCards;
	private List<TrackKind> rClosedStack;
	protected int rClosedStackSize;

	public PlayerLogic(String host, int port, String Name) throws IOException {
		this.gamestartboolean = false;
		GameEngine.setBoard(new Board());
		this.rClosedStack = new ArrayList<TrackKind>();
		this.setOpenResources(new ArrayList<TrackKind>());
		this.setPlayerList(new ArrayList<Contestant>());
		this.setPlayerMap(new HashMap<Integer, Contestant>());
		PlayerLogic.player = new Player(host, port);
		connected = true;
		rClosedStackSize=0;

		// try {

		// this.player.register(Name);
		PlayerLogic.contestant = new Contestant(player.getPlayerId(), Name, -1);
		this.getPlayerList().add(contestant);
		myId = player.getPlayerId();
		getPlayerMap().put(myId, contestant);

		// } catch (Exception e) {

		// System.out.println(e.toString());
		// }
		// this.player.register(Name);
		// this.contestant= new Contestant(player.getPlayerId(), Name, -1);
		// myId= player.getPlayerId();
		// connected=true;

	}

	/**
	 * The method handles the incoming message and creates the fitting event out
	 * of the .events package
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void decideEvent(ServerEvent event) throws Exception { // Message
																	// event but
																	// does not
		// compile
		Event myEvent;
		switch (event.getId()) {
		case 24: // Additional Cost
			AdditionalCost adCost = (AdditionalCost) event;
			int ad = adCost.getAdditionalCosts();
			contestant.setAddCosts(ad);
			gamer.additionalCosts(ad);
			break;

		case 6: // Die
			Die devent = (Die) event;
			String m = devent.getMessage();
			gamer.die(m);
			connected = false;
			break;

		case 5: // Failure
			Failure fail = (Failure) event;
			String message = fail.getMessage();
			gamer.failure(message);
			break;

		case 8: // Final Mission
			FinalMissions finalMissions = (FinalMissions) event;
			gamer.finalMissions(finalMissions.getPlayerId(), finalMissions.getMissionSuccess(),
					finalMissions.getMissionFail());
			break;

		case 7: // Final Points
			FinalPoints finalPoints = (FinalPoints) event;
			gamer.finalPoints(finalPoints.getPlayerId(), finalPoints.getPoints());
			break;

		case 10: // GameStart
			gamer.gameStart();
			break;

		case 0: // GameState
			GameState gameState = (GameState) event;
			numRequiredPlayers = gameState.getPlayers();
			break;

		case 9: // Longest Track
			LongestTrack lTrack = (LongestTrack) event;
			gamer.longestTrack(lTrack.getPlayerId(), lTrack.getLength());
			break;

		case 20: // MaxTrackLength
			MaxTrackLength maxTrack = (MaxTrackLength) event;
			this.setMaxTracks(maxTrack.getMaxTracks());
			for (int i = 0; i < getPlayerList().size(); i++) {
				getPlayerList().get(i).setTrainsLeft(maxTrack.getMaxTracks());
			}

			gamer.maxTrackLength(maxTrack.getMaxTracks());
			rClosedStackSize= rClosedStack.size();
			rClosedStackSize-= 4*this.numRequiredPlayers;
			rClosedStackSize -= 5;
			gamestartboolean = true;
			setMaxHandCards(Math.floorDiv(rClosedStack.size(), getNumRequiredPlayers()));
			break;

		case 2: // NewCards

			NewCards newCards = (NewCards) event;
			if (gamestartboolean == true) {
				numCards = newCards.getCount();
				myEvent = new NewCardsEvent(contestant, newCards.getKind(), newCards.getCount());
				myEvent.execute();
				
				gamer.update(contestant);
			} else {
				for (int i = 0; i < newCards.getCount(); i++) {
					rClosedStack.add(newCards.getKind());
				}
			}
			break;

		case 22:// NewCity
			NewCity newCity = (NewCity) event;

			myEvent = new NewCityEvent(newCity.getCityId(), newCity.getName(), newCity.getX(), newCity.getY(),
					this.getBoard());
			myEvent.execute();
			// TODO: bei jeder City das Board updaten?
			break;

		case 4:// NewMissions
			NewMissions newMiss = (NewMissions) event;
			myEvent = new NewMissionsEvent(newMiss.getMissions(), contestant, this.getBoard(), gamer);
			myEvent.execute();
			break;

		case 17: // NewOpenCard
			NewOpenCard oCard = (NewOpenCard) event;
			myEvent = new NewOpenCardEvent(oCard.getKind(), this.getOpenResources());
			myEvent.execute();
			// gamer.update();
			break;

		case 12:// NewPlayer
			NewPlayer nPl = (NewPlayer) event;
			myEvent = new NewPlayerEvent(nPl.getPlayerId(), nPl.getName(), this.getPlayerList(), this.getPlayerMap());
			myEvent.execute();
			numCurrentPlayers++;
			break;

		case 21:// NewTrack
			NewTrack nTr = (NewTrack) event;
			myEvent = new NewTrackEvent(nTr.getTrackId(), nTr.getLength(),
					this.getBoard().getListCities().get(nTr.getCity1()),
					this.getBoard().getListCities().get(nTr.getCity2()), this.getBoard(), nTr.getKind(),
					nTr.getTunnel());
			myEvent.execute();
			break;

		case 11:// PlayerLeft
			PlayerLeft playerLeft = (PlayerLeft) event;
			getPlayerList().remove(playerLeft.getPlayerId());
			getPlayerMap().remove(playerLeft.getPlayerId());
			gamer.playerLeft(playerLeft.getPlayerId());
			break;

		case 18:// TookCard
			TookCard tookCard = (TookCard) event;

			myEvent = new TookCardEvent(contestant, getPlayerList().get(0), tookCard.getKind(),
					this.getOpenResources());
			myEvent.execute();
			rClosedStackSize--;
			// TODO: vielleicht vielleicht aber auch nicht
			if (turnsLeft == 1 || tookCard.getKind() == TrackKind.ALL) {
				Contestant tmp = getPlayerList().remove(0);
				getPlayerList().add(tmp);
				turnsLeft = 2;
			} else {
				turnsLeft--;
			}
			gamer.tookCard(tookCard.getPlayerId(), tookCard.getKind());
			break;

		case 13:// TookMissions
			TookMissions tookMissions = (TookMissions) event;
			gamer.tookMissions(tookMissions.getPlayerId(), tookMissions.getCount());
			Contestant tmp = getPlayerList().remove(0);
			getPlayerList().add(tmp);
			break;

		case 19: // TookSecretCard
			TookSecretCard tsc = (TookSecretCard) event;
			gamer.tookSecretCard(tsc.getPlayerId());
			rClosedStackSize--;
			if (turnsLeft == 1) {
				Contestant tmpcont= getPlayerList().remove(0);
				getPlayerList().add(tmpcont);
				turnsLeft = 2;
			} else {

				turnsLeft--;
			}

			break;

		case 14:// TrackBuilt
			TrackBuilt tb = (TrackBuilt) event;
			myEvent = new TrackBuiltEvent(myId, tb.getPlayerId(), tb.getTrackId(), tb.getLocomotives(), getBoard(),
					this.getPlayerMap(), tb.getKind());
			myEvent.execute();
			gamer.trackBuilt(tb.getPlayerId(), tb.getTrackId());
			Contestant tmpcont = getPlayerList().remove(0);
			getPlayerList().add(tmpcont);
			rClosedStackSize+= getBoard().getListTracks().get(tb.getTrackId()).getLength();
			break;

		case 15:// TunnelBuilt
			TunnelBuilt tub = (TunnelBuilt) event;
			myEvent = new TunnelBuiltEvent(tub.getPlayerId(), tub.getTrackId(), tub.getLocomotives(), tub.getKind(),
					tub.getAdditionalCosts(), getBoard(), getPlayerMap(),this);
			myEvent.execute();
			gamer.tunnelBuilt(tub.getPlayerId(), tub.getTrackId());
			Contestant tmpcont2 = getPlayerList().remove(0);
			getPlayerList().add(tmpcont2);
			rClosedStackSize+= getBoard().getListTracks().get(tub.getTrackId()).getLength();
			rClosedStackSize+= tub.getAdditionalCosts();
			break;

		case 16: // TunnelNotBuilt
			TunnelNotBuilt tnb = (TunnelNotBuilt) event;
			gamer.tunnelNotBuilt(tnb.getPlayerId(), tnb.getTrackId());
			Contestant tmpcont1 = getPlayerList().remove(0);
			getPlayerList().add(tmpcont1);
			break;

		case 23: // Winner
			Winner win = (Winner) event;
			gamer.winner(win.getPlayerId());
			break;

		default: // TODO: Error, Exception
			break;
		}
	}

	public void handleMove(Moves move) throws Exception {
		switch (move.getId()) {

		case 0:
			BuildTrackMove btm = (BuildTrackMove) move;
			player.buildTrack((short) btm.getTrackId(), btm.getKind(), btm.getNumlocs());
			break;
		case 1:
			player.getMissions();
			break;
		case 2:
			player.close();
			connected = false;
			break;
		case 3:
			RegisterMove rm = (RegisterMove) move;
			player.register(rm.getMyName());
			break;
		case 4:
			ReturnMissionsMove rmm = (ReturnMissionsMove) move;
			Mission[] missions = new Mission[rmm.getMissions().size()];
			for (int i = 0; i < rmm.getMissions().size(); i++) {
				Mission m1 = new Mission((short) rmm.getMissions().get(i).getCity1().getId(),
						(short) rmm.getMissions().get(i).getCity2().getId(), rmm.getMissions().get(i).getPoints());
				missions[i] = m1;
				contestant.getTempMissions().remove(rmm.getMissions().get(i));
			}
			contestant.getListMissions().addAll(contestant.getTempMissions());
			contestant.getTempMissions().clear();
			player.returnMissions(missions);

			break;
		case 5:
			TakeCardMove tcm = (TakeCardMove) move;
			player.takeCard(tcm.getKind());
			break;
		case 6:
			player.takeSecretCard();
			break;
		case 7:
			PayTunnelMove ptm= (PayTunnelMove) move;
			//System.out.println(ptm.getNumLocs());
			player.payTunnel(ptm.getNumLocs());
			break;
		}
	}

	public String eventToString(ServerEvent m) {
		switch (m.getId()) {
		case 24: // Additional Cost
			return "Additional Cost Event";

		case 6: // Die
			return "Die Event";

		case 5: // Failure
			return "Failure Event";

		case 8: // Final Mission
			return "Final Mission Event";

		case 7: // Final Points
			return "Final Points Event";

		case 10: // GameStart
			return "Game Start Event";

		case 0: // GameState
			return "Game State Event";

		case 9: // Longest Track
			return "Longest Track Event";

		case 20: // MaxTrackLength
			return "Max Track Length Event";

		case 2: // NewCards
			return "New Cards Event";

		case 22:// NewCity
			return "New City Event";

		case 4:// NewMissions
			return "New Missions Event";

		case 17: // NewOpenCard
			return "New Open Card Event";

		case 12:// NewPlayer
			return "New Player Event";

		case 21:// NewTrack
			return "New Track Event";

		case 11:// PlayerLeft
			return "Player Left Event";

		case 18:// TookCard
			return "Took Card Event";

		case 13:// TookMissions
			return "Took Missions Event";

		case 19: // TookSecretCard
			return "Took Secret Card Event";

		case 14:// TrackBuilt
			return "Track Built Event";

		case 15:// TunnelBuilt
			return "Tunnel Built Event";

		case 16: // TunnelNotBuilt
			return "Tunnel Not Built Event";

		case 23: // Winner
			return "Winner Event";

		default:
			return "This is not a Event";
		}
	}

	/**
	 * The run loop that continuously sends commands and handles events for the
	 * players sake
	 * 
	 * @throws IOException
	 */
	public void run() throws IOException {

		gamer.setContestant(this.contestant);
		gamer.setNumCards(this.numCards);
		gamer.setNumPlayers(this.numRequiredPlayers);

		// gamer.setPlayerLogic(this);
		//// while(true) {
		//// System.out.println("hey");
		//// player.register("Megan");
		//// }
		//// while(connected) {
		//// ServerEvent se= (ServerEvent) player.pollNextEvent();
		//// decideEvent(se);
		//// }
		//
		// while(player.isConnected() && connected) {
		// decideEvent((ServerEvent)player.pollNextEvent());
		// }

		// Thread th = new Thread(eventTask);
		// th.setDaemon(true);
		// th.start();
		// Thread th1 = new Thread(gui);
		// th1.setDaemon(true);
		// th1.start();
		//

	}

	public int getNumRequiredPlayers() {
		return numRequiredPlayers;
	}

	public Gamer getGamer() {
		return gamer;
	}

	public Player getPlayer() {
		return player;
	}

	public Contestant getContestant() {
		return contestant;
	}

	public void setGamer(Gamer gamer) {
		this.gamer = gamer;

	}

	public int getNumCurrentPlayers() {
		return numCurrentPlayers;
	}

	public void setNumCurrentPlayers(int numCurrentPlayers) {
		this.numCurrentPlayers = numCurrentPlayers;
	}

	public int getMaxHandCards() {
		return maxHandCards;
	}

	public List<TrackKind> getrClosedStack() {
		return rClosedStack;
	}

	public void setMaxHandCards(int maxHandCards) {
		this.maxHandCards = maxHandCards;
	}

}
