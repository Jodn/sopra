package de.unisaarland.sopra.zugumzug.client;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.event.ChangeEvent;

import de.unisaarland.sopra.zugumzug.client.ZoomHandler;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.commands.TakeCardCommand;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.moves.GetMissionsMove;
import de.unisaarland.sopra.zugumzug.moves.RegisterMove;
import de.unisaarland.sopra.zugumzug.moves.TakeCardMove;
import de.unisaarland.sopra.zugumzug.moves.TakeSecretCardMove;
import javafx.animation.Animation;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Point3D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;

public class Controller implements Initializable {
	Image rails = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/iconRail.png"), 90, 0, true, true);
	Image lok = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/lok.png/"));
	Image black = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/black.png/"));
	Image blue = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/blue.png/"));
	Image green = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/green.png/"));
	Image violet = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/violet.png/"));
	Image yellow = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/yellow.png/"));
	Image white = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/white.png/"));
	Image orange = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/orange.png/"));
	Image red = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/red.png/"));
	Image loksmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/lok2.png/"));
	Image blacksmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/black2.png/"));
	Image bluesmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/blue2.png/"));
	Image greensmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/green2.png/"));
	Image violetsmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/violet2.png/"));
	Image yellowsmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/yellow2.png/"));
	Image whitesmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/white2.png/"));
	Image orangesmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/orange2.png/"));
	Image redsmall = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/klein/red2.png/"));
	Image playerpic = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/framelas.png/"));
	Image missioncard = new Image(getClass().getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/missioncard.png/"));
	private HashMap<Line,Track> BoardTrackList = new HashMap<Line,Track>();
	static private PlayerLogic pl;
    static private FXMLLoader buildLoader;
   
    static private SimpleStringProperty myres1prop = new SimpleStringProperty();
    static private SimpleStringProperty myres2prop = new SimpleStringProperty();
    static private SimpleStringProperty myres3prop = new SimpleStringProperty();
    static private SimpleStringProperty myres4prop = new SimpleStringProperty();
    static private SimpleStringProperty myres5prop = new SimpleStringProperty();
    static private SimpleStringProperty myres6prop = new SimpleStringProperty();
    static private SimpleStringProperty myres7prop = new SimpleStringProperty();
    static private SimpleStringProperty myres8prop = new SimpleStringProperty();
    static private SimpleStringProperty myres9prop = new SimpleStringProperty();
    static private List<ImageView> resList = new ArrayList<ImageView>();
    static private List<ImageView> openResList = new ArrayList<ImageView>();
    static private List<SimpleStringProperty> resPropList = new ArrayList<SimpleStringProperty>();
    static private List<Label> resLabelList = new ArrayList<Label>();
	private List<TrackKind> HandCards = new ArrayList<TrackKind>();
    private Map<Integer,TrackKind> OpenResCards = new HashMap<Integer,TrackKind>();
	private Map<TrackKind, Integer> HandCardsMap;
	private Map<TrackKind, Image> smallImageMap = new HashMap<TrackKind,Image>();
	private Map<TrackKind, Image> ImageMap = new HashMap<TrackKind, Image>();
	private Map<Integer, TrackKind> TrackMap = new HashMap<Integer, TrackKind>();
	static private ObservableList<TrackKind> HandCardObserv, OpenCardObserv, MissionCardObserv ;
    @FXML
	private ImageView res1, res2, res3, res4, res5, resStack, misStack; 

	@FXML
	private ImageView myres1, myres2, myres3, myres4, myres5, myres6, myres7, myres8, myres9, mymission;

	@FXML
	private ImageView player;

	@FXML
	private Button button;

	@FXML
	private Button closeButton;

	@FXML
	Label res1label, res2label, res3label, res4label, res5label, res6label, res7label, res8label, res9label;
	
	@FXML
	private Label mission;

	@FXML
	private Label trainsleft;

	@FXML
	private Label numMission;

	@FXML
	private Label numTrainsLeft;

	@FXML
	private Label points;

	@FXML
	private ScrollPane sp;

	@FXML
	private AnchorPane ap;

	static private Stage stage;

	public Controller() {
	}

	public void setMain(Stage primaryStage, PlayerLogic pl) {
		this.stage = primaryStage;
		this.pl = pl;
		
		HandCardObserv = FXCollections.observableList(pl.getContestant().getHandCards());
		pl.getContestant().setHandCards(HandCardObserv);
		HandCardObserv.addListener(new ListChangeListener<TrackKind>() {
				@Override
			public void onChanged(ListChangeListener.Change<? extends TrackKind> c) {
					while(c.next()){
	
							  handCardsUpdate();
		                    
						
					}
				
			}
		     });
		
		
		OpenCardObserv = FXCollections.observableArrayList(pl.getOpenResources());
		pl.setOpenResources(OpenCardObserv);
		OpenCardObserv.addListener(new ListChangeListener<TrackKind>() {
				@Override
			public void onChanged(ListChangeListener.Change<? extends TrackKind> c) {
					while(c.next()){
						openCardsUpdate();
					}
				
				
			}
		     });
		
			
		
		player.setImage(playerpic);
		player.toFront();
	}

	@FXML
	private void zoom() {
		sp.addEventFilter(ScrollEvent.ANY, new ZoomHandler(ap));
		Scale scaletrafo = new Scale(ap.getScaleX(), ap.getScaleY(), 0, 0); 
		ap.getTransforms().add(scaletrafo);
		board.getTransforms().add(scaletrafo);
		
		
	}

	@FXML
	private void closeButtonAction() throws Exception {

		RegisterMove rg = new RegisterMove("dieter");
		pl.handleMove(rg);

	}

	@FXML
	private void update() {
		
		resStack.setImage(red);
		misStack.setImage(orange);
		myres1.setImage(bluesmall);
		
		points.setText("30");
	}

	@FXML
	private void clickRes1() throws Exception {
		TrackKind kind = OpenResCards.remove(0);
		TakeCardMove tc = new TakeCardMove(kind,pl.getPlayer());
			pl.handleMove(tc);

	}

	@FXML
	private void clickRes2() throws Exception {
		TrackKind kind = OpenResCards.remove(1);
		TakeCardMove tc = new TakeCardMove(kind,pl.getPlayer());
		pl.handleMove(tc);

	}

	@FXML
	private void clickRes3() throws Exception {
		TrackKind kind = OpenResCards.remove(2);
		//moveCard(res3,resList.get(1));
		TakeCardMove tc = new TakeCardMove(kind, pl.getPlayer());
		 //resList.indexOf(res3)
		pl.handleMove(tc);		

		
	}
	
	private void moveCard(ImageView start, ImageView end){

		ImageView tempV = new ImageView();
		tempV.setImage(start.getImage());
		tempV.blendModeProperty().bind(start.blendModeProperty());
		RotateTransition rt = new RotateTransition(Duration.millis(3000),tempV);
		TranslateTransition tt = new TranslateTransition(Duration.millis(3000),tempV);
		tt.setFromX(start.getParent().getBoundsInParent().getMinX());
		tt.setFromY(start.getParent().getBoundsInParent().getMinX());
		
		tt.setToX(end.getLayoutX());
		tt.setToY(end.getLayoutY());
	    rt.setByAngle(90);
	    rt.setAxis(new Point3D(1, 1, 0));
	    rt.setAutoReverse(true);
	    ParallelTransition  parallelTransition = new ParallelTransition();
	    parallelTransition.getChildren().add(tt);
	    parallelTransition.getChildren().add(rt);
	    parallelTransition.setCycleCount(1);
	    parallelTransition.play();
	        
	}

	@FXML
	private void clickRes4() throws Exception {
		TrackKind kind = OpenResCards.remove(3);
		TakeCardMove tc = new TakeCardMove(kind,pl.getPlayer());
		pl.handleMove(tc);

	}

	@FXML
	private void clickRes5() throws Exception {
		TrackKind kind = OpenResCards.remove(4);
		TakeCardMove tc = new TakeCardMove(kind,pl.getPlayer());
		pl.handleMove(tc);

	}

	@FXML
	private void clickResStack() {
		TakeSecretCardMove tsc = new TakeSecretCardMove(pl.getPlayer());
		try {
			tsc.execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void clickMissionStack() {
		GetMissionsMove move = new GetMissionsMove();
		try {
			move.execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void myRes1() {
	}

	@FXML
	private void myRes2() {
	}

	@FXML
	private void myRes3() {
	}

	@FXML
	private void myRes4() {
	}

	@FXML
	private void myRes5() {
	}

	@FXML
	private void myRes6() {
	}

	@FXML
	private void myRes7() {
	}

	@FXML
	private void myRes8() {
	}

	@FXML
	private void myRes9() {
	}
	@FXML
	Group board;
	
	double x, y;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		board.autosize();
		
		ap.setOnMousePressed(new EventHandler<MouseEvent>() {
			  @Override public void handle(MouseEvent mouseEvent) {
			    x = ap.getTranslateX() - mouseEvent.getScreenX();
			    y = ap.getTranslateY() - mouseEvent.getScreenY();
			  }
			});
		ap.setOnMouseDragged(new EventHandler<MouseEvent>() {
			  @Override public void handle(MouseEvent mouseEvent) {
			    ap.setTranslateX(mouseEvent.getScreenX() + x);
			    ap.setTranslateY(mouseEvent.getScreenY() + y);
			  }
			});	

	    BackgroundImage myBI= new BackgroundImage(new Image(Controller.class.getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/bgbuild.png/"),800,600,false,true),
		        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
		          BackgroundSize.DEFAULT);
	    
		board.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent event) {
	        	
	            for(Node a: board.getChildren()){
	            	if(a.contains(event.getX(), event.getY())){

	            		
	            	   	Platform.runLater(new Runnable() {
			        		  @Override public void run() {
			        			  try {

			        				    buildLoader = new FXMLLoader();
			        				    buildLoader.setLocation(GUI.class.getResource("buildGUI.fxml"));
			        				  
			         buildController buildC = new buildController();
			          
			      	Stage buildStage = new Stage();
			    	AnchorPane buildPane = new AnchorPane();
			    	buildPane = (AnchorPane) buildLoader.load();
			    	Scene buildScene = new Scene(buildPane); 
			    	
						
					
					
			  		  buildPane.setStyle( "-fx-background-color: rgba(0, 0, 0, 0.3);  -fx-border-radius: 10 10 10 10; -fx-background-radius: 10 10 10 10;" ); 
			  		  buildStage.setScene(buildScene);
			  		  buildStage.initOwner(stage);
			  		  buildStage.initModality(Modality.WINDOW_MODAL);
			  		  buildStage.initStyle(StageStyle.TRANSPARENT);
			  		  buildScene.setFill(Color.TRANSPARENT); 
			  		  buildStage.setTitle("Build Track");
			  		  buildPane.setBackground(new Background(myBI));
			  		  buildC.runConfig(stage, pl, BoardTrackList.get(a), buildStage);
			  		  
			  
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        		  }
			        		  
			        		  });
     		
	            		break;

	            	}
	            }
	        }
	    });
		resList.add(myres1);
		resList.add(myres2);
		resList.add(myres3);
		resList.add(myres4);
		resList.add(myres5);
		resList.add(myres6);
		resList.add(myres7);
		resList.add(myres8);
		resList.add(myres9);
		
		
		resLabelList.add(res1label);
		resLabelList.add(res2label);
		resLabelList.add(res3label);
		resLabelList.add(res4label);
		resLabelList.add(res5label);
		resLabelList.add(res6label);
		resLabelList.add(res7label);
		resLabelList.add(res8label);
		resLabelList.add(res9label);
		
		for(Label e: resLabelList){
			e.setId("ressource");
			e.setVisible(false);
		}
		
		res1label.textProperty().bind(myres1prop);
		res2label.textProperty().bind(myres2prop);
		res3label.textProperty().bind(myres3prop);
		res4label.textProperty().bind(myres4prop);
		res5label.textProperty().bind(myres5prop);
		res6label.textProperty().bind(myres6prop);
		res7label.textProperty().bind(myres7prop);
		res8label.textProperty().bind(myres8prop);
		res9label.textProperty().bind(myres9prop);
		
		
		resPropList.add(myres1prop);
		resPropList.add(myres2prop);
		resPropList.add(myres3prop);
		resPropList.add(myres4prop);
		resPropList.add(myres5prop);
		resPropList.add(myres6prop);
		resPropList.add(myres7prop);
		resPropList.add(myres8prop);
		resPropList.add(myres9prop);
		
	
		openResList.add(res1);
		openResList.add(res2);
		openResList.add(res3);
		openResList.add(res4);
		openResList.add(res5);
		
		
		smallImageMap.put(TrackKind.ALL, loksmall);
		smallImageMap.put(TrackKind.BLACK, blacksmall);
		smallImageMap.put(TrackKind.BLUE, bluesmall);
		smallImageMap.put(TrackKind.GREEN, greensmall);
		smallImageMap.put(TrackKind.ORANGE, orangesmall);
		smallImageMap.put(TrackKind.RED, redsmall);
		smallImageMap.put(TrackKind.VIOLET, violetsmall);
		smallImageMap.put(TrackKind.WHITE, whitesmall);
		smallImageMap.put(TrackKind.YELLOW, yellowsmall);
		
		ImageMap.put(TrackKind.ALL, lok);
		ImageMap.put(TrackKind.BLACK, black);
		ImageMap.put(TrackKind.BLUE, blue);
		ImageMap.put(TrackKind.GREEN, green);
		ImageMap.put(TrackKind.ORANGE, orange);
		ImageMap.put(TrackKind.RED, red);
		ImageMap.put(TrackKind.VIOLET, violet);
		ImageMap.put(TrackKind.WHITE, white);
		ImageMap.put(TrackKind.YELLOW, yellow);
		
		
		}

	
	@FXML
	private void myMisAction() {
	}
	
	
	
	public void buildBoard(){
		Platform.runLater(new Runnable() {
			
			@Override public void run() {
		handCardsUpdate();
		openCardsUpdate();
			}
		});
		

		for(Track e : pl.getBoard().getListTracks()){

			
			Platform.runLater(new Runnable() {
				
				@Override public void run() {
				
					   double delta = Math.sqrt((100.0*e.getCity1().getX()-100.0*e.getCity2().getX())*(100.0*e.getCity1().getX()-100.0*e.getCity2().getX())+(100.0*e.getCity1().getY()-100.0*e.getCity2().getY())*(100.0*e.getCity1().getY()-100.0*e.getCity2().getY()));
					   double step = delta/e.getLength() - 20;
					   double phi = Math.atan((100.0*e.getCity1().getY()-100*e.getCity2().getY())/(100.0*e.getCity1().getX()-100*e.getCity2().getX()));
					   
					   Line trackline = new Line();
					   
					switch (e.getColor()) {
					case  VIOLET:
						trackline.setStroke(Color.VIOLET);
						break;
					case  WHITE:
						trackline.setStroke(Color.WHITE);
						break;
					case  BLUE:
						trackline.setStroke(Color.BLUE);
						break;
					case  YELLOW:
						trackline.setStroke(Color.YELLOW);
						break;
					case  ORANGE:
						trackline.setStroke(Color.ORANGE);
						break;
					case  BLACK:
						trackline.setStroke(Color.BLACK);
						break;
					case RED:
						trackline.setStroke(Color.RED);
						break;
					case  GREEN:
						trackline.setStroke(Color.GREEN);
						break;
					case  ALL:
						trackline.setStroke(Color.GRAY);
						break;
					default:
						trackline.setStroke(Color.GRAY);
						break;
					}
					
				if(e.getCity1().getX() < e.getCity2().getX()){
					 
					   trackline.setStartX(100.0*e.getCity1().getX()+Math.cos(phi)*20);
					   trackline.setStartY(100.0*e.getCity1().getY()+Math.sin(phi)*20);
					   trackline.setEndX(100.0*e.getCity2().getX()-Math.cos(phi)*20);
					   trackline.setEndY(100.0*e.getCity2().getY()-Math.sin(phi)*20);
					   
				}
				else
				{
					   trackline.setStartX(100.0*e.getCity2().getX()+Math.cos(phi)*20);
					   trackline.setStartY(100.0*e.getCity2().getY()+Math.sin(phi)*20);
					   trackline.setEndX(100.0*e.getCity1().getX()-Math.cos(phi)*20);
					   trackline.setEndY(100.0*e.getCity1().getY()-Math.sin(phi)*20);
				}
				
					   trackline.setSmooth(true);
					   trackline.getStrokeDashArray().addAll(step,20d);
					   trackline.setId("track");
					   int i=1;
					   for(Node a: board.getChildren()){   
						  
					   while(a.getBoundsInParent().contains(trackline.getBoundsInParent())){
						
							if(e.getCity1().getX() == e.getCity2().getX()){
								 
								trackline.setStartX(trackline.getStartX()+i*15);
								trackline.setEndX(trackline.getEndX()+i*15);
							}
							else if(e.getCity1().getY() == e.getCity2().getY()){
								trackline.setStartY(trackline.getStartY()+i*15);
								trackline.setEndY(trackline.getEndY()+i*15);
							}
							else
							{
								trackline.setStartX(trackline.getStartX()+i*15);
								trackline.setEndX(trackline.getEndX()+i*15);
								trackline.setStartY(trackline.getStartY()+i*15);
								trackline.setEndY(trackline.getEndY()+i*15);
							}
							i++;
							
					   }
				
					   }
					   
					   
					   board.getChildren().add(trackline);
					   BoardTrackList.put(trackline, e);
				  }
			});	
			
		}
		
		
		
		for(City e : pl.getBoard().getListCities()){
			Platform.runLater(new Runnable() {
				
				@Override public void run() {
					Text Cityname = new Text(e.getName());
					Cityname.setX(e.getX()*100);
					Cityname.setY(e.getY()*100-10);
					Cityname.setStyle( "-fx-fill: linear-gradient(#ffd65b, #e68400), linear-gradient(#ffea6a, #efaa22), linear-gradient(#ffe657 0%, #f8c202 50%, #eea10b 100%), linear-gradient(from 0% 0% to 15% 50%, rgba(255,255,255,0.9), rgba(255,255,255,0));-fx-effect: dropshadow( one-pass-box , rgba(0,0,0,0.9) , 1, 0.0 , 0 , 1 );");
					
					Ellipse circle = new Ellipse();
					//  circle.setId("citycircle");
					   circle.setRadiusX(5);
					   circle.setRadiusY(5);
					   circle.setCenterX(e.getX()*100);
					   circle.setCenterY(e.getY()*100);
					   board.getChildren().add(Cityname);
					   board.getChildren().add(circle);
					
					   Cityname.toFront();
	
				  }
			});	
		
		}
		
		Platform.runLater(new Runnable() {
			
			@Override public void run() {
				Group tempG = new Group();
		tempG.getChildren().add(new ImageView(missioncard));
		
				
		WritableImage im =new WritableImage(500, 399);
		WritableImage im2 =new WritableImage(1110, 589);
		SnapshotParameters params = new SnapshotParameters();
		params.setFill(Color.TRANSPARENT);
		board.snapshot(params, im);
		ImageView tempI = new ImageView(im);
		tempI.setScaleX(0.9);
		tempI.setScaleY(0.9);
		tempG.getChildren().add(tempI);
		
		tempG.snapshot(params, im);
		
		final Image blub = im;
		misStack.setImage(blub);
	
			 }
		});	
		board.autosize();
		ap.heightProperty().add(board.getLayoutY());
		ap.widthProperty().add(board.getLayoutX());

		
	}
	
	public void handCardsUpdate(){
		HandCardsMap = new HashMap<TrackKind,Integer>();
		for(TrackKind e: pl.getContestant().getHandCards()){
			if(!HandCardsMap.keySet().contains(e)){
				HandCardsMap.put(e, 1);
			}
			else
			{
				HandCardsMap.put(e, HandCardsMap.get(e)+1);
			}
		}
		
		
		
		Platform.runLater(new Runnable() {
			
			@Override public void run() {
			
			
		for(TrackKind e : HandCardsMap.keySet()){
			if(!HandCards.contains(e)){
				HandCards.add(e);
				int i = HandCards.indexOf(e);
				resPropList.get(i).set(Integer.toString(HandCardsMap.get(e)));
				resLabelList.get(i).setVisible(true);
				resList.get(i).setImage(smallImageMap.get(e));

			}
			else
			{
				int i = HandCards.indexOf(e);
				resPropList.get(i).set(Integer.toString(HandCardsMap.get(e)));
				
			}
		}
			}		
		});	
		
	}
	
	
	public void openCardsUpdate(){
		
		int i =0;
		for(TrackKind e:pl.getOpenResources()){
			
			System.out.println(e.toString());
			
			OpenResCards.put(i, e);
			
//			if(OpenResCards.size()==5){
//				OpenResCards.add(i, e);	
//			}
//			else
//			{
//				OpenResCards.add(e);
//			}
			
				openResList.get(i).setImage(ImageMap.get(e));	
			i++;
			
		}
		
		
	}
			
}
