package de.unisaarland.sopra.zugumzug.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import de.unisaarland.sopra.AdditionalCost;
import de.unisaarland.sopra.Failure;
import de.unisaarland.sopra.FinalMissions;
import de.unisaarland.sopra.FinalPoints;
import de.unisaarland.sopra.GameStart;
import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.NewMissions;
import de.unisaarland.sopra.NewObserver;
import de.unisaarland.sopra.NewOpenCard;
import de.unisaarland.sopra.NewPlayer;
import de.unisaarland.sopra.Player;
import de.unisaarland.sopra.ServerEvent;
import de.unisaarland.sopra.TakeCard;
import de.unisaarland.sopra.TakeSecretCard;
import de.unisaarland.sopra.TookCard;
import de.unisaarland.sopra.TookSecretCard;
import de.unisaarland.sopra.TrackBuilt;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.TunnelBuilt;
import de.unisaarland.sopra.TunnelNotBuilt;
import de.unisaarland.sopra.Winner;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.moves.BuildTrackMove;
import de.unisaarland.sopra.zugumzug.moves.GetMissionsMove;
import de.unisaarland.sopra.zugumzug.moves.LeaveMove;
import de.unisaarland.sopra.zugumzug.moves.Moves;
import de.unisaarland.sopra.zugumzug.moves.PayTunnelMove;
import de.unisaarland.sopra.zugumzug.moves.RegisterMove;
import de.unisaarland.sopra.zugumzug.moves.ReturnMissionsMove;
import de.unisaarland.sopra.zugumzug.moves.TakeCardMove;
import de.unisaarland.sopra.zugumzug.moves.TakeSecretCardMove;

class CityTupel {

	public City start, destination;

	public CityTupel(City start, City destination) {
		this.start = start;
		this.destination = destination;
	}

}

public class KI implements Gamer {

	private PlayerLogic pl;
	private String name;
	private Player p;
	private int myId;
	static ServerEvent event;
	private Board board;
	private HashMap<Integer, QueueCity> hashMapFinished;
	private HashMap<Integer, QueueCity> hashMapQueue;
	PriorityQueue<QueueCity> pq;
	private Mission[] missions;
	List<Mission> myMissions = new ArrayList<Mission>();
	private int maxTracks;
	private int numTurns = 0;
	ArrayList<ConnectedCities> tracksToBuild = new ArrayList<ConnectedCities>();
	ArrayList<TrackResource> myTracks = new ArrayList<TrackResource>();
	private Track currTunnel;
	private int currCards;
	ArrayList<TrackKind> myResources = new ArrayList<TrackKind>();
	private Contestant contestant;
	

	public KI(String name) {
		this.name = name;
		this.myId=-1;
	}

	@Override
	public void go(PlayerLogic playerLogic) throws Exception {
		setPlayerLogic(playerLogic);
		setPlayer(pl.getPlayer());
		pl.setGamer(this);
		board = pl.getBoard();
		this.contestant = pl.getContestant();
		// handle the gamestate event
		event = (ServerEvent) p.pollNextEvent();
		pl.decideEvent(event);
		// send out register
		RegisterMove regMove = new RegisterMove(name);
		pl.handleMove(regMove);

		event = (ServerEvent) p.pollNextEvent();

		// if the register event fails we close the player
		if (event.getId() == 5) {
			p.close();
			System.exit(0);
		}

		// handle own newPlayer event and further newplayer events
		while (event instanceof NewPlayer) {
			NewPlayer np = (NewPlayer) event;
			pl.decideEvent(event);

			if (np.getName().equals(name)&&myId==-1) {
				myId = np.getPlayerId();
			}
			event = (ServerEvent) p.pollNextEvent();
		}

		// handle the map data resources/maxtracks/cities/tracks
		handleMapData();

		// handle the card events open cards/hand cards/ mission cards
		handleCards();

		// catch all events that are not game start and print them
		// should be none (only for debug)
		while (!(event instanceof GameStart)) {

//			pl.eventToString(event);
			event = (ServerEvent) p.pollNextEvent();
			pl.decideEvent(event);
		}

		// the game loop for the ki
		while (!(event instanceof FinalMissions)) {
		

			if (myTurn(event)) {
				makeTurn();
			} else {
				event = (ServerEvent) p.pollNextEvent();
				while (event.getId() == Failure.ID || event.getId() == NewObserver.ID) {
					pl.decideEvent(event);
					event = (ServerEvent) p.pollNextEvent();
				}
				pl.decideEvent(event);
			}

		}

		// if the final mission event comes you know the game is over
		handleEndGameEvents();

	}

	/**
	 * handes all events after the game is over (also leaves the game)
	 * 
	 * @throws Exception
	 */
	private void handleEndGameEvents() throws Exception {

		// TODO: 2 winners

		int maxPoints = 0;
		int winners = 1;

		while (event.getId() != Winner.ID) {

			if (event.getId() == FinalPoints.ID) {

				FinalPoints fp = (FinalPoints) event;
				//System.out.println(fp.getPoints());
				if (fp.getPoints() > maxPoints) {
					maxPoints = fp.getPoints();
					winners = 1;
				} else if (fp.getPoints() == maxPoints) {
					winners++;
				}

			}

			event = (ServerEvent) p.pollNextEvent();
			pl.decideEvent(event);

		}

		Winner win = (Winner) event;

		//System.out.println("Player with id " + win.getPlayerId() + " has won the game.");

		if (winners == 1) {
			p.close();
			System.exit(0);
		} else {
			Thread.sleep(10000);	
			p.close();
				System.exit(0);
			}


	}
	
	public void getHandCards() throws Exception {
		//System.out.println("take first secretCard");
		pl.handleMove(new TakeSecretCardMove(p));
		event = (ServerEvent) p.pollNextEvent();
		while (event.getId() != TookSecretCard.ID) {
			pl.decideEvent(event);
			//System.out.println(pl.eventToString(event));
			if (event.getId() == Failure.ID) {
				drawMissionCards();
				//System.out.println("this is fucked up");
			} else if (event.getId() == FinalMissions.ID) {
				handleEndGameEvents();
			}
			event = (ServerEvent) p.pollNextEvent();
		}
		pl.decideEvent(event);

		//System.out.println(pl.eventToString(event));

		if (pl.rClosedStackSize > 0) {
			//System.out.println("Take Sec Secret Card");
			pl.handleMove(new TakeSecretCardMove(p));

			event = (ServerEvent) p.pollNextEvent();
			while (event.getId() != TookSecretCard.ID) {
				pl.decideEvent(event);
				//System.out.println(pl.eventToString(event));
				if (event.getId() == Failure.ID) {
					//System.out.println("this is fucked up");
				} else if (event.getId() == FinalMissions.ID) {
					handleEndGameEvents();
				}
				event = (ServerEvent) p.pollNextEvent();
			}
			pl.decideEvent(event);
			//System.out.println(pl.eventToString(event));

		} else {
			//System.out.println("Take sec openStack Card");
			for (int i = 0; i < pl.getOpenResources().size(); i++) {
				if (pl.getOpenResources().get(i) != TrackKind.ALL) {
					pl.handleMove(new TakeCardMove(pl.getOpenResources().get(i), p));
					break;
				}
			}
				while (event.getId() != NewOpenCard.ID) {
					pl.decideEvent(event);
					//System.out.println(pl.eventToString(event));
					if (event.getId() == Failure.ID) {
						//System.out.println("this is fucked up");
					} else if (event.getId() == FinalMissions.ID) {
						handleEndGameEvents();
					}
					event = (ServerEvent) p.pollNextEvent();
				}
				pl.decideEvent(event);

			

		}
	}
	private void drawMissionCards() throws Exception {
		 MissionsCalculator mc= new MissionsCalculator(pl.getContestant(),pl);
		 pl.handleMove(new GetMissionsMove());
		 event = (ServerEvent) p.pollNextEvent();
		 while(event.getId()!= NewMissions.ID) {
			 if(event.getId()==Failure.ID) {
				 getHandCards();
				 return;
			 }
			 pl.decideEvent(event);
			 //System.out.println(pl.eventToString(event));
			 event= (ServerEvent) p.pollNextEvent();
		 }
		 pl.decideEvent(event);
		 //System.out.println(pl.eventToString(event));
		 Mission[] mi= new Mission[contestant.getTempMissions().size()];
		 
		 for(int w=0;w<contestant.getTempMissions().size();w++) {
			 mi[w]= new Mission((short)contestant.getTempMissions().get(w).getCity1().getId(),
					 (short)contestant.getTempMissions().get(w).getCity2().getId(), (short)contestant.getTempMissions().get(w).getPoints());
		 }
		 
		 mc.choseMissions(mi);
	
	}
	
	private void buildTrack(Track build, HashMap<Track,TrackKind> decidedTracks) throws Exception {
		TrackKind tk = decidedTracks.get(build);
		int numcolors = 0;
		
		for (int i = 0; i < contestant.getHandCards().size(); i++) {
			if (contestant.getHandCards().get(i) == tk) {
				numcolors++;
			}
		}
		int locs = build.getLength() - numcolors;
		if (locs < 0) {
			numcolors=  numcolors-build.getLength();
			locs = 0;
		} else {
			numcolors=0;
		}
		pl.handleMove(new BuildTrackMove(tk, build.getId(), locs));

		if (!build.isTunnel()) {
			//System.out.println("BuildingTracks");
			event = (ServerEvent) p.pollNextEvent();
			while (event.getId() != TrackBuilt.ID) {
				pl.decideEvent(event);
				//System.out.println(pl.eventToString(event));
				if (event.getId() == Failure.ID) {
					if(pl.getrClosedStack().size()>0&&pl.getMaxHandCards()-2>contestant.getHandCards().size()) {
						getHandCards();
						return;
					} else {
						drawMissionCards();
						return;
					}
					//System.out.println("this is fucked up");
				} else if (event.getId() == FinalMissions.ID) {
					handleEndGameEvents();
				}
				event = (ServerEvent) p.pollNextEvent();
			}
			pl.decideEvent(event);

		} else {
			//System.out.println("Building Tunnel");
			currTunnel = build;
			currCards = numcolors;
			event = (ServerEvent) p.pollNextEvent();
			while (event.getId() != TunnelNotBuilt.ID) {
				if (event.getId() == AdditionalCost.ID) {
					pl.decideEvent(event);
					//System.out.println(pl.eventToString(event));
					AdditionalCost ac = (AdditionalCost) event;
					int numlocs= ac.getAdditionalCosts()- numcolors;
					//System.out.println("ac " + ac.getAdditionalCosts());
					//System.out.println("numcolors " + numcolors);
					if(numlocs<0) {
						numlocs=0;
					}
					//System.out.println("1. " + numlocs);
					PayTunnelMove ptm = new PayTunnelMove(numlocs);
					//System.out.println(ptm.getNumLocs());
					pl.handleMove(ptm);
					event= (ServerEvent) p.pollNextEvent();
					while(event.getId()!= TunnelBuilt.ID) {
					
					pl.decideEvent(event);
					//System.out.println(pl.eventToString(event));
					event= (ServerEvent) p.pollNextEvent();
					}
					break;
				}
				pl.decideEvent(event);
				//System.out.println(pl.eventToString(event));
				if (event.getId() == Failure.ID) {
					//System.out.println("this is fucked up");
				} else if (event.getId() == FinalMissions.ID) {
					handleEndGameEvents();
				}
				event = (ServerEvent) p.pollNextEvent();
			}
			
			
			
			pl.decideEvent(event);
			//System.out.println(pl.eventToString(event));	
			currTunnel = null;

		}
	}
	

	/**
	 * This method decides which turn to make
	 * 
	 * @throws Exception
	 */
	private void makeTurn() throws Exception {
		missions = new Mission[pl.getContestant().getListMissions().size()];

		for (int k = 0; k < pl.getContestant().getListMissions().size(); k++) {
			missions[k] = new Mission((short) pl.getContestant().getListMissions().get(k).getCity1().getId(),
					(short) pl.getContestant().getListMissions().get(k).getCity2().getId(),
					pl.getContestant().getListMissions().get(k).getPoints());
		}

		numTurns++;
		this.contestant = pl.getContestant();
		//System.out.println("I make my " + numTurns + " turn.");
		List<TrackKind> handCards = pl.getContestant().getHandCards();
		List<TrackKind> openCards = pl.getOpenResources();
		myMissions = Arrays.asList(missions);

		// check the missions and add all tracks that are needed to build those
		// missions
		//System.out.println("Calculating resources...");

		MissionsCalculator mc = new MissionsCalculator(pl.getContestant(), pl);
		tracksToBuild = mc.calculateBestTrack(missions);

		// calculateNeededTracks(myMissions);
		ResourceCalculator rc = new ResourceCalculator(tracksToBuild, pl);
		HashMap<Track, TrackKind> decidedTracks = rc.filter();

		if (decidedTracks.isEmpty()) {
			//System.out.println("All missions are built.");
			NewTracks nt = new NewTracks(decidedTracks, contestant.getHandCards(), board, contestant.getTrainsLeft(), pl);
			rc = new ResourceCalculator(nt.getNewTracksToBuild(), pl);
			decidedTracks = rc.filter();
			if(nt.drawMissions) {
				return;
			}
			
		}

		if (!rc.canBuildAll() && handCards.size() < pl.getMaxHandCards() - 2 && pl.rClosedStackSize > 0) {
			getHandCards();

		}

		else if (!decidedTracks.isEmpty()) {
			TrackCalculator tc = new TrackCalculator(decidedTracks,contestant.getHandCards());
			Track build = tc.pickTrack();
			if(build==null) {
				NewTracks nt = new NewTracks(decidedTracks, contestant.getHandCards(), board, contestant.getTrainsLeft(), pl);
				rc = new ResourceCalculator(nt.getNewTracksToBuild(), pl);
				decidedTracks = rc.filter();
				if(nt.drawMissions) {
					return;
				}
				tc = new TrackCalculator(decidedTracks,contestant.getHandCards());
				build = tc.pickTrack();
			}
			assert(build!=null);
			
			buildTrack(build,decidedTracks);

		} else {
			System.out.println("here");
			if(handCards.size() < pl.getMaxHandCards() - 2 && pl.rClosedStackSize > 0) {
				getHandCards();
			} else {
				drawMissionCards();
			}
		}
		// Thread.sleep(2000);

	}

	/**
	 * 
	 * @param m
	 * @param handCards
	 * @param reqRes
	 * @return
	 */
	private void calculateNeededTracks(List<Mission> m) {

		tracksToBuild.clear();
		Iterator<Mission> iterM = m.iterator();

		while (iterM.hasNext()) {

			Mission miss = iterM.next();

			City city1 = board.getListCities().get(miss.getStart());
			City city2 = board.getListCities().get(miss.getDestination());

			// System.out.println("The shortest path form " + city1.getName() +
			// " to " + city2.getName());
			Dijkstra dijkstra = new Dijkstra();
			List<City> path = dijkstra.findShortestPath(city1, city2, pl);
			/*
			 * for (int i = 0; i < path.size(); i++) {
			 * System.out.println(path.get(i)); }
			 */

			int i = 0;
			City start = path.get(0);

			while (i < (path.size() - 1)) {

				Iterator<Integer> trackIdIter = start.getMap().keySet().iterator();

				while (trackIdIter.hasNext()) {

					int id = trackIdIter.next();

					if (start.getMap().get(id).equals(path.get(i + 1))) {

						Track t = board.getListTracks().get(id);

						ConnectedCities newCc = new ConnectedCities(t.getCity1(), t.getCity2());

						// if the 2 cities are connected add the track to this
						// city pair
						if (tracksToBuild.contains(newCc)) {

							Iterator<ConnectedCities> iterC = tracksToBuild.iterator();

							while (iterC.hasNext()) {
								ConnectedCities c = iterC.next();
								if (newCc.equals(c)) {
									c.tracks.add(t); // TODO: track is added
														// twice
								}
							}
							// add a new connection
						} else {
							ConnectedCities cc = new ConnectedCities(t.getCity1(), t.getCity2());
							cc.tracks.add(t);
							tracksToBuild.add(cc);

						}
					}
				}
				i++;
				start = path.get(i);
			}
		}

		for (Iterator<ConnectedCities> itrc = tracksToBuild.iterator(); itrc.hasNext();) {
			ConnectedCities cc = itrc.next();
			for (int j = 0; j < cc.tracks.size(); j++) {
				if (cc.tracks.get(j).getOwnerId() == pl.myId) {
					itrc.remove();
					break;
				}

			}
		}

	}

	/**
	 *
	 * @return true if the ki can make a turn, false otherwise
	 */
	private boolean myTurn(ServerEvent event) {
		if (pl.getPlayerList().get(0).getId() == myId) {
			return true;
		} else
			return false;
	}

	/**
	 * This method chooses which missions to take
	 * 
	 * @throws Exception
	 */
	private void chooseMissionCards() throws Exception {

		// take all 3 missions
		List<MissionCard> returnMissions = new ArrayList<MissionCard>();
		NewMissions nm = (NewMissions) event;
		missions = nm.getMissions();


//		ReturnMissionsMove rmm = new ReturnMissionsMove(returnMissions);
//		pl.handleMove(rmm);
		
		MissionsCalculator mc= new MissionsCalculator(pl.getContestant(), pl);
		Mission[] miss=mc.choseMissions(missions);
		
		//System.out.println("I returned " + returnMissions.size() + " Missions.");
		for (int i = 0; i < miss.length; i++) {

			Mission m = miss[i];
			City city1 = board.getListCities().get(m.getStart());
			City city2 = board.getListCities().get(m.getDestination());
			//System.out.println("My " + i + ". missions is from " + city1.getName() + " to " + city2.getName());

		}
		

		while (event.getId() == 11 || event.getId() == 13|| event.getId()==NewObserver.ID) { // PlayerLeft oder
																// TookMissions
			pl.decideEvent(event);
			event = (ServerEvent) p.pollNextEvent();
			//System.out.println(pl.eventToString(event));
		}

	}

	/**
	 * This method handles all the events that send open cards/ hand cards /
	 * mission cards
	 * 
	 * @throws Exception
	 */
	private void handleCards() throws Exception {

		// newOpenCard or new(Hand)Card
		while (event.getId() == 11 || event.getId() == 17 || event.getId() == 2|| event.getId()==NewObserver.ID) {

			pl.decideEvent(event);
			event = (ServerEvent) p.pollNextEvent();
		}

		// newMission
		if (event.getId() == 4 || event.getId() == 11|| event.getId()==NewObserver.ID) {
			pl.decideEvent(event);
			chooseMissionCards();
		} else
			event.toString(); // debug print

	}

	/**
	 * This method handles the events that send reources/maxtracks/cities/tracks
	 * 
	 * @throws Exception
	 */
	private void handleMapData() throws Exception {

		// new cards(reources) or playerleft event
		while (event.getId() == 2 || event.getId() == 11|| event.getId()==NewObserver.ID) {

			pl.decideEvent(event);
			event = (ServerEvent) p.pollNextEvent();

		}

		// max tracks
		if (event.getId() == 20 || event.getId() == 11|| event.getId()==NewObserver.ID) {
			pl.decideEvent(event);
		} else
			event.toString(); // again debug print

		// handle cities events
		event = (ServerEvent) p.pollNextEvent();

		while (event.getId() == 22 || event.getId() == 11|| event.getId()==NewObserver.ID) {

			pl.decideEvent(event);
			event = (ServerEvent) p.pollNextEvent();
		}

		// handle tracks
		while (event.getId() == 21 || event.getId() == 11|| event.getId()==NewObserver.ID) {
			pl.decideEvent(event);
			event = (ServerEvent) p.pollNextEvent();
		}

	}

	@Override
	public void additionalCosts(int additionalCosts) throws Exception {
		int locs = currTunnel.getLength() - currCards;
		if (locs < 0) {
			locs = additionalCosts;
		}
		p.payTunnel(locs);

	}

	@Override
	public void die(String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void failure(String message) {
//		System.out.println(message);

	}

	@Override
	public void finalMissions(int playerId, Mission[] missionsSuccess, Mission[] missionsFail) {
		// TODO Auto-generated method stub

	}

	@Override
	public void finalPoints(int playerId, int points) {
		// TODO Auto-generated method stub

	}

	@Override
	public void gameStart() {
		// TODO Auto-generated method stub

	}

	@Override
	public void longestTrack(int playerId, int length) {
		// TODO Auto-generated method stub

	}

	@Override
	public void maxTrackLength(int maxTrack) {
		maxTracks = maxTrack;

	}

	@Override
	public void update(Contestant contestant) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playerLeft(int playerId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tookCard(int playerId, TrackKind kind) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tookMissions(int playerId, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tookSecretCard(int playerId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void trackBuilt(int playerId, short trackId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tunnelBuilt(int playerId, short trackId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tunnelNotBuilt(int playerId, short trackId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void winner(int playerId) {
		// TODO Auto-generated method stub

	}

	@Override
	public Moves getMove() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setContestant(Contestant contestant) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNumPlayers(int numPlayers) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNumCards(int numCards) {
		// TODO Auto-generated method stub

	}

	public PlayerLogic getPlayerLogic() {
		return pl;
	}

	public void setPlayerLogic(PlayerLogic pl) {
		this.pl = pl;
	}

	private void setPlayer(Player player) {
		this.p = player;

	}

	@Override
	public void newMissions() throws IOException {
		// TODO Auto-generated method stub

	}

}

class TrackResource {

	Track t;
	TrackKind tk;
	int count;

	public TrackResource(Track t, TrackKind tk, int count) {
		this.t = t;
		this.tk = tk;
		this.count = count;
	}

}

class ConnectedCities {

	City city1, city2;
	List<Track> tracks = new ArrayList<Track>();

	public ConnectedCities(City city1, City city2) {
		this.city1 = city1;
		this.city2 = city2;
	}

	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if (o instanceof ConnectedCities) {
			ConnectedCities cc = (ConnectedCities) o;

			if (this.city1.equals(cc.city1) && this.city2.equals(cc.city2)) {
				return true;
			} else if (this.city1.equals(cc.city2) && this.city2.equals(cc.city1)) {
				return true;
			}

		}
		return false;
	}

}
