package de.unisaarland.sopra.zugumzug.client;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.media.AudioEqualizer;
import javafx.stage.Stage;

public class welcomeController implements Initializable{
	static final private AudioClip sound = new AudioClip(welcomeController.class.getResource("/de/unisaarland/sopra/zugumzug/client/res/trains019.mp3").toString());
	static final private AudioClip sound2 = new AudioClip(welcomeController.class.getResource("/de/unisaarland/sopra/zugumzug/client/res/bgmusic.mp3").toString());
	static private PlayerLogic pl;
	static private Stage stage;
	static private Scene scene;
	@FXML
	TextField playername;
	@FXML
	Button join;
	@FXML
	Pane bgpane;
	@FXML
	public void joinButton() throws IOException{
		sound2.cycleCountProperty();
		
		//sound2.play();
			for(int i=1;i>=0;i--){
				sound.setVolume(i);
			}
			sound.stop();
			
	
		 pl.getPlayer().register(playername.getText());
		 stage.setScene(scene);
		 join.requestFocus();
		 bgpane.requestFocus();
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//sound.play();
		join.setId("ressource");
		
	}
	public void setPL(PlayerLogic pl) {
		this.pl = pl;
		
	}
	public void setStage(Stage welcomeStage, Scene scene) {
		this.stage = welcomeStage;
		this.scene = scene;
		
	}
}
