package de.unisaarland.sopra.zugumzug.client;

import java.io.IOException;

import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.moves.Moves;
import javafx.concurrent.Task;

public interface Gamer {

	public void additionalCosts(int additionalCosts) throws Exception;

	public void die(String message);

	public void failure(String message);

	public void finalMissions(int playerId, Mission[] missionsSuccess, Mission[] missionsFail);

	public void finalPoints(int playerId, int points);

	public void gameStart();

	public void longestTrack(int playerId, int length);

	public void maxTrackLength(int maxTrack); // Do we Need this?

	public void playerLeft(int playerId);

	public void tookCard(int playerId, TrackKind kind);

	public void tookMissions(int playerId, int count);

	public void tookSecretCard(int playerId);

	public void trackBuilt(int playerId, short trackId);

	public void tunnelBuilt(int playerId, short trackId);

	public void tunnelNotBuilt(int playerId, short trackId);

	public void winner(int playerId);

	public Moves getMove();

	public void go(PlayerLogic pl) throws IOException, Exception;

	public void setContestant(Contestant contestant);

	public void setNumPlayers(int numPlayers);

	public void setNumCards(int numCards);

	public void update(Contestant contestant);

	void newMissions() throws IOException;

}
