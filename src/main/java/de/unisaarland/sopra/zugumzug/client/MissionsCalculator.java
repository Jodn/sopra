package de.unisaarland.sopra.zugumzug.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import de.unisaarland.sopra.Failure;
import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.ServerEvent;
import de.unisaarland.sopra.TookMissions;
import de.unisaarland.sopra.zugumzug.commands.ReturnMissionCommand;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.moves.ReturnMissionsMove;

public class MissionsCalculator {
	
	private Contestant contestant;
	private PlayerLogic pl;

	public MissionsCalculator(Contestant contestant, PlayerLogic pl) {
		this.contestant=contestant;
		this.pl=pl;

	}
	

	
	public Mission[] choseMissions(Mission[] missions) throws Exception {
		List<Mission> keep= new ArrayList<Mission>();
		//System.out.println("new Missions");
		List<Mission> negMissions= new ArrayList<Mission>();
		List<Mission> tmpMissions= new ArrayList<Mission>();
		
		
		
		for(int i=0; i<missions.length;i++) {
			if(missions[i].getPoints()<=0) {
				negMissions.add(missions[i]);
				
			} else {
				tmpMissions.add(missions[i]);
			}
		}
		
		
		missions= new Mission[tmpMissions.size()];
		for(int i=0;i<tmpMissions.size();i++) {
			missions[i]=tmpMissions.get(i);
		}
		
		
		
		HashSet<Track> allMissions= bestRoute(missions);
		if(calculateTrackLength(allMissions)<(pl.getContestant().getTrainsLeft()*0.9)) {
			keep=new ArrayList<Mission>(Arrays.asList(missions));
			//TODO: return Missions
		} else {
			int maxLength= pl.getMaxTracks();
			Mission tmpMi=missions[0];
			if(missions.length>1) {
				
				
				for(int i=0;i<missions.length;i++) {	
					
					ArrayList<Mission> miArray= new ArrayList<Mission>(Arrays.asList(missions));

					miArray.remove(i);
					Mission[] tmpArray= new Mission[miArray.size()];
					for(int d=0;d<miArray.size();d++) {
						tmpArray[d]= miArray.get(d);
					}
					
					allMissions=bestRoute(tmpArray);
					if(calculateTrackLength(allMissions)>(pl.getContestant().getTrainsLeft()*0.9)) {
						if(maxLength< calculateTrackLength(allMissions)) {
							keep.clear();
							keep.addAll(miArray);
							maxLength= calculateTrackLength(allMissions);
							tmpMi=missions[i];
					}
				}
				
				}
			}
			if(maxLength<pl.getMaxTracks()) {
				//TODO: return Missions
			} else {
				
				for(int i=0;i<missions.length;i++) {
					Mission[] tmpArray= new Mission[1];
					tmpArray[0]= missions[i];
					allMissions=bestRoute(tmpArray);
					if(calculateTrackLength(allMissions)>(pl.getContestant().getTrainsLeft()*0.9)) {
						if(maxLength< calculateTrackLength(allMissions)) {
							keep.clear();
							keep.addAll(Arrays.asList(tmpArray));
							maxLength= calculateTrackLength(allMissions);
							tmpMi=missions[i];
						}
					}
				}
				if(maxLength>=pl.getMaxTracks()) {
					int minMissPoints=Integer.MAX_VALUE;
					for(int i=0;i< missions.length;i++) {
						if(minMissPoints>missions[i].getPoints()) {
							Mission[] tmpArray= new Mission[1];
							tmpArray[0]= missions[i];
							minMissPoints=missions[i].getPoints();
							keep.clear();
							keep.addAll(Arrays.asList(tmpArray));
						}
					}
				}
			}
			
			
		}
		List<MissionCard> retMissions= new ArrayList<MissionCard>();
		for(int i=0;i<missions.length;i++) {
			if(!keep.contains(missions[i])) {
				retMissions.add(new MissionCard(pl.getBoard().getListCities().get(missions[i].getStart()),
										pl.getBoard().getListCities().get(missions[i].getDestination()), missions[i].getPoints(), i));
			}
		}
		
		
		
		ReturnMissionsMove rmm= new ReturnMissionsMove(retMissions);
		//pl.getContestant().setTempMissions(retMissions);
		pl.handleMove(rmm);
		ServerEvent event= (ServerEvent) pl.getPlayer().pollNextEvent();
		while(event.getId()!=TookMissions.ID) {
			pl.decideEvent(event);
			if(event.getId()==Failure.ID) {
				//System.out.println("fuck");
			}
			pl.getPlayer().pollNextEvent();
		}
		pl.decideEvent(event);
		
		
//		for(int k=0; k<negMissions.size();k++) {
//			Mission m= negMissions.get(k);
//			keep.add(m);
//		}
//		
		
		
		Mission[] resArray= new Mission[keep.size()+negMissions.size()];
		for(int i=0;i<keep.size();i++) {
			resArray[i]= keep.get(i);
		}
		for(int k=keep.size();k<keep.size()+negMissions.size();k++) {
			resArray[k]=negMissions.get(k-keep.size());
		}
		return resArray;
	
	}
	
	
	private HashSet<Track> bestRoute(Mission[] missions) {
		int minTracks= Integer.MAX_VALUE;
		List<City> path1= new ArrayList<City>();
		Dijkstra dijk= new Dijkstra();
		HashSet<Track> ShortestTracks= new HashSet<Track>();
		
		//first, just calc shortest Path for every mission independently
		for(int i=0; i<missions.length;i++) {
			if(missions[i]!=null) {
				path1= dijk.findShortestPath(pl.getBoard().getListCities().get(missions[i].getStart()),
													pl.getBoard().getListCities().get(missions[i].getDestination()), pl);
				if(path1!=null) {
					ShortestTracks.addAll(calculateTracks(path1));
				}
			}
				
		}
		minTracks= calculateTrackLength(ShortestTracks);
		
		
		
		
		
		for(int i=0; i<missions.length;i++) {
			//integer is position of the Mission in the Array, Tracks it's corresponding shortest Path
			HashMap<Integer,HashSet<Track>> tmpShortTrack= new HashMap<Integer,HashSet<Track>>();
			path1=dijk.findShortestPath(pl.getBoard().getListCities().get(missions[i].getStart()),
					pl.getBoard().getListCities().get(missions[i].getDestination()), pl);
			tmpShortTrack.put(i,calculateTracks(path1));
			
			if(path1==null) {
				continue;
			}
			
			for(int j=0; j<missions.length;j++) {
				if(i!= j&& missions[i]!= null && missions[j]!=null) {
					List<City> path2= new ArrayList<City>();
					int tmpShortLength=Integer.MAX_VALUE;
					
					path2=dijk.findShortestPath(pl.getBoard().getListCities().get(missions[j].getStart()),
							pl.getBoard().getListCities().get(missions[j].getDestination()), pl);
					if(path2==null) {
						continue;
					}
					tmpShortTrack.put(j,calculateTracks(path2));
					tmpShortLength=calculateTrackLength(calculateTracks(path2));
					
					
					for(int k=0;k<path1.size();k++) {
						for(int l=0; l<path1.size();l++) {
							List<City> tmpListCities=dijk.findShortestPath(pl.getBoard().getListCities().get(missions[j].getStart()),path1.get(k), pl);
							List<City> tmpPath= new ArrayList<City>();
							if(tmpListCities==null)
								continue; 
							tmpPath.addAll(tmpListCities);

		
							tmpListCities=(dijk.findShortestPath(path1.get(l),pl.getBoard().getListCities().get(missions[j].getDestination()), pl));
							if(tmpListCities==null) {
								continue;
							}
							else tmpPath.addAll(tmpListCities);
							
							HashSet<Track> currTracks= calculateTracks(tmpPath);
							int tmp= calculateTrackLength(currTracks);
							if(tmp<tmpShortLength) {
								tmpShortLength=tmp;
								tmpShortTrack.put(j, currTracks);
							}
						}
					}
					
				}
				
			
				
			}
			HashSet<Track> calc= new HashSet<Track>();
			for(Iterator<HashSet<Track>> it= tmpShortTrack.values().iterator();it.hasNext();) {
				HashSet<Track> tmpSet= it.next();
				calc.addAll(tmpSet);			
			}
			int tmpres= calculateTrackLength(calc);
			if(tmpres<minTracks) {
				minTracks=tmpres;
				ShortestTracks= calc;
			}
			
		}
		return ShortestTracks;
	}
	
	
	public ArrayList<ConnectedCities> calculateBestTrack(Mission[] missions) {
		
		List<Mission> tmpMission= new ArrayList<Mission>(Arrays.asList(missions));
		for(int i=0;i< tmpMission.size();i++) {
			if(tmpMission.get(i).getPoints()<=0) {
				tmpMission.remove(i);
			}
		}
		
		missions= new Mission[tmpMission.size()];
		
		for(int j=0;j<tmpMission.size();j++) {
			missions[j]= tmpMission.get(j);
		}
		
		HashSet<Track> ShortestTracks= bestRoute(missions);
		
		
		return createConnectedCities(ShortestTracks); 
	}
	
	
	private ArrayList<ConnectedCities> createConnectedCities(HashSet<Track> tracks) {
		
		ArrayList<ConnectedCities> cc= new ArrayList<ConnectedCities>();
		List<Track> tracklist= pl.getBoard().getListTracks();
		
		for(Iterator<Track> it= tracks.iterator();it.hasNext();) {
			Track track = it.next();
			ConnectedCities tmp= new ConnectedCities(track.getCity1(),track.getCity2());
			if(cc.contains(tmp)) {
				for(int k=0; k<cc.size();k++) {
					if (cc.get(k).equals(tmp)) {
						tmp= cc.get(k);
					}
				}
			} else {
				cc.add(tmp);
			}
			if(!tmp.tracks.contains(track)) {
				tmp.tracks.add(track);
			}
			
			for(int j=0; j<pl.getBoard().getListTracks().size();j++) {
				if((tracklist.get(j).getCity1().equals(track.getCity1())||tracklist.get(j).getCity1().equals(track.getCity2()))&&
								((tracklist.get(j).getCity2().equals(track.getCity1()))||tracklist.get(j).getCity2().equals(track.getCity2()))){
					if(!(tmp.tracks.contains(tracklist.get(j)))) {
					tmp.tracks.add(tracklist.get(j));
					}
				}
			}
				
		}
			
		
		
		
		return cc;
	}
	
	
	
	
	
	
	private HashSet<Track> calculateTracks(List<City> connectedCities) {
		HashSet<Track> result= new HashSet<Track>();
		if(connectedCities==null) {
			return null;
		}
		for(int i=0; i<connectedCities.size();i++) {
			if(i!=connectedCities.size()-1) {
				int con=-1;
				for(Iterator<Integer> itr= connectedCities.get(i).getMap().keySet().iterator(); itr.hasNext();) {
					int tmp= itr.next();
					if(connectedCities.get(i).getMap().get(tmp).equals(connectedCities.get(i+1))) {
						con=tmp;
						break;
					}
				}
				if (con >=0) {
					result.add(pl.getBoard().getListTracks().get(con));
				}
			}
				
		}
		return result;
	}
	
	private int calculateTrackLength(HashSet<Track> tracks) {
		int res=0;
		for(Iterator<Track> itr= tracks.iterator();itr.hasNext();) {
			Track track= itr.next();
			if(track.getOwnerId()==-1) {
			res+=track.getLength();
			}
		}
		return res;
	}
	
}
