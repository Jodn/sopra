package de.unisaarland.sopra.zugumzug.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.omg.IOP.TAG_RMI_CUSTOM_MAX_STREAM_FORMAT;

import com.sun.javafx.collections.MappingChange.Map;

import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.NewMissions;
import de.unisaarland.sopra.ServerEvent;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Board;
import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.MissionCard;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.moves.GetMissionsMove;

public class NewTracks {

	private HashMap<TrackKind, Integer> mapCards;
	private HashMap<Track, TrackKind> decidedTracks;
	private List<TrackKind> handCards;
	private Board board;
	private int trainsLeft;
	private PlayerLogic pl;
	protected boolean drawMissions = true;
	List<ConnectedCities> tracksToBuild;

	public NewTracks(HashMap<Track, TrackKind> decidedTracks, List<TrackKind> handCards, Board board, int trainsLeft,
			PlayerLogic pl) {
		this.decidedTracks = decidedTracks;
		this.handCards = handCards;
		this.board = board;
		this.trainsLeft = trainsLeft;
		this.pl = pl;
	}

	public List<ConnectedCities> getNewTracksToBuild() throws Exception {

		// look how many cards from which trackkind we have
		mapCards = new HashMap<TrackKind, Integer>();

		for (TrackKind tk : TrackKind.values()) {

			mapCards.put(tk, 0);
		}

		for (int i = 0; i < handCards.size(); i++) {

			mapCards.put(handCards.get(i), mapCards.get(handCards.get(i)) + 1);

		}

		drawMissions = true;

		for (int i = 0; i < pl.getPlayerList().size(); i++) {

			if (pl.getPlayerList().get(i).getTrainsLeft() < pl.getMaxTracks() * 0.4) {
				drawMissions = false;
			}
		}

		if (drawMissions) {
			
			return getNewMissions();
		} else if(canConnectMissions()){
			return tracksToBuild;
		}
		return takeRandomTrack();

	}

	private List<ConnectedCities> getNewMissions() throws Exception {
		
		GetMissionsMove gmm = new GetMissionsMove();
		pl.handleMove(gmm);
		ServerEvent event = (ServerEvent) pl.getPlayer().pollNextEvent();
		while (event.getId() != NewMissions.ID) {
			pl.decideEvent(event);
			event = (ServerEvent) pl.getPlayer().pollNextEvent();
		}
		pl.decideEvent(event);

		NewMissions nm = (NewMissions) event;

		MissionsCalculator mc = new MissionsCalculator(pl.getContestant(), pl);

		Mission[] missions = mc.choseMissions(nm.getMissions());
		tracksToBuild = mc.calculateBestTrack(missions);
		return tracksToBuild;
	}

	/**
	 * if possible adds the best track to build in the decidedTracks List
	 */
	private List<ConnectedCities> takeRandomTrack() {
		
		
		Track track = null;
		List<ConnectedCities> ttb = null;

		// look for all tracks to build and take the longest one you can afford
		for (Track t : board.getListTracks()) {

			if (t.getOwnerId() == -1 && t.getLength() <= trainsLeft) {

				if (mapCards.get(t.getColor()) + mapCards.get(TrackKind.ALL) >= t.getLength()) {
					
					if(!citiesAreConnected(t)){
					
					if (track == null) {
						track = t;
					} else if (track.getLength() < t.getLength()) {
						track = t;
					}
				}
				}
			}
		}

		if (track != null) {
			//System.out.println("added new track");
			ttb = new ArrayList<ConnectedCities>();
			ConnectedCities cc = new ConnectedCities(track.getCity1(), track.getCity2());
			cc.tracks.add(track);
			ttb.add(cc);
		} else {
			for(Track t: board.getListTracks()) {
				if(t.getOwnerId()==-1&&t.getLength()<trainsLeft) {
					if(!citiesAreConnected(t)) {
						ttb = new ArrayList<ConnectedCities>();
						ConnectedCities cc = new ConnectedCities(t.getCity1(), t.getCity2());
						cc.tracks.add(t);
						ttb.add(cc);
						return ttb;
					}
				}
			}
			
		}
		return ttb;

	}

	private boolean citiesAreConnected(Track track) {
		
		HashMap<Integer,City>  cityMap = track.getCity1().getMap();
		for(int trackId : cityMap.keySet()){
			
			Track tempTrack = board.getListTracks().get(trackId);
			if(cityMap.get(trackId).getId()==track.getCity2().getId()) {
			
			if(tempTrack.getOwnerId() == pl.myId){
				return true;
			}
		}
		}
			return false;
			
	}

	/**
	 * looks for missions(current tracks) to connect
	 * 
	 * @return true if missions can be connected with current resource, false
	 *         otherwise
	 */
	private boolean canConnectMissions() {
		
		//TODO:
		
		return false;
	}

}
