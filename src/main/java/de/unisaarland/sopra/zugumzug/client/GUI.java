package de.unisaarland.sopra.zugumzug.client;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.event.EventHandler;
import de.unisaarland.sopra.Mission;
import de.unisaarland.sopra.ServerEvent;
import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Contestant;
import de.unisaarland.sopra.zugumzug.moves.LeaveMove;
import de.unisaarland.sopra.zugumzug.moves.Moves;
import de.unisaarland.sopra.zugumzug.moves.RegisterMove;
import de.unisaarland.sopra.zugumzug.moves.TakeSecretCardMove;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class GUI extends Application implements Gamer {
	static private Contestant contestant;
	public static int myId;
	private int numPlayers;
	private int numCards;
	private boolean firstMission = true;
	static private PlayerLogic pl;
	private static Text output = new Text();
	private Moves move;
	private AnchorPane rootLayout, welcomeLayout;
	private Stage welcome;
	static private Stage primaryStage;
	private AnchorPane design;
	private TextInputDialog dialog;
	private Task<ServerEvent> pollnextEventTask;
	private Task<Void> task;
	private Thread eventThread;
	private SimpleStringProperty numCardsCon = new SimpleStringProperty();
	private Task<Void> label;
	static private FXMLLoader missionLoader,welcomeLoader;
	static private Scene scene;
	static private Controller controller;
	
	public void start(Stage stage) throws IOException {
		this.primaryStage = stage;
		primaryStage.setAlwaysOnTop(true);
		primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);

		// FXMLLoader welcomeLoader = new FXMLLoader();
		// welcomeLoader.setController(this);
		// welcomeLoader.setLocation(GUI.class.getResource("welcomeGUI.fxml"));
		// welcomeLayout = (AnchorPane) welcomeLoader.load();
		// Scene welcomeScene = new Scene(welcomeLayout);
		//
		//Scene scene = new Scene(rootLayout);
		// welcome.setScene(welcomeScene);
	//	primaryStage.setFullScreen(true);
	//	primaryStage.setScene(scene);

		this.primaryStage.setTitle("Ticket to Ride");
		// this.primaryStage.setFullScreen(true);
		this.primaryStage.setResizable(false);
		initRootLayout();
		
		 welcomeScreen();
		missionLoader = new FXMLLoader();
        missionLoader.setLocation(GUI.class.getResource("missionGUI.fxml"));
        welcomeLoader = new FXMLLoader();
        welcomeLoader.setLocation(GUI.class.getResource("welcomeGUI.fxml"));
        primaryStage.setOnCloseRequest(e -> Platform.exit());
		
                
        
        
		pollnextEventTask = new Task<ServerEvent>() {
			@Override
			protected ServerEvent call() throws Exception {
				return null;

			}

			@Override
			public void run() {

				try {
					while (true) {
						final ServerEvent event = (ServerEvent) pl.getPlayer().pollNextEvent();
						pl.decideEvent(event);
						System.out.println(event.toString());
					
						Thread.sleep(10);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		};

		Task task = new Task<Void>() {
			@Override
			public Void call() throws Exception {
				if (contestant != null) {

					while (true) {
						final String temp = new String(Integer.toString(contestant.getHandCards().size()));
						Platform.runLater(new Runnable() {
							@Override
							public void run() {

								numCardsCon.set(temp);
								
							}
						});
						Thread.sleep(1000);
					}
				}
				return null;

			}
		};

		Thread th = new Thread(task);
		th.setDaemon(true);
		th.start();

		eventThread = new Thread(pollnextEventTask);
		eventThread.setDaemon(true);
		eventThread.start();


		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent t) {
				Platform.exit();
				System.exit(0);
			}
		});


	}
	
	
	public Stage getPrimaryStage(){
		return primaryStage;
	}
	
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(GUI.class.getResource("Design.fxml"));
			design = (AnchorPane) loader.load();

			// Show the scene containing the root layout.
			scene = new Scene(design);
			//primaryStage.setScene(scene);
			primaryStage.sizeToScene();
			//primaryStage.show();

			controller = loader.getController();
			controller.setMain(primaryStage,pl);
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void welcomeScreen(){
		Platform.runLater(new Runnable() {
  		  @Override public void run() {
  			  try {
  				  				  
  				  Stage welcomeStage = primaryStage;// new Stage();
    welcomeController welcomeS = new welcomeController();
	AnchorPane welcomePane = new AnchorPane();
	welcomePane = (AnchorPane) welcomeLoader.load();
	Scene welcomeScene = new Scene(welcomePane); 
	BackgroundImage myBI= new BackgroundImage(new Image(welcomeController.class.getResourceAsStream("/de/unisaarland/sopra/zugumzug/client/res/steamtrain.jpg/"),1280,800,false,true),
	        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
	          BackgroundSize.DEFAULT);

	welcomePane.setBackground(new Background(myBI));
	welcomeStage.setScene(welcomeScene); 
	welcomeS.setPL(pl);
	welcomeS.setStage(welcomeStage,scene); 
	welcomeStage.setTitle("Welcome"); 
	welcomeStage.show();
	
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		  }
  		  
  		  });





	}

	@Override
	public void stop() throws Exception {
		move = new LeaveMove();
		pl.handleMove(move);
	}

	@FXML
	public void joinButton() {
		welcome.hide();
		primaryStage.show();
		
	}

	@FXML
	/*
	 * @FXML >>>>>>> refs/heads/master Label nameLabel;
	 * 
	 * @FXML Label numCardsLabel;
	 * 
	 * @FXML TextField textfeld;
	 * 
	 * @FXML public void registerAction() throws Exception { dialog = new
	 * TextInputDialog("Name"); dialog.setTitle("Change Name");
	 * dialog.setHeaderText("Change your playername"); dialog.setContentText(
	 * "Please enter your name:");
	 * 
	 * dialog.showAndWait(); move = new RegisterMove(dialog.getResult());
	 * pl.handleMove(move);
	 * 
	 * // nameLabel.setText(""+pl.getPlayer().getPlayerId());
	 * this.die(dialog.getResult());
	 * 
	 * }
	 * 
	 * @FXML public void drawSecretCardClick() throws Exception {
	 *
	 * }
	 * 
	 * @FXML public void changenameAction() { dialog.showAndWait(); }
	 */
	public void go(PlayerLogic pl) throws IOException {
		this.pl = pl;
		pl.setGamer(this);
		pl.run();
		Application.launch();
	}

	@Override
	public void additionalCosts(int additionalCosts) {
		// TODO Auto-generated method stub

	}

	@Override
	public void die(String message) {

		System.out.println(pl.toString());

	}

	@Override
	public void failure(String message) {
		System.out.println(message);
	}

	@Override
	public void finalMissions(int playerId, Mission[] missionsSuccess, Mission[] missionsFail) {
		// TODO Auto-generated method stub

	}

	@Override
	public void finalPoints(int playerId, int points) {
		// TODO Auto-generated method stub

	}

	@Override
	public void gameStart() {
		// TODO Auto-generated method stub

	}

	@Override
	public void longestTrack(int playerId, int length) {
		// TODO Auto-generated method stub

	}

	@Override
	public void maxTrackLength(int maxTrack) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Contestant contestant) {
		this.contestant = contestant;
	  
	}

	@Override
	public void playerLeft(int playerId) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void newMissions() throws IOException{
		if(firstMission){
			controller.buildBoard();
			firstMission=false;
		}
		
		        	Platform.runLater(new Runnable() {
		        		  @Override public void run() {
		        			  try {
		         missionController missionC;
		          missionC = new missionController();
		          
		      	Stage missionStage = new Stage();
		    	AnchorPane missionPane = new AnchorPane();
		    	missionPane = (AnchorPane) missionLoader.load();
		    	Scene missionScene = new Scene(missionPane); 
		    	
					
				
				
		  		  missionPane.setStyle( "-fx-background-color: rgba(0, 0, 0, 0.1);  -fx-border-radius: 10 10 0 0; -fx-background-radius: 10 10 0 0;" ); 
		  		  missionStage.setScene(missionScene); 
		  		  missionStage.initOwner(primaryStage);
		  		  missionStage.initModality(Modality.WINDOW_MODAL);
		  		  missionStage.initStyle(StageStyle.TRANSPARENT);
		  		  missionScene.setFill(Color.TRANSPARENT); 
		  		  missionC.setPL(pl);
		  		  missionC.setStage(missionStage); 
		  		  missionStage.setTitle("Take Missions"); 
				  missionStage.showAndWait();
				 
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        		  }
		        		  
		        		  });
		   
		
		
		  
		
		  
		  
	}
	

	
	@Override
	public void tookCard(int playerId, TrackKind kind) {
	}

	@Override
	public void tookMissions(int playerId, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tookSecretCard(int playerId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void trackBuilt(int playerId, short trackId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tunnelBuilt(int playerId, short trackId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tunnelNotBuilt(int playerId, short trackId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void winner(int playerId) {
		// TODO Auto-generated method stub

	}

	@Override
	public Moves getMove() {
		// TODO Auto-generated method stub
		return move;
	}

	public void setContestant(Contestant contestant) {
		this.contestant = contestant;
	}

	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	public void setNumCards(int numCards) {
		this.numCards = numCards;
	}

}
