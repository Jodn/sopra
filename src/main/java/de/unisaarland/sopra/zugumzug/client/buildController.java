package de.unisaarland.sopra.zugumzug.client;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import de.unisaarland.sopra.TrackKind;
import de.unisaarland.sopra.zugumzug.data.Track;
import de.unisaarland.sopra.zugumzug.moves.BuildTrackMove;
import de.unisaarland.sopra.zugumzug.moves.Moves;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class buildController implements Initializable {
	private double x,y;
	private int numcards;
	private int numlocs;
	private static TrackKind paymentColor;
	private static Map<TrackKind,Integer> resourceMap = new HashMap<TrackKind,Integer>();
	private static Track buildtrack;
	private static Map<TrackKind, Integer> countKindMap = new HashMap<TrackKind,Integer>();
	static private Stage stage,buildStage;
	static private PlayerLogic pl;
	static final private SimpleStringProperty trackprop = new SimpleStringProperty();
	static final private SimpleStringProperty trackLengthprop = new SimpleStringProperty();
	static final private SimpleStringProperty trackColorprop = new SimpleStringProperty();
	static final private SimpleStringProperty trackPointsprop = new SimpleStringProperty();
	static final private SimpleStringProperty trackTunnelprop = new SimpleStringProperty();
	
	@FXML
	Label track, tracklength, trackcolor, trackpoints, tracktunnel, locoLabel, colorKindlabel;
	
	@FXML
	Pane trackpane;
	@FXML
	Button buildtrackb;
	@FXML
	TextField numloco;
	
	@FXML
	public void buildok() throws Exception{
		numloco.getText();
		if( numloco.getText()!=null && numloco.getText().length()>0) {
		if(Integer.parseInt(numloco.getText())<= resourceMap.get(TrackKind.ALL)){
			Moves btm = new BuildTrackMove(buildtrack.getColor(), buildtrack.getId(),Integer.parseInt(numloco.getText()));  
			System.out.println(resourceMap.get(buildtrack.getColor()));
			pl.handleMove(btm);
			buildStage.close();	
		}
		else
		{
			 Alert alert = new Alert(AlertType.INFORMATION);
			 alert.setTitle("Wrong amount of resources");
			 alert.setHeaderText(null);
			 alert.setContentText("You would better watch your steps. You chose the wrong amount of resources to build this track.");
			 alert.initOwner(stage);
			 alert.showAndWait();
		}
		}
	
	}
	
	public void runConfig(Stage stage, PlayerLogic pl, Track buildtrack, Stage buildStage){
		this.stage = stage;
		this.pl = pl;
		this.buildStage =buildStage;
		List<String> choices = new ArrayList<>();
		 numcards = 0;
		 numlocs = 0;
		this.buildtrack = buildtrack;
		
		
		
		
		if(buildtrack.getColor().equals(TrackKind.ALL)){
			this.paymentColor = null;
			for(TrackKind e : pl.getContestant().getHandCards()){
				if(!countKindMap.containsKey(e)){
					countKindMap.put(e, 1);
				}
				else
				{
					countKindMap.put(e, countKindMap.get(e)+1);
				}
				if(!choices.contains(e.toString())){
					choices.add(e.toString());
				}
			}
			
			ChoiceDialog<String> dialog = new ChoiceDialog<>("kind", choices);
			dialog.setTitle("Choose your color");
			dialog.setHeaderText("You better choose a good color!");
			dialog.setContentText("Choose your color:");
			dialog.initOwner(stage);
			Optional<String> result = dialog.showAndWait();
			for(TrackKind a:TrackKind.values()){
				if(a.toString().equals(result.get()))
				{
					this.paymentColor = a;
				}
				
			}
			System.out.println(paymentColor.toString());
			if(paymentColor==null || result.get().equals("kind")){
				 Alert alert = new Alert(AlertType.INFORMATION);
				 alert.setTitle("Not enough resources");
				 alert.setHeaderText(null);
				 alert.setContentText("I have a great message for you! You do not have enough resources to build a Track from "+ buildtrack.getCity1() + " to "+ buildtrack.getCity2()+".");
				 alert.initOwner(stage);
				 alert.showAndWait();
				 buildStage.close();
				 return;
			}
		}
		else
		{
			this.paymentColor = buildtrack.getColor();
		}
		 for(TrackKind e : pl.getContestant().getHandCards()){
			     if(e.equals(paymentColor)){
			    	 numcards++;
			     }
			     else if(e.equals(TrackKind.ALL))
			     {
			    	 numlocs++;
			     }
		 }
		 
		 resourceMap.put(paymentColor, numcards);
		 resourceMap.put(TrackKind.ALL, numlocs);
		 
		 
		 
		 if(numcards >= buildtrack.getLength()){
			 numcards=buildtrack.getLength();
			 numlocs = 0;
		 }
		 else if((numcards + numlocs)>=buildtrack.getLength()){
			 numlocs = buildtrack.getLength() - numcards;
		 }
		 else
		 {
			 Alert alert = new Alert(AlertType.INFORMATION);
			 alert.setTitle("Not enough resources");
			 alert.setHeaderText(null);
			 alert.setContentText("I have a great message for you! You do not have enough resources to build a Track from "+ buildtrack.getCity1() + " to "+ buildtrack.getCity2()+".");
			 alert.initOwner(stage);
			 alert.showAndWait();
			 buildStage.close();
			 return;
		 }
		
		 final int k =numcards;
		 
		Platform.runLater(new Runnable() {
			final String trackstring = "From "+ buildtrack.getCity1().getName() +" to "+ buildtrack.getCity2().getName();
			final String tracklengthstring = Integer.toString(buildtrack.getLength());
			final String trackcolorstring = buildtrack.getColor().toString();
			final String trackpointsstring = Integer.toString(buildtrack.getPoints());
			final String tracktunnelstring = Boolean.toString(buildtrack.isTunnel());
			final String numcolorString = Integer.toString(k);
			@Override public void run() {		
				
				 
				 trackprop.set(trackstring);
				 trackLengthprop.set(tracklengthstring);
				 trackColorprop.set(trackcolorstring);
				 trackPointsprop.set(trackpointsstring);
				 trackTunnelprop.set(tracktunnelstring);
				}
		});	
		
		buildStage.showAndWait();
		 
		
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		

		track.textProperty().bind(trackprop);
		tracklength.textProperty().bind(trackLengthprop);
		trackcolor.textProperty().bind(trackColorprop);
		trackpoints.textProperty().bind(trackPointsprop);
		tracktunnel.textProperty().bind(trackTunnelprop);
			track.setId("label");
			tracklength.setId("label");
			trackcolor.setId("label");
			trackpoints.setId("label");
			tracktunnel.setId("label");
			buildtrackb.setId("ressource");
			numloco.setId("ressource");
			numloco.setAlignment(Pos.CENTER);
		trackpane.setOnMousePressed(new EventHandler<MouseEvent>() {
			  @Override public void handle(MouseEvent mouseEvent) {
			    x = buildStage.getX() - mouseEvent.getScreenX();
			    y = buildStage.getY() - mouseEvent.getScreenY();
			  }
			});
			trackpane.setOnMouseDragged(new EventHandler<MouseEvent>() {
			  @Override public void handle(MouseEvent mouseEvent) {
			      buildStage.setX(mouseEvent.getScreenX() + x);
			    buildStage.setY(mouseEvent.getScreenY() + y);
			  }
			});	
	
			
	}
	

}
