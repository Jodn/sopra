package de.unisaarland.sopra.zugumzug.client;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import de.unisaarland.sopra.zugumzug.data.City;
import de.unisaarland.sopra.zugumzug.data.Track;

public class Dijkstra {
	
	

	public List<City> findShortestPath(City city1, City city2, PlayerLogic pl) {
		if(city1.equals(city2)) {
			return null;
		}
		LinkedList<City> shortestWay = new LinkedList<City>();
		Comparator<QueueCity> comp = new QueueCityComparator();
		PriorityQueue<QueueCity> pq = new PriorityQueue<QueueCity>(comp);
		HashMap<Integer,QueueCity> hashMapFinished = new HashMap<Integer, QueueCity>();
		HashMap<Integer,QueueCity> hashMapQueue = new HashMap<Integer, QueueCity>();
		QueueCity start = new QueueCity(0, city1);
		pq.offer(start);
		while (!pq.isEmpty()) {

			handleNode(pq, hashMapFinished, hashMapQueue,  pl);
		}

		QueueCity end = hashMapFinished.get(city2.getId());

		if (end == null) {
			//System.out.println("end is null");
			return null;
		}
		
		while (true) {

			if(end==null|| end.getPreCity()==null) {
				return null;
			}

			if(end.getPreCity().getId() == city1.getId())
				break;

			shortestWay.addFirst(end.getPreCity());
			end = hashMapFinished.get(end.getPreCity().getId());

		}

		shortestWay.addFirst(end.getPreCity());
		shortestWay.add(city2);
		return shortestWay;

	}

	private void handleNode(PriorityQueue<QueueCity> pq, HashMap<Integer,QueueCity> hashMapFinished, 
										HashMap<Integer,QueueCity> hashMapQueue, PlayerLogic pl) {

		QueueCity qc = pq.poll();
		qc.setIsFinished(true);
		hashMapFinished.put(qc.getCity().getId(), qc);
		Iterator<Integer> iterTrackId = qc.getCity().getMap().keySet()
				.iterator();

		while (iterTrackId.hasNext()) {

			Track track = pl.getBoard().getListTracks().get(iterTrackId.next());

			if (track.getOwnerId() == -1||track.getOwnerId()==pl.myId) {

				QueueCity newQCity = null;
				int distance;
				if(track.getOwnerId()==pl.myId) {
					distance = qc.getDistance();
				} else {
					distance = qc.getDistance() + track.getLength();
				}
				
				int id = -1;

				if (track.getCity1().equals(qc.getCity())) {
					newQCity = new QueueCity(distance, track.getCity2());
					id = track.getCity2().getId();
				} else {
					newQCity = new QueueCity(distance, track.getCity1());
					id = track.getCity1().getId();
				}

				if (!hashMapFinished.containsKey(id)) {

					if (pq.contains(newQCity)) {

						int currentDistance = 0;

						QueueCity queueCity = hashMapQueue.get(newQCity
								.getCity().getId());
						currentDistance = queueCity.getDistance();

						if (distance < currentDistance) {
							pq.remove(newQCity);
							newQCity.setDistance(distance);
							newQCity.setPreCity(qc.getCity());
							pq.add(newQCity);
							hashMapQueue.put(newQCity.getCity().getId(),
									newQCity);
						}

					} else {
						newQCity.setDistance(distance);
						newQCity.setPreCity(qc.getCity());
						hashMapQueue.put(newQCity.getCity().getId(), newQCity);
						pq.add(newQCity);
					}

				}
			}
		}

	}
	
	
	
	
}

class QueueCityComparator implements Comparator<QueueCity> {

	public int compare(QueueCity city1, QueueCity city2) {
		if (city1.getDistance() < city2.getDistance()) {
			return -1;
		} else if (city1.getDistance() > city2.getDistance()) {
			return 1;
		} else
			return 0;
	}

}

class QueueCity {

	private int distance;
	private City city, preCity;
	private boolean isFinished = false;

	public QueueCity(int distance, City city) {
		this.distance = distance;
		this.city = city;
	}

	public boolean equals(Object o) {

		if (o == null) {
			return false;
		} else if (o instanceof QueueCity) {
			QueueCity qc = (QueueCity) o;

			if (this.city.getId() == qc.getCity().getId()) {
				return true;
			}
		}
		return false;
	}

	public void setDistance(int d) {
		distance = d;
	}

	public boolean getIsFinsihed() {
		return isFinished;
	}

	public void setIsFinished(boolean finished) {
		this.isFinished = finished;
	}

	public int getDistance() {
		return distance;
	}

	public City getCity() {
		return city;
	}

	public void setPreCity(City preCity) {
		this.preCity = preCity;
	}

	public City getPreCity() {
		return preCity;
	}
}

